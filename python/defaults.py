from utils import getthumbnail

def getapplications():
    APPLICATIONS = {"Finder":{"name":"finder2",
                              "path":"/System/Library/CoreServices/Finder.app/Contents/MacOS/Finder"},
                       #       "thumbnail":getthumbnail('data/finder.png')},
                    "Maya":{"path":"/Applications/Autodesk/maya2015/Maya.app/Contents/bin/maya",
                            "name":"maya2"},
                          #  "thumbnail":getthumbnail('data/maya.png')},
                    "Nuke":{"name":"nuke2",
                            "path":"/Applications/Nuke9.0v4/NukeX9.0v4"}
                           # "thumbnail":getthumbnail('data/nuke.png')}
                   }
    return APPLICATIONS

def getprojects():
    PROJECTS = {"armada":{"longname":"Armada Studios",
					  "welcomemessage":"Sailing Away..."},
			"8i":{"longname":"8i Inc.",
				  "welcomemessage": "Welcome to 8i",
				  "applications":[0,1,2]},
			"afi":{"longname":"American Film Institute"},
			"base":{"longname":"Base FX"},
			"deutch":{"longname":"Deutsch Inc"},
			"eightfx":{"longname":"Eight VFX"},
			"feniqo": {"longname":"Feniqo"},
			"frank": {"longname":"Frankincense Films"},
			"mango": {"longname":"Post Mango"},
			"mirada": {"longname":"Mirada Studios"},
			"simon": {"longname":"Simon Brown"},
			"ryangray": {"longname":"Ryan Gray"},
			"straightshooters": {"longname":"Straight Shooters"},
			"unitel": {"longname":"Unitel Productions"},
			"weta": {"longname":"Weta Digital"},
			"chemicalfx":{"longname":"Chemical FX"},
			"deutsch": {"longname":"Deutsch Inc."},
			"logan": {"longname":"Logan"},
			"nancybishop": {"longname":"Nancy Bishop"},
			"muthana":{"longname": "Muthana"},
			}
    return PROJECTS

def gettags():
	TAGS = [{"freshmen": {"name":"freshmen",
						  "imagefilename":"static/images/freshmen.png"}
			 },
			{"seniors": {"name": "seniors",
						   "imagefilename": "static/images/seniors.png"}
			},
			{"sophomores": {"name": "sophomores",
							"imagefilename": "static/images/sophomores.png"}
			 },
			{"juniors": {"name": "juniors",
							   "imagefilename": "static/images/juniors.png"}
			 }]
	return TAGS

def getusers():
    USERS = [
        {"username": "arc",
         "email":"arc@augmentedrealitycapture.com",
         "password":"Z!gR!g2046",
		 "administrator":True,
		 'firstname':'Ar',
		 'lastname':'Capture',
		 'thumbnailfilename':'static/images/defaultthumbnail.png'
         },
        ]
    return USERS