import os
import arkconfig

CSRF_ENABLED = True
SECRET_KEY = 'd573a69d-5033-44b8-832b-4e178c1eab7e'
SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious')


SERVER_PORT = hasattr(arkconfig,"SERVER_PORT") and arkconfig.SERVER_PORT or 5600
SERVER_ADDRESS = hasattr(arkconfig,"SERVER_ADDRESS") and arkconfig.SERVER_ADDRESS or "0.0.0.0"
SERVER_URL = "http://%s:%d"%(SERVER_ADDRESS,SERVER_PORT)
CLIENT_PORT = hasattr(arkconfig,"CLIENT_PORT") and arkconfig.CLIENT_PORT or 5601
CLIENT_ADDRESS = hasattr(arkconfig,"CLIENT_ADDRESS") and arkconfig.CLIENT_ADDRESS or "0.0.0.0"
CLIENT_URL = "http://%s:%d"%(CLIENT_ADDRESS,CLIENT_PORT)

# database
basedir = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
arkdbfile = os.path.join(basedir,"noah.archelon.db")
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + arkdbfile
SQLALCHEMY_DATABASE_URI = "postgresql://%s/noah"%SERVER_ADDRESS
SQLALCHEMY_DATABASE_URI = hasattr(arkconfig,"DATABASE_URI") and arkconfig.DATABASE_URI or SQLALCHEMY_DATABASE_URI
SQLALCHEMY_BINDS = {
#    'users':        'sqlite:///'+os.path.join(basedir,"users.db")
}
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True

#with open('plugins.txt', 'rb') as f:
#	PLUGIN_ARCHIVE = f.readline()

#PLUGIN_ARCHIVE = '\\\\NAS2\\RenderFarmPlugins'

home = os.path.expanduser("~")

# flask_security and flask_social
SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
SECURITY_TRACKABLE = True
SECURITY_PASSWORD_SALT = 'something_super_secret_change_in_production'
# Without this get_auth_token via POST request w/ JSON data does not work
# You keep getting "CSRF token missing" error
WTF_CSRF_ENABLED = False

SECURITY_POST_LOGIN = '/profile'
SECURITY_LOGIN_USER_TEMPLATE = "login.html"
SECURITY_POST_LOGIN_VIEW = "/postlogin"
SECURITY_POST_LOGOUT_VIEW = "/postlogout"
SECURITY_LOGIN_URL = "/login"
SECURITY_PASSWORD_HASH = 'bcrypt'
SECURITY_PASSWORD_SALT = '/2aX16zPnnIgfMwkOjGX4S'

##print ("webconfig",SERVER_ADDRESS, SERVER_PORT, SQLALCHEMY_DATABASE_URI)
