#!/usr/bin/env python

import os
import sys
import re
from string import *

import posixpath

############################
# Globals
############################

IS_SEQUENCE = 1
NOT_SEQUENCE = 0

# Used to sort files based on type so all dirs, files and sequences appear in groups.
IS_DIR = 0
IS_FILE = 1 
IS_SEQ = 2
ETC = 2

############################
# Makes a string of the zeroes padding the sequence.
############################

def getZeroes(num_zeroes):
    zero_string = ""
    for x in range (0,num_zeroes):
        zero_string = zero_string + "0"
    return zero_string

############################
# This will take 2 tuples and sort them based on their 2nd value 
# ([0-2] is range), returning true if val 1 is  larger...
############################

def sortFiles(t1):
    return int(t1[1])

############################
# createFileInfo
# For each File object, define a file name, type, and sequence-missing string.
# Return a tuple of all this information so selected parts can be printed out or omitted.
############################

def createFileInfo(class_list, class_key, add_missing):

    f_name = ""

    # Defaults as ETC, which has same value as seq, so files can be sorted according to type.
    f_type = ETC 
    f_missing = ""
    formatted_class = (f_name, f_type, f_missing)

    missing_string = ""
    file_count = 1

    list_size = len(class_list)
    print_missing = 0

    first_file = class_list[0]
    last_file = class_list[list_size-1]


    # This is either a directory or a file, non-sequential.
    if (first_file.name == last_file.name):
        if (os.path.isdir(first_file.name)):
            f_name  = first_file.name + "/"
            f_type = IS_DIR
        else:
            f_name = first_file.name
            f_type = IS_FILE
    else: 
        # It is a sequence, so start making sequence string.
        front_file = ".".join(first_file.name.split(".")[:-1])


        # Entire string is made here - remembering to pad the last index with it's leading 0's,
        # since they get stripped when the number is read in.
        f_name = (front_file + "-" + last_file.leading_zeroes + repr(last_file.index_num) + "." + class_key[1])
        
        if (add_missing):
            # Only need to do this if they have indicated MISSING need to be listed.
            for each_file in range (0,list_size):

                # Still going through files
                if (file_count != list_size):

                    this_file = class_list[each_file]
                    next_file = class_list[each_file+1]

                    file_count = file_count + 1

                    if ((next_file.index_num - this_file.index_num) != 1):
                        if (print_missing == 0):
                            f_missing = " MISSING: ("
                            print_missing = 1

                        first_missing = this_file.index_num + 1
                        amt_missing = next_file.index_num - this_file.index_num - 1
                        last_missing = this_file.index_num + amt_missing
                         
                        # Number of digits in sequence - length of this one -add that many zeroes.
                        if (class_key[2] != 0):
                            first_zero_pad = getZeroes(class_key[2] - len(repr(first_missing)))
                            last_zero_pad = getZeroes(class_key[2] - len(repr(last_missing)))
                        else:
                            first_zero_pad = ""
                            last_zero_pad = ""

                        f_missing = f_missing +  " " + first_zero_pad + repr(first_missing) + "-" + last_zero_pad + repr(last_missing)

                # Last file, close MISSING sequence.
                else:
                    this_file = class_list[each_file]

        if (print_missing == 1):
            f_missing = f_missing + " )"
            f_type = IS_SEQ
            
    return (f_name, f_type, f_missing)

##############################################################
# This is a class that stores the file and its numerical index
# it is being sorted on. Also stores whether or not class is a
# sequence, and the number of leading zeroes if any, so the proper name can 
# be recreated (UNIX strips leading zeroes when it stores numbers).
##############################################################

def cmp(a):
    return a.index_num

class File:

    def __init__(self, name, num, seq, zeroes):
        self.name = name
        self.index_num = num
        self.is_sequence = seq
        self.leading_zeroes = zeroes
        
############################
# Override the compare function so it compares two index numbers (the number in a sequence).
############################

    def __cmp__(self, other_val):
        return cmp(self.index_num, other_val.index_num)




##############################################################
# This class is the main processor - gets the ls and sorts it into
# a dictionary of classes, then creates a list of tuples containing all
# the information that may have been asked for on the command line. Sorts
# the tuple list based on type, and prints the formatted information.
##############################################################

class Processor:

    def __init__(self,directory=None):

        # A dictionary that holds File objects.
        self.dir_list = {}

        # These are all used in comparisons to arrange sequences.
        self.zero_numbered =  re.compile("\(0*\)")
        self.is_numbers = re.compile("[0-9][0-9]*")
        self.all_zeroes = re.compile("\(0*\)[1-9]*")


        # A list that holds tuples of information to be printed out.
        self.format_list = []


        # Used to store command-line preferences.
        self.print_missing = 0
        self.which_dir = directory or os.environ["PWD"]
        self.print_path = 0

############################
# Parses what came in on the command line.
############################

    def parseArgs(self):

        count = 0

        for x in sys.argv[:]:
            count = count + 1
        
            if (x == "-a"):
                print ("Still under construction")
                # List all files

            elif (x == "-c"):
                print ("Still under construction")
                # Report piz format files that may be corrupted (bad checksum)
        
            elif (x == "-d"):
                # Directory where seq is desired.
                self.which_dir =  sys.argv[count]
                if (posixpath.isdir(self.which_dir)):
                    print (self.which_dir,": ")
                else:
                    print ("Directory", self.which_dir, "doesn't exist. Try again.")
                    sys.exit(1)             

            elif (x == "-e"):
                print ("Still under construction")
                # Show explicitly each frame missing from a sequence. -only if -m already on.

            elif (x == "-F"):
                print ("Still under construction")
                # Show file type

            elif (x == "-g"):
                print ("Still under construction")
                # %s Grep for a given string within label portion of piz format files.

            elif (x == "-i"):
                print ("Still under construction")
                # Ignore upper/lower case distinction during comparisons.

            elif (x == "-m"):
                self.print_missing = 1
                # Show files missing from a sequence.

            elif (x == "-p"):
                self.print_path = 1
                # Prepend complete path.

            elif (x == "-t"):
                print ("Still under construction")
                # %s Change delimeter marking a frame sequence.

            elif (x == "-v"):
                print ("This is version one.")
                sys.exit(1)
                # Show this version of ta_seq

            elif (x == "-"):
                self.showUsage()
                sys.exit(1)

############################
# Show usage.
############################

    def showUsage(self):
        print ("-a: List all files.")
        print ("-c: Report piz format files that may be corrupted (bad checksum).")
        print ("-d: %s Directory where frames exist. Default is current working directory.")
        print ("-e: Show (explicitly) each frame missing from a sequence.")
        print ("....(This option only applicable with -m.)")
        print ("-F: Show file type.")
        print ("-g: %s Grep for a given string within label portion of piz format files.")
        print ("-i: Ignore upper/lower case distinction during comparisons.")
        print ("....(This option only applicable with -g.)")
        print ("-m: Show files missing from a sequence.")
        print ("-p: Prepend complete path.")
        print ("-t: %s Change delimeter marking a frame sequence. Default is \"-\".")
        print ("-v: Show this version of ta_seq")

############################
# Key into the dictionary is the prefix (everything before the numbers), the suffix 
# (everything after the numbers) and the number of digits if there are leading zeroes.
############################

    def processFiles(self):

        self.dir_files = os.listdir(self.which_dir)
        self.dir_files = sorted(self.dir_files)
        for each_file in self.dir_files:
            try:

                # check if ending is a number, then pad with extension
                if (self.is_numbers.search(each_file.split(".")[-1]) and not os.path.isdir(each_file)):
                    each_file = each_file+"."

                ascii_file_num = each_file.split(".")[-2]
                # There is some sort of numbers in the next-to-last position.
                if (self.is_numbers.search(ascii_file_num) and not os.path.isdir(each_file)) :
                    num_digits = 0
                    num_digits = re.subn("[0-9]","1",ascii_file_num)[-1]
                    prefix = ".".join(each_file.split(".")[:-2])
                    suffix = each_file.split(".")[-1]

                    # filter out non numbers
                    ascii2 = ""
                    for x in range(0,len(ascii_file_num)):
                        letter = ascii_file_num[x]
                        if (self.is_numbers.search(letter)):
                            ascii2= ascii2+letter
                    ascii_file_num = ascii2
#	     	    ascii_file_num = re.sub("[a-zA-Z]","",ascii_file_num)

                    # count # of leading zeroes
                    num_zeroes=0
                    for i in range(0, len(ascii_file_num)):
                        if (ascii_file_num[i] == "0"):
                            num_zeroes = num_zeroes+1
                        else:
                            break

                    # If there are any leading zeroes, store the number of zeroes
                    if (num_zeroes > 0):
                        self.all_zeroes.match(ascii_file_num)
                        zero_string = getZeroes(num_zeroes) #self.all_zeroes.group(1)

                        file_num = int(ascii_file_num)

                        new_file = File(each_file, file_num, IS_SEQUENCE, zero_string)

                        # If it is just .0. - don't consider that 0 a leading 0.
                        if (file_num == 0): 
                            if (num_digits == 1):
                                num_digits = 0
                        if ((prefix,suffix,num_digits) in self.dir_list):
                            self.dir_list[prefix,suffix,num_digits].append(new_file)
                        else:
                            self.dir_list[prefix,suffix,num_digits] = [new_file]

                    # There are no leading 0's, so check for a same-#-digits, leading-zeros bucket
                    else:
                        file_num = int(ascii_file_num)

                        new_file = File(each_file, file_num, IS_SEQUENCE, "")


                        # if there is a leading zero bucket add it there
                        if ((prefix,suffix,num_digits) in self.dir_list):
                            self.dir_list[prefix,suffix,num_digits].append(new_file)

                        #otherwise if there is a no-leading-zero bucket, add it there
                        elif ((prefix,suffix,0) in self.dir_list):
                            self.dir_list[prefix,suffix,0].append(new_file)

                        # otherwise make a new no-leadingzeros bucket
                        else:
                            self.dir_list[prefix,suffix,0] = [new_file]
                            
                        # If there is a no-leading-zero bucket, add it there.
#                        if (self.dir_list.has_key((prefix,suffix,0))):
#                            self.dir_list[prefix,suffix,0].append(new_file)

                            # Add it to both if there is a leading-zero, same-#-digits, bucket as well.
#                            if (self.dir_list.has_key((prefix,suffix,num_digits))):
#                                self.dir_list[prefix,suffix,num_digits].append(new_file)
                        # Otherwise make a new no-leading-zeros bucket - this may need be level w/ above if. 
#                        else:
#

#                            self.dir_list[prefix,suffix,0] = [new_file]
#                            if (self.dir_list.has_key((prefix,suffix,num_digits))):
#                                self.dir_list[prefix,suffix,num_digits].append(new_file)

                else:
                    # None of the below are sequential files, so send in 0.
                    # Only need to check these if directory... add slash.

                    new_file = File(each_file, 0, NOT_SEQUENCE, "")

                    self.dir_list[each_file," ",0] = [new_file]

            except IndexError:

                new_file = File(each_file, 0, NOT_SEQUENCE, "")

                self.dir_list[each_file," ",0] = [new_file]


############################
# Makes a complete string full of info that can be formatted and printed out depending 
# on which options are used. For each key, sorts all the File classes (if sequences) in ascending order.
############################

    def createFileList(self):
        for each_key in self.dir_list.keys():
            self.dir_list[each_key] = sorted(self.dir_list[each_key], key=cmp)
            #self.dir_list[each_key].sort()

            self.format_list.append(createFileInfo(self.dir_list[each_key], each_key, self.print_missing))


############################
# This prints out information based on the command-line options.
############################

    def printFileList(self):

        # Use the given sort function as the sort.
        # Sorts according to file type, (dir, file, or seq)
        self.format_list = sorted(self.format_list, key=sortFiles)

        # For each tuple made based on info from the File classes.
        for each_value in self.format_list:

            # If -p option was selected.
            if (self.print_path):
                print (self.which_dir + "/",end = '')

            #Only do this if all are being printed (-a), and if missing (-m) are being printed.
            print (each_value[0],end = '')

            if (self.print_missing):
                print (" " + each_value[2])
            else:
                print ("")

    def getFileList(self):
        ret = []
        self.format_list = sorted(self.format_list,key=sortFiles)
        for each_value in self.format_list:
            z = ""
            if self.print_path:
                z = z+self.which_dir+"/"
            z = z+each_value[0]
            if self.print_missing:
                z = z+ " "+each_value[2]
            ret.append(z)
        return ret
                      
            
def seq(directory=None):
    p = Processor(directory)
    p.processFiles()
    p.createFileList()
    return p.getFileList()

##############################################################
# Make it go.
##############################################################

if __name__ == "__main__":
    p = Processor()

    p.parseArgs()

    p.processFiles()
    p.createFileList()
    p.printFileList()
