#! /usr/bin/env python
#	Copyright  (c)  1997    Industrial   Light   and   Magic.
#	All   rights   reserved.    Used   under   authorization.
#	This material contains the confidential  and  proprietary
#	information   of   Industrial   Light   and   Magic   and
#	may not be copied in whole or in part without the express
#	written   permission   of  Industrial Light  and   Magic.
#	This  copyright  notice  does  not   imply   publication.
#
ilmversion = '$Header: /net/gs/src/scripts/exflist/RCS/exflist.py,v 1.24 1998/11/18 21:52:56 tommy Exp $'

import string
from ilm import frange
import functools

# mimics string.join, but forces all args to be strings
join = lambda args: "%s " * len(args) % tuple(args)

# this translates from -%*,~ shorthand to exflist words like times, on, etc
# from ilm_regex import *
import re

_translate_table = {'dash': (re.compile('\\(<dash> - \\|[0-9]-[0-9]\\)'), '-', ' to '),
                    'modulo': (re.compile('\\(<modulo>%\\)'), '%', ' on '),
                    'star': (re.compile('\\(<star>\\*\\)'), '*', ' times '),
                    'tilde': (re.compile('\\(<tilde>\\~\\)'), '~', ' except '),
                    'comma': (re.compile('\\(<comma>,\\)'), ',', ' '),
                    }


def _translate_shorthand(args):
    for i in range(0, len(args)):
        # substitute for 1-5 dash
        n = re.search("[0-9]-[0-9]", args[i])
        if n:
            newarg = re.sub("-", " to ", args[i])
            args[i] = newarg

        # substitue x for on
        n = re.search("x", args[i])
        if n:
            newarg = re.sub("x", " on ", args[i])
            args[i] = newarg

    str = join(args)

    for label, (robj, repl, sub) in _translate_table.items():
        start = robj.search(str)
        while start != None:
            group = start.group(label)
            glen = len(group)
            subpos = find(group, repl)
            sublen = len(repl)
            group = group[:subpos] + sub + group[subpos + sublen:]
            str = str[:start] + group + str[start + glen:]
            start = robj.search(str)
    return str.split()


_print_warnings = 1


def _WARNWORD(word):
    if _print_warnings:
        print ("%s: WARNING: frame list word '%s' ignored" % (_progname, word))


def _extractrange(args):
    first = []
    lastarg = None
    while args:
        arg = args[0]
        if arg == '-':
            arg = 'to'
        try:
            try:
                next = int(arg)
            except TypeError:
                from types import IntType
                if type(arg) is IntType:
                    next = arg
                else:
                    raise ValueError
            if lastarg == 'to':
                # compute the range
                args = args[1:]
                incr, bump = 1, 1
                try:
                    # use on increment if it's there
                    if args[0] == 'on':
                        try:
                            incr = abs(int(args[1]))  # keep incr positive
                        except ValueError:
                            # try to accept float incrs too
                            incr = abs(atof(args[1]))  # keep incr positive
                        args = args[2:]
                except (ValueError, IndexError):
                    pass
                if next < first[0]:
                    # go backwards if we must
                    incr = -incr
                    bump = -1
                # if incr is non-integer use frange
                if incr % 1:
                    nextrange = frange(first[0], next + bump, incr)
                    nextrange = filter(lambda x, n=next: x <= n, nextrange)
                else:
                    nextrange = range(first[0], next + bump, incr)
                return nextrange, args
            elif first:
                # number followed by number means this range is over
                return first, args
            else:
                # first number in range, keep going
                first = [next]
                args = args[1:]
        except ValueError:
            # ValueError raised if int called on a string
            if arg == 'to':
                if not (first and args[1:]):
                    _WARNWORD(arg)
                    arg = ''
            # these are handled by exflist
            elif arg in ['except', 'times']:
                return first, args
            else:
                _WARNWORD(arg)
            args = args[1:]
        lastarg = arg
    return first, []


def exflist(args, warn=1):
    # turn off warnings if user so desires
    global _print_warnings
    _print_warnings = warn

    import types
    atype = type(args)
    if isinstance(atype, str):
        args = args.split()
    elif atype == type((0,)):
        args = list(args)
    elif atype is type(1):
        args = [repr(args)]
    if len(args) == 1:
        try:
            # check for a singleton number
            int(args[0])
        except (ValueError, TypeError):
            # or maybe a singleton list with an exflist string in it
            try:
                args = args[0].split()
            except TypeError:
                pass
    # or maybe a singleton list with a number in it.  If so-
    # leave args as-is

    args = _translate_shorthand(args)

    ranges = [[]]
    while args:
        thisrange, args = _extractrange(args)

        def remove(item, l=thisrange):
            # this is nested here so we can late-bind the default
            # value for l to the most recent value for 'thisrange'
            try:
                l.remove(item)
            except ValueError:
                pass

        # extract any except ranges, then remove them from the current range.
        # extract any times clause,  then multiply the preceding range by it.
        try:
            while args[0] == 'except':
                # skip except clause if there's no range to apply it to
                if not thisrange:
                    _WARNWORD('except')
                    args = args[1:]
                else:
                    notrange, args = _extractrange(args[1:])
                    map(remove, notrange)
            if args[0] == 'times':
                try:
                    # skip times clause if there's no range to apply it to
                    if not thisrange: raise IndexError
                    # get hom many dups to make
                    count = int(args[1])
                    # produce a list of multiples
                    thisrange = functools.reduce(lambda head, i, c=count: list(head) + ([i] * c), thisrange, [])
                    args = args[2:]
                except (ValueError, IndexError):
                    _WARNWORD('times')
                    args = args[1:]
        except IndexError:
            pass
        ranges.append(thisrange)
    # paste 'em all into one list of numbers
    return functools.reduce(lambda a, b: list(a) + list(b), ranges)


def collapse(numbers, inc=1, bunchesof=-1):
    # the opposite of exflist, takes a long list of numbers and
    # returns an exflist-style contraction (no except, on or times, though)
    # if bunchesof > 1 this returns a list of bunches
    def contract(begin, end, inc=inc):
        if begin == end:
            return begin
        else:
            connector = ('-' * (end - begin > 1)) or ' '
            return '%s%s%s' % (begin, connector, end)

    # copy numbers first so we don't screw up the original list
    numbers = numbers[:]

    import string
    bunches = ''
    # make bunchesof 0 be one big bunch
    bunchesof = bunchesof or -1
    if bunchesof != -1:
        bunches = []
        while numbers:
            bunches.append(collapse(numbers[:bunchesof], inc))
            numbers = numbers[bunchesof:]
    elif numbers:
        # am I breaking anything by not sorting these?
        # numbers.sort()
        begin = end = numbers[0]
        for frame in numbers[1:]:
            if frame - end == inc:
                end = frame
            else:
                bunches = '%s %s' % (bunches, contract(begin, end))
                if (inc > 1) and (begin != end):
                    bunches = bunches + '%' + repr(inc)
                begin = end = frame
        bunches = string.strip('%s %s' % (bunches, contract(begin, end)))
        if (inc > 1) and (begin != end):
            bunches = bunches + '%' + repr(inc)
        # if this bunch is a single number, let it be anumber not a string
        try:
            bunches = int(bunches)
        except ValueError:
            pass
    return bunches


if __name__ == '__main__':
    from sys import argv, exit

    _progname = argv[0]
    try:
        if argv[1] == '-':
            print ("Usage: %s [start [to end [on incr] [except <range>] [times count]]] ..." % _progname)
            print ('  Examples: "1 to 10"')
            print ('            "1 to 10 on 2"')
            print ('            "50 to 40 1 to 70 except 50 to 40"')
            print ('            "1 to 5 times 5"')
            print ('            "1 to 20 except 8 to 15 times 3"')
            exit(2)
        for item in exflist(argv[1:]):
            print (item)
    except IndexError:
        pass
else:
    _progname = 'exflist'

