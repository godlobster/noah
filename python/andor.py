import string


andor = {"$and": [{"$or": [{"one":1},{"two": 2}]},
				  {"name":"%dave%"},
				  {"id":6}],
		 "$or": [{"$and": [{"three":3},{"four": 4}]},
			 {"name":"%mike%"},
				 {"id":5}],
		 "id":1,
        }

def buildand(l):
	q = ""
	for elem in l:
		print elem	

def buildquery(d,operator=None, result="", model=""):
	print (d,operator)
	if type(d) == type ({}):
		keys = d.keys()
		if operator in ("$and","$or"):
			return result+ string.join(list(map(lambda x:buildquery(d[x],x,model=model),keys)))
		elif operator:
			if type(d[operator]) == int:
				return result + " %s.%s == %s,"%(model,operator,d[operator])
			val = d[operator]
			if "%" in val:
				return result + "%s.%s.like('%s')"%(model,operator,d[operator])
			else:
				return result + " %s.%s == '%s',"%(model,operator,d[operator])			
		else:
			return result+ string.join(list(map(lambda x:buildquery(d[x],x,model=model),keys)))
		
	elif type(d) == type([]):
		if operator == "$and":
			return result + "and_(" + string.join(list(map(lambda x:buildquery(x,operator,model=model),d)))[:-1] +"),"
		elif operator == "$or":
			return result + "or_(" + string.join(list(map(lambda x:buildquery(x,operator,model=model),d)))[:-1] +"),"

	else:
		if type(d) == int:
			return result + " %s.%s == %s,"%(model,operator,d)

		val = d
		if "%" in val:
			return result + "%s.%s.like('%s')"%(model,operator,d)
		return result + " %s.%s == '%s',"%(model,operator,d)
	
	return result + ")"

print (".filter("+buildquery(andor,model="ArkEntry")[:-1]+")")

