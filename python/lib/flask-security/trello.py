# -*- coding: utf-8 -*-
"""
    flask.ext.social.providers.google
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    This module contains the Flask-Social google code

    :copyright: (c) 2012 by Matt Wright.
    :license: MIT, see LICENSE for more details.
"""

from __future__ import absolute_import

import httplib2
import oauth2client.client as googleoauth
import apiclient.discovery as googleapi
import oauth2client

config = {
    'id': 'trello',
    'name': 'Trello',
    'module': 'flask_social.providers.trello',
    'base_url': 'https://api.trello.com/1/',
    'authorize_url': 'https://trello.com/1/OAuthAuthorizeToken ',
    'access_token_url': 'https://trello.com/1/OAuthGetAccessToken ',
    'request_token_url': "https://trello.com/1/OAuthGetRequestToken ",
    'access_token_method': 'POST',
    'access_token_params': {
 #       'grant_type': 'authorization_code'
    },
    'request_token_params': {
        'scope': 'read,write',
        'expiration':'never',
}


def get_api(connection, **kwargs):
    credentials = googleoauth.AccessTokenCredentials(
        access_token=getattr(connection, 'access_token'),
        user_agent='',
        scope='https://www.googleapis.com/auth/plus.profile.emails.read'        
    )
    http = httplib2.Http()
    http = credentials.authorize(http)
    return googleapi.build('plus', 'v1', http=http)


def get_provider_user_id(response, **kwargs):
    if response:
        credentials = googleoauth.AccessTokenCredentials(
            access_token=response['access_token'],
            user_agent=''
        )

        http = httplib2.Http()
        http = credentials.authorize(http)
        api = googleapi.build('plus', 'v1', http=http)
        profile = api.people().get(userId='me').execute()
        return profile['id']
    return None


def get_connection_values(response, **kwargs):
    if not response:
        return None

    access_token = response['access_token']

    credentials = googleoauth.AccessTokenCredentials(
        access_token=access_token,
        user_agent=''
    )

    http = httplib2.Http()
    http = credentials.authorize(http)
    api = googleapi.build('plus', 'v1', http=http)
    def test(msg):
        print msg
    profile = api.people().get(userId='me').execute()
    print "flask_social.google.get_connection_values: profile=",profile

    if type(profile["name"]) == dict:
        full_name = profile["name"]["givenName"] +";"+ profile["name"]["familyName"]
    else:
        full_name = profile["name"]

    
    email = profile["emails"][0]["value"]
    for e in profile["emails"]:
        if e["type"] == "account":
            email = e["value"]


    return dict(
        name=config["name"],
        provider_id=config['id'],
        provider_user_id=profile['id'],
        access_token=access_token,
        secret=None,
        display_name=profile["displayName"],
        full_name=full_name,
        profile_url=(profile.has_key("url") and profile["url"]) or profile.get("link"),
        image_url=(profile.has_key("image") and profile["image"]["url"]) or profile.get("picture"),
        email = email,
        password = profile.get("password")
    )
