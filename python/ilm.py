#! /usr/bin/env python
#	Copyright  (c)  1997    Industrial   Light   and   Magic.
#	All   rights   reserved.    Used   under   authorization.
#	This material contains the confidential  and  proprietary
#	information   of   Industrial   Light   and   Magic   and
#	may not be copied in whole or in part without the express
#	written   permission   of  Industrial Light  and   Magic.
#	This  copyright  notice  does  not   imply   publication.
#
ilmversion = '$Header: /net/gs/src/scripts/py/RCS/ilm.py,v 1.24 2001/05/03 00:18:35 joeld Exp $'

import string, re, sys, os, types


# put in for backwards-compatibility for switch from keys
# to keys() in attrmodule
def KEYS(obj):
    keys = obj.keys
    try:
        return keys()
    except TypeError:
        return keys


# takes args of any type, returns as join-by-space string
def joins(*args):
    return ("%s " * len(args) % args)[:-1]


# uses joins to join heterogeneous contents of list into a string
def joinl(l):
    return apply(joins, tuple(l))


# strips filenames in string l down to just basename
def stripf(l):
    return reduce(lambda s, f: s + os.path.basename(f) + " ",
                  " ".split(l), "")[:-1]


# given ['a', 'b', 'c'] returns [(0, 'a'), (1, 'b'), (2, 'c')]
def irange(l, bump=0, step=1):
    return map(None, range(bump, (len(l) * step) + bump, step), l)


def iirange(l, bump=0, step=1):
    return map(None, l, range(bump, (len(l) * step) + bump, step))


def cshsub(s, namespace):
    # expects strings having $names in them and resolves those names to
    # keys in namespace
    bits = "$".split(s)
    newbits = [bits[0]]
    for varbit in bits[1:]:
        splits = re.sub.splitx(varbit, '[^%s]' % (string.digits + string.letters))
        splits[0] = '%%(%s)s' % splits[0]
        newbits.append(string.join(splits, ''))
    return string.join(newbits, '') % namespace


# returns modified time of file in same format as time.time()
# seconds since the epoch, that is
def mtime(filename):
    from stat import ST_MTIME
    return os.stat(filename)[ST_MTIME]


# returns the size of file "filename"
def fsize(filename):
    from stat import ST_SIZE
    return os.stat(filename)[ST_SIZE]


# this figures out the time on a particular fileserver
def fileServerTime(dir=None):
    import tempfile
    tmpdir = None
    if dir:
        # tempfile uses $TMPDIR as location
        if os.environ.has_key('TMPDIR'):
            # remember previous value for this if there was one
            tmpdir = os.environ['TMPDIR']
        os.environ['TMPDIR'] = dir
    # make the file and gets its modify time
    try:
        tmp = tempfile.mktemp()
        open(tmp, 'w').write('\n')
        time = mtime(tmp)
        # ditch the tmpfile
        os.remove(tmp)
    except (IOError, os.error):
        # sometimes the file servers are cranky?
        import time
        time = time.time()
    # unset $TMPDIR
    if tmpdir:
        os.environ['TMPDIR'] = tmpdir
    elif dir:
        del os.environ['TMPDIR']

    return time


# if filename and number passed in, returns if filename's mtime is later
# than number (in seconds since the epoch).  if two filenames passed in
# returns whichever filename has most recent mtime.
def newer(file1, file2):
    if type(file2) is not types.StringType:
        return mtime(file1) >= file2
    return os.path.exists(file1) and \
           ((not os.path.exists(file2)) or (mtime(file1) >= mtime(file2)))


# determines if filename exists anywhere along our path
def onPath(filename):
    import stat
    for path in os.pathsep.split(os.environ['PATH']):
        fullpath = os.path.join(path, filename)
        if os.path.isfile(fullpath):
            if os.stat(fullpath)[stat.ST_MODE] & stat.S_IEXEC:
                return fullpath
    return ''


# mimics unix uptodate program
def uptodate(dst, srclist, prnt=0):
    import stat
    if not os.path.isfile(dst):
        if (prnt):
                print ("%s hasn't been created yet." % dst)
        return 0
    dtime = os.stat(dst)[stat.ST_MTIME]
    for src in srclist:
        stime = os.stat(src)[stat.ST_MTIME]
        if stime > dtime:
            if (prnt):
                print ("%s is out of date." % dst)
            return 0
    return 1


# determines if file is pic or piz based on gtinfo -nbits
def picorpiz(filename):
    version = '['.split(os.popen('gtinfo %s' % filename).readlines()[1])
    if (len(version) > 1) and (version[1][:3] == 'PIZ'):
        return 'piz'
    return 'pic'


# floating point range
def frange(a, b, f=1):
    """Floating point version of range.
    frange(a,b,f) is like range but accepts floating point numbers.
    a will be the first item of the sequence.
    b will not be returned in the sequence, in fact, the highest
       inclusive value will not be included.  Use frangei if you
       need the highest number included.

    (note the absence of 3.0)
    >>> frange(1,3,0.5)
    [1.0, 1.5, 2.0, 2.5]

    (note the absence of 3.7)
    >>> frange(1.2, 3.8, 0.5)
    [1.2, 1.7, 2.2, 2.7, 3.2]"""

    return map(lambda x, a=a, f=f: a + f * x, range(0, 1.0 / f * (b - a)))


# frange inclusive
def frangei(a, b, f=1):
    """Floating point version of range (inclusive).
    frange(a,b,f) is like range but accepts floating point numbers.
    Note that:
        1) a will be the first item of the sequence
	2) b (or the highest inclusive value)
	   will be the last item of the sequence
    see frange().

    (note the inclusion of 3.0)
    >>> frangei(1,3,0.5)
    [1.0, 1.5, 2.0, 2.5, 3.0]

    (note the inclusion of 3.7)
    >>> frangei(1.2, 3.8, 0.5)
    [1.2, 1.7, 2.2, 2.7, 3.2, 3.7]"""

    return map(lambda x, a=a, f=f: a + f * x, range(0, 1.0 / f * (b - a) + 1))


def mail(address, subject, message):
    import os

    cmd = 'Mail -s "' + subject + '" ' + address + ' << EOM\n'
    cmd = cmd + message + "\n"
    cmd = cmd + "EOM\n"

    os.system(cmd)
    return cmd


# amount that t is between v1 and v2
def inb(t, v1, v2):
    v = (t - v1) / (v2 - v1)
    v = min(max(v, 0.0), 1.0)
    return v


# linearly interpolate between v1 and v2 by t.
def lerp(t, v1, v2):
    _lerp = lambda v1, v2, t: v1 * (1.0 - t) + v2 * t
    if type(v1) in [types.ListType, types.TupleType]:
        import UserArray
        v1 = UserArray.UserArray(v1)
        v2 = UserArray.UserArray(v2)
        return list(_lerp(v1, v2, t))
    return _lerp(v1, v2, t)


# Find time at which curve has value v within tolerance.  Assumes that
# curve is monotonically increasing or decreasing.
def solve(curve, v, toler):
    t1 = curve.start  # Start at endpoints of curve
    t2 = curve.end
    v1 = curve[t1]
    v2 = curve[t2]
    while (1):
        a = (v - v1) / (v2 - v1)  # Find desired value along straight lin
        if (a <= 0.0):  # between v1 and v2; if outside range,
            return t1  # bail and return closer endpoint
        if (a >= 1.0):
            return t2
        t = lerp(a, t1, t2)  # Interpolate time for that value
        v0 = curve[t]  # Get actual value at that time
        if (math.fabs(v - v0) <= toler):
            return t  # Close enough to desired value
        if (v1 < v < v0 or v1 > v > v0):
            t2 = t
            v2 = v0
        elif (v0 < v < v2 or v0 > v > v2):
            t1 = t
            v1 = v0
        else:
            print ("solve Error: curve not monotonically increasing or decreasing")
            return t


## gives us a safe rsh that will ignore any garbage a users dot files
## might spit out.  also throws away any stderr output.  args can be
## any types and are stuck together into a space-separated string.
## NOTE: returns None if rsh fails
def rsh(host, *args):
    COOKIE = '__COOKIE__'
    cmd = 'rsh %s -n "echo %s; %s" 2> /dev/null' % (host, COOKIE, apply(joins, args))
    response = os.popen(cmd).read()
    try:
        return COOKIE.split(response)[1][1:]
    except IndexError:
        import format
        format.err('could not rsh to host:', host)
        return None


## mimics ilm function netpath- returns fully expanded pathname
def netpath(name=None, host=None):
    # Empty dirname: use cwd
    if not name:
        name = os.getcwd()
    # Convert to absolute path
    elif not os.path.isabs(name):
        name = os.path.join(os.getcwd(), os.path.expanduser(name))

    # Find net path for name
    if (name[:5] == "/net/"):
        nfsName = name
    elif (name[:5] == "/gfx/"):
        nfsName = name
    elif (name[:13] == "/tmp_mnt/net/"):
        nfsName = name[8:]
    else:
        host = host or os.uname()[1]
        if (name[:5] == "/usr/"):
            nfsName = "/net/%s_%s" % (host, name[1:])
        elif (name[:2] == "/u" and name[2] in string.digits and name[3] == "/"):
            # u1, u2, etc. disks can be either /net/host_u? or
            # /net/host_usr/u? - test which
            nfsName = "/net/%s_u%c" % (host, name[2])
            if (os.path.isdir(nfsName)):
                nfsName = "/net/%s_u%s" % (host, name[2:])
            else:
                nfsName = "/net/%s_usr%s" % (host, name)
        else:
            nfsName = "/net" + name
    return os.path.normpath(nfsName)


## Return path of "name" relative to "refpath"
def reducepath(name, refpath=None):
    refpath = netpath(refpath or os.getcwd())
    nname = netpath(name)
    common = os.path.commonprefix([refpath, nname])
    if common == refpath:
        name = nname[len(common):]
        return name[(name[0] == '/'):]  # strips possible leading '/'
    return name


# tries to find filename alone, then tries joining it to dirname.
# returns '' if it can't be found."""
def findFile(file, dirname='', warn=0):
    import os
    if file:
        if os.path.exists(file):
            return file
        elif dirname:
            if os.path.exists(os.path.join(dirname, file)):
                return os.path.join(dirname, file)
    if warn:
        if dirname:
            dirname = 'in ' + dirname
        print ('ERROR: could not find %s %s' % (file, dirname))
    return ''


# tests to see if obj is an instance of classobj or any subclass
def isA(obj, classobj):
    try:
        raise obj
    except classobj:
        return 1
    except:
        return 0


# compares alphanumerically, so foo9 < foo10.
# return is same as buildin cmp()
def ancmp(astr, bstr):
    # split string like "foo_12aa00" --> "foo_" "12" "aa" "00"
    def striptok(ss):
        n = re.match("[0-9]+", ss)
        if n <= 0: n = re.match("[^0-9]+", ss)
        if n > 0:
            tok = ss[:n]
            ss = ss[n:]
        else:
            return (None, None)

        # convert the token to a number if possible
        try:
            ntok = string.atoi(tok)
            tok = ntok
        except ValueError:
            pass

        return (tok, ss)

    while 1:
        atok, astr = striptok(astr)
        btok, bstr = striptok(bstr)

        if ((atok == None) and (btok == None)):
            return 0
        elif (btok == None):
            return -1
        elif (atok == None):
            return 1
        else:
            res = cmp(atok, btok)
            if not (res == 0):
                return res


# Sorts a list alphanumerically, i.e. prevents [ foo19 foo2 foo20 ]
def ansort(l):
    l.sort(ancmp)


## ##############################################################
# Please GOD let's hope nobody ever tries to use this again...
def v(s):
    i = 0
    s2 = ''
    flag = 0
    length = len(s)
    while i < length:
        if (flag == 0) and (s[i] == '$'):
            flag = 1
            s2 = s2 + "%("
        elif (flag == 1) and (s[i] in (string.digits + string.letters)):
            s2 = s2 + s[i]
        elif flag == 1:
            flag = 0
            s2 = s2 + ")s" + s[i]
        else:
            s2 = s2 + s[i]
        i = i + 1
    if flag == 1:
        s2 = s2 + ")s"

    # force exception to get sys.exc_traceback
    try:
        return s2 % {}
    except KeyError:
        frame = sys.exc_traceback.tb_frame.f_back
        # TODO: this doesn't really fit python "two-scope" philosophy
        while frame != None:
            try:
                return s2 % frame.f_locals
            except KeyError:
                frame = frame.f_back


# Expand strings containing variable substitutions (from Doug MacMillan).
_varprog = None


def expandvars(path, *namespaces, **kw):
    """
    Given a string 'path' expand any patterns $variable and ${variable}.
    Variables that cannot be expanded are left unchanged.
    Altered from os.path.expandvars() so you can provide
    an arbitrary number of optional name spaces, instead of 
    just os.environ.  Any keyword args are used as variables as well.
    Eg.  expandvars('$a ${b} $c',{'a': 'junk','b':'foo'},c=3)
    """
    global _varprog
    if '$' not in path:
        return path
    if not (namespaces or kw):
        import os
        namespace = os.environ
    else:
        namespace = {}
        for n in namespaces:
            for k, v in n.items():
                namespace[k] = v
        for k, v in kw.items():
            namespace[k] = v
    if not _varprog:
        import re
        _varprog = re.compile('$\([a-zA-Z0-9_]+\|{[^}]*}\)')
    i = 0
    while 1:
        i = _varprog.search(path, i)
        if i < 0:
            break
        name = _varprog.group(1)
        j = i + len(_varprog.group(0))
        if name[:1] == '{' and name[-1:] == '}':
            name = name[1:-1]
        if namespace.has_key(name):
            tail = path[j:]
            path = "%s%s" % (path[:i], namespace[name])
            i = len(path)
            path = path + tail
        else:
            i = j
    return path
