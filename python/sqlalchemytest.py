#!/usr/bin/env python

from sqlalchemy import create_engine, MetaData
import webconfig 
dbfile = webconfig.SQLALCHEMY_DATABASE_URI

eng = create_engine(dbfile)
# rs = con.execute("SELECT VERSION()")
# print rs.fetchone()
print(eng.table_names())


#q = eng.execute("SHOW TABLES")
#available_tables = q.fetchall()
#print (available_tables)


print ("Metadata:")

metadata = MetaData()
metadata.reflect(bind=eng)
for table in metadata.sorted_tables:
    print(table)

