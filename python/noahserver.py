import sys,os
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from app import application
import webconfig
import tornado

if __name__ == '__main__':
    port = int(os.environ.get('PORT', webconfig.SERVER_PORT))
    print ("%s:%d"%(webconfig.SERVER_ADDRESS,webconfig.SERVER_PORT))
    application.listen(str(port),webconfig.SERVER_ADDRESS)
    tornado.ioloop.IOLoop.instance().start()
