import sys, os
print (os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from app import app,db
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.script import Manager

manager = Manager(app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

if __name__ == "__main__":
        manager.run()
