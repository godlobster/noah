import os, sys, re
import argparse
import Utilities
import platform
import arkconfig
import arkdb
import collections

def printhelp(parser):
	perr = os.sys.stderr.write
	# perr("usage:  "+parser.usage+"\n")
	# perr( "\tark "+"-/-h[elp] - Print Usage\n")
	# perr("\n")
	# perr( parser.description+"\n\n")
	# perr( parser.example+"\n\n")
	# perr( "options:\n")
	perr(parser.format_help())
	perr("\n\n"+parser.example)
	perr ("\n")
#	for opt in parser.option_list:
#		perr( "\t"+opt.help+"\n")

if __name__ == '__main__':
    # ------------------------------------------------------------------------
    #  parse the args
    # ------------------------------------------------------------------------
    nlevels=20
    opts = ("-complete","-command","-list","-info",
            "-filter","-prefix","-pre","-children","-list",
            "-help","-type","-cd","-query","-init",
            "-clear","-verbose","-querypath","-addlibrary",
            "-updatelibrary","-sourceenv")
    opts = opts + tuple(map(lambda x:"-arklevel%d"%x,range(0,nlevels)))
    Utilities.singledashargs(opts)

    usage = "noah [options] [-complete|-command] prefix/descriptors "
    description = ""
    example = ("example: \n"
           "    Navigation\n"
           "         noah shot01\n"
           "         noah weta avatar shot01\n"
           "         noah /therark/proj/show/scene/shot\n"
           "         noah .\n\n"
           "    Completions\n"
           "         noah shot -complete -filter element\n\n"
           "    Database Init/Update\n"
           "         noah -addlibrary /theark/projects\n"
           "         noah -updatelibrary /theark/projects\n"
           "         noah -init -verbose")
    parser = argparse.ArgumentParser(usage=usage,
                     description=description)
    parser.example=example

    # -- options
    parser.add_argument("descriptors", nargs="*", help="")
    parser.add_argument("--complete",
              help="-complete.  Do completion mode - return list for shell completions",
              action="store_true",
              default = False)
    parser.add_argument("-l","--list",
              help="-list  Print the list of findings.",
              action="store_true",
              default = False)
    parser.add_argument("--command",
              help="-command  Print the findings as a shell command for eval.",
              action="store_true",
              default = True)
    parser.add_argument("--info",
              help="-info Print the whole list of the database..",
              action="store_true",
              default = False)
    parser.add_argument("-f", "--filter", nargs="*",
              help="-f[ilter] apply filter by type (project,task,group ...)",
              default = "")
    parser.add_argument("--prefix",
              "--pre", nargs="?",
              help="-pre[fix] <prefix> Search for a prefix for pattern matching.  Eg. shot returns shot01,shot02 etc...",
              default=None)
    parser.add_argument("-c", "--children",
                        help="-c[hildren] show only all the children/descendents (recursive).",
                        action="store_true",
                        default=False)
    for i in range(0,nlevels):
        parser.add_argument("--arklevel%d"%i,
                            nargs="?",
                            help="level %d name."%i)
    parser.add_argument("--type", nargs="?",
              help="-type for a given element we can specify a 'type'.  eg. _shots,_assets.",
              default="")
    parser.add_argument("--cd",nargs = "?",help="-cd specify a direct path",
                default="")
    parser.add_argument("-q","--query",nargs="*",help="-q[uery] info",
                default="")
    parser.add_argument("--querypath",nargs="?",help="-query id",
                        default="")
    parser.add_argument("--init",action="store_true",help="-initialize database",
                default=False)
    parser.add_argument("--addlibrary",nargs="?",help="-add library path")
    parser.add_argument("--updatelibrary",nargs="?",help="-update library path")
    parser.add_argument("--clear",action="store_true",help="-clear database",
                default=False)
    parser.add_argument("--verbose",action="store_true",help="-verbose",
                default=False)
    parser.add_argument("--sourceenv",action="store_true",help="-sourceenv source the environment of when using 'command' mode",
                default=False)

    if (len(sys.argv) == 2 and (sys.argv[1] == "-" or sys.argv[1] in ("-h","-help"))):
        printhelp(parser)
        sys.exit(2)

    options = parser.parse_args()
    argsv = options.descriptors

    if not options.prefix:
        descriptors = (argsv and argsv[:-1]) or []
        searchstring = (argsv and argsv[-1]) or ""
    else:
        descriptors = (argsv or [])
        searchstring = ""

    thefilter = options.filter
    prefix = options.prefix
    children = options.children
    grouptype = options.type

    levels = collections.OrderedDict()
    for i in range(0,nlevels):
        l = getattr(options,"arklevel%d"%i)
        if l:
            levels["arklevel%d"%i] = l

    if (len(sys.argv) == 2 and sys.argv[1] in (".", "./")):
        options.cd = os.getcwd()

    data = None

    # ------------------------------------------------------------------------
    #  build/query the database
    # ------------------------------------------------------------------------

    db = arkdb.ArkDb(verbose=options.verbose)

    if options.clear:
        db.clear()
        sys.exit(1)

    if options.init:
#        arkdb.clear()
        db.create()
        sys.exit(1)

    if options.updatelibrary:
        db.updatelibrary(options.updatelibrary)
        sys.exit(1)

    if options.addlibrary:
        db.addlibrary(options.addlibrary)
        sys.exit(1)

    if options.query == [] or options.query:
        doentries=dousers=dovolumes=dohosts=True
        if len(options.query):
            doentries= "entries" in options.query
            dousers="users" in options.query
            dovolumes="volumes" in options.query
            dohosts="hosts" in options.query
            
        db.printdb(doentries=doentries,
                   dousers=dousers,
                   dovolumes=dovolumes,
                   dohosts=dohosts,
                   deep=True)
        sys.exit(1)

    if options.querypath:
        query = options.querypath
        if len(query) > 1 and query[0] == "/": query = query[1:]
        if len(query) > 1 and query[-1] == "/": query = query[:-1]
        result = db.queryone({"path":query},db.dbentries, deep=True)
        if result:
            print ("ArkEntry Class data -")
            result.printdata()
            print ("\nDatabase data -")
            for key,value in result.getdata().items():
                print ("%15s:"%key,value)

        else:
            print ("Not found.")
        sys.exit(1)

    # ------------------------------------------------------------------------
    #  search the database
    # ------------------------------------------------------------------------

    # -- prepare mode we're going to be in

    # this is an explicit mode.  go directly to the path.
    arkroot = arkconfig.LOCALVOLUME["arkroot"]
    if (options.cd or \
        (len(sys.argv) > 1 and \
         os.path.exists(sys.argv[1]) and \
         sys.argv[1][0] == "/" and sys.argv[1] != "/") or \
        (len(sys.argv) == 1)):
        volume = Utilities.getValueIfKeyExists(os.environ,
                                               "ARKVOLUME",
                                               platform.node())
        cd = options.cd
        if not cd:
            if len(sys.argv) ==1:
                cd = Utilities.getValueIfKeyExists(os.environ,"ARKDIR",".")
                cd = re.sub("^"+arkroot,"",cd)
            else:
                cd = sys.argv[1]
                cd = re.sub("^"+arkroot,"",cd)

        else:
            cd = re.sub("^"+arkroot,"",cd)
        if len(cd) > 1 and cd[0] == "/": cd = cd[1:]
        if len(cd) > 1 and cd[-1] == "/": cd = cd[:-1]

        entry = db.queryone({"path":cd},db.dbentries, deep=True) or None
        data = [entry]
        cmd = arkdb.shellcmd(data,*argsv)
        if entry:
            arkdb.ark2cmd(db,data, cmd, searchstring, *descriptors)
        else:
            cmd.errornotfound()
        print (cmd.getcmd())
        sys.exit(1)

    # completion and info mode
    if options.complete or options.info:
        thefilter = options.filter or [None]
        if options.prefix == None:
            prefix = "*"

    # command mode - we search database for an entry and return a cd command
    elif options.command:
        thefilter = options.filter
        prefix,children = None,None

        # figure out a searchstring, this is the last not null
        # element of the environment unless we specified a specific project
        #
        if (not searchstring and not levels):
            # use the first valid element
            currentenviron = arkdb.getCurrentEnvironment()
            descriptors = currentenviron
            searchstring = ""
            i = 0

            while (not searchstring) and currentenviron:
                searchstring = currentenviron[-1]
                currentenviron=currentenviron[:-1]

            descriptors = list(filter(lambda x: x,descriptors))[:-1]

    # -- Resolve the environment
    if not descriptors:
        currentenviron = arkdb.getCurrentEnvironment()
        descriptors = list(filter(lambda x: x,currentenviron))[:-1]

    # -- Add the explicit options passed in as arguments
    tosearchin = collections.OrderedDict()
    for i in range(0,nlevels):
        l = getattr(options,"arklevel%d"%i)
        if l:
            tosearchin["arklevel%d"%i] = l
    # we prefer to stay in grouptype
#    grouptype = grouptype or Utilities.getValueIfKeyExists(os.environ,"ARKGROUPTYPE")

    # perform the search
    if not data:
        data = arkdb.searchdb(db,thefilter,
                        prefix,
                        children,
                        searchstring,
                        grouptype,
                        *descriptors,
                        **tosearchin)

        # # filter only entries in this volumes
        # arkvolumeslocal = [ x.id \
        #                    for x in map(db.findarkvolume, [x["name"] for x in arkconfig.ARKVOLUMESLOCAL])]
        #
        # # print ("arkvolumeslocal",arkvolumeslocal)
        # # for x in data:
        # # print ([x["name"] for x in arkconfig.ARKVOLUMESLOCAL])
        # data = [x for x in data if len (arkdb.intersect(list(map(lambda y:y.id,x.arkvolumes)),
        #                                                 arkvolumeslocal))]

    # process thedata
    if searchstring == "/":
        data = [db.gettoplevel()]

    if options.complete:
        current = None
        if ("ARKCURRENT") in os.environ:
            id = os.environ["ARKCURRENT"]
            current = db.queryone({"path":id},db.dbentries,deep=True)

        data.sort(lambda x,y:cmp(x.type,y.type))
        arkdb.printdata(data,current)

    elif options.info:
        arkdb.printdatainfo(data)

    elif options.command:
        cmd = arkdb.shellcmd(data,*argsv)
        arkdb.ark2cmd(db,data, cmd, searchstring, *descriptors)
        print (cmd.getcmd())
        