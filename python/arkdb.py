import os, re, string
import Utilities
import platform
from arkinfo import SIDEBARCMD
import urllib
import json
import arkconfig
import webconfig
import shutil
import requests

import app
from app.models.arkvolume import Library

from utils import encodeforweb
import defaults

import sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
import app.models.arkentry
import glob

_DEBUG = True
IMAGESUFFIXES = ["jpg","png","gif","tif","tga","jpeg"]

def getthumbnail(filename,ext="jpeg"):
	return encodeforweb(filename,ext)

def DEBUG(x):
	if _DEBUG:
		print (x)

# aliases
def printl(l,n=0):
	for x in l:
		print (x,)
		if n: print ()
	print
def printll(ll):
    for x in ll:
        for y in x:
            print (y, end = '')
    print
def printlp(l):
	for x in l:
		printl( x.getpath())
	print
    
def intersect(seq1, seq2, attr=None):
	seq1 = [x for x in seq1 if x]
	seq2 = [x for x in seq2 if x]
	result = []                     # start empty
	if attr:
		seq2 = list(map(lambda x:getattr(x,attr),seq2))
	for x in seq1:
		test = x
		if attr:
			test = getattr(x,attr)
		if test in seq2:            # common item?
			result.append(x)        # add to end
	return result

# where the 2nd is a list of strings, the first is a list of arkentries
def intersect2(seq1, seq2, attr=None):
	seq1 = [x for x in seq1 if x]
	seq2 = [x for x in seq2 if x]
	result = []                     # start empty
	for x in seq1:
		test = x
		if attr:
			test = getattr(x,attr)
		if test in seq2:            # common item?
			result.append(x)        # add to end
	return result

def issubset(a,b):
	if a == b:
		return 1
	# return if a is a subset of b
	if len(a) < len(b):
		for i in range(len(b)-len(a)):
			if a == b[i:len(b)-1]:
				return 1
	return 0

def getrelativepath(a,b,realpath=False):
	if realpath:
		a = os.path.realpath(a)
		b = os.path.realpath(b)
	relative = re.sub("^"+a,"",b)
	if relative and relative[0] == "/" and len(relative) > 1:
		relative= relative[1:]
	return relative

def intersectpath(a,b,realpath=True):
    # return intersection point and two branches,
#    if a[-1] != "/": a=a+"/"
#    if b[-1] != "/": b=b+"/"
    if realpath:
        a = os.path.realpath(a)
        b = os.path.realpath(b)

    asplit = string.split(a,"/")
    bsplit = string.split(b,"/")

    spl = [asplit,bsplit][len(asplit) > len(bsplit)]
    n = 0
    for i in range(0,len(spl)):
        if bsplit[i] != asplit[i]:
            break
        n=i
    if n == 0:
        return ("",a,b)
    else:
        n=n+1
        return "/".join(asplit[0:n]),"/".join(asplit[n:]), "/".join(bsplit[n:])

def Combinations(set,length):
	if not set or not length or len(set) < length:
		yield (())
	elif len(set) == length:
		yield tuple(set)
	elif len(set) > length:
		Z = (set[0],)
		for c in Combinations(set[1:], length-1):
			yield Z+c
		for c in Combinations(set[1:],length):
			yield c

def isemptydict(dict):
	if not dict:
		return True
	
	for x in dict.values():
		if x:
			return False
	return True

def isSuperGroup(groupname):
	return (groupname[0] == "_" or groupname in ARKGROUPTYPES)

def getsubdirectories(path):
	dirs = os.listdir(path)
	dirs = [y for y in dirs if y[0] != "."]
	dirs = list(map (lambda y: os.path.join(path,y),dirs))
	dirs = [x for x in dirs if (os.path.isdir(x) or os.path.islink(x))]
	return dirs

def is_subdir(path, directory):
    path = os.path.realpath(path)
    directory = os.path.realpath(directory)
    relative = os.path.relpath(path, directory)
    return not relative.startswith(os.pardir + os.sep)

class DataSubmit:
    def __init__(self,data,model,url=webconfig.SERVER_URL):
        self.data = data
        self.model = model()
        self.url=url
        if type(data) == type({}):
            for key in data.keys():
                setattr(self.model,key,data[key])

    def sendrequests(self, SEND_URL, post=False):
        try:
            if post:
                response = requests.post(SEND_URL,
                                         data=json.dumps(self.data),
                                         headers={"Content-Type" : "application/json"})
            else:
                response = requests.get(SEND_URL,
                                        params=self.data)
        except urllib.request.URLError:
            print ("echo URLError - Error contacting server:", SEND_URL, "\\\\n")
            return None

        message = response.text
        if response.status_code in (500, 401, 400,405):
            print("Error:", post, SEND_URL, self.data)
            print(message)
            data = {"Error":message,
                    "data":{}}

        else:
            data = json.loads(message or "{}")

        return data

    def sendadd(self):
        SEND_URL = self.url + '/api/%s/add'%self.model.url
        data = self.sendrequests(SEND_URL, post=True)

        return data and data["data"] and data["data"][0]
	
    def sendquery(self):
        SEND_URL = self.url + '/api/%s/query'%self.model.url
        return (self.sendrequests(SEND_URL))

    def sendqueryids(self):
        SEND_URL = self.url + '/api/%s/queryids'%self.model.url
        return (self.sendrequests(SEND_URL))


    def sendquerybyregex(self):
        SEND_URL = self.url + '/api/%s/querybyregex'%self.model.url
        return (self.sendrequests(SEND_URL))

    def sendupdate(self,updatedata):
        SEND_URL = self.url + '/api/%s/update'%self.model.url
        self.data["_updatedata"] = updatedata
        return (self.sendrequests(SEND_URL, post=True))


class ArkLevel:
    def __init__(self,level):
        self.level=level
        if level < len(arkconfig.DEFAULTCONFIG["arklevels"]):
            self.name = arkconfig.DEFAULTCONFIG["arklevels"][level]
        else:
            self.name = "level%d"%self.level
        self.environmentvar = "ARKLEVEL%d"%self.level
        self.environmentdirvar = "ARKLEVEL%dDIR"%self.level
        self.environmentnamevar = "ARK%s"%(self.name.upper())
        self.environmentnamedirvar = "ARK%sDIR"%(self.name.upper())
        
    def printdata(self):
        print ("%15s"%"name:",self.name)
        print ("%15s"%"level:",self.level)
        print ("%15s"%"environmentvar:",self.environmentvar)
        print ("%15s"%"environmentdirvar:",self.environmentdirvar)
        print ("%15s"%"environmentnamevar:",self.environmentnamevar)
        print ("%15s"%"environmentnamedirvar:",self.environmentnamedirvar)

class ArkEntryPrefs(object):
    def __init__(self, db=None,id=None, arkentryid=None, thumbnail=None,poster=None,status=None,
                 longname="",shortname="",description="",welcomemessage="",applications=[], tags=[]):
        self.db = db
        self.id = id
        self.arkentryid = arkentryid
        self.thumbnail = thumbnail
        self.poster = poster
        self.status = status
        self.longname = longname
        self.shortname = shortname
        self.description = description
        self.welcomemessage = welcomemessage
        self.applications = applications
        self.tags

    def getdata(self):
        data = {"id": self.id,
                "arkentryid":self.arkentryid,
                "thumbnail": self.thumbnail,
                "poster": self.poster,
                "status":  self.status,
                "longname":  self.longname,				
                "shortname":self.shortname,
                "description": self.description,
                "welcomemessage": self.welcomemessage,
                "tags": self.tags,
                }
        return data

    def getattributes(self):
        return self.getdata().keys()

    def getquery(self):
        if self.id:
            return {"id":self.id}
        else:
            return {"arkentryid":self.arkentryid}

    def insert(self):
        return self.db.insert(self,self.db.dbprefs)

    def update(self,data):
        return self.db.update(self.getquery(),data,self.db.dbprefs)

class ArkEntry(object):
    def __init__(self, name = None, path=None, fullpath=None, 
                 db=None, level=0,parent=None,children=[],prefs=None,
                 arkvolumes=[],id=None,thumbnail=None, library=None, users=[],
                 subscribedusers=[]):

        for key in app.models.arkentry.ArkEntry.getcolumns():
            setattr(self,key,None)

        self.id = id #
        self.name = name # the shortname
        self.path = path  # local relative path
        self.fullpath = fullpath  # full path - arkvolumeroot + path
        self.arkvolumes = arkvolumes 
        self.parent = parent
        self.children = children
        self.level=level
        self.db = db
        self.arklevel = ArkLevel(level)
        self.type = "arklevel%d"%level
        self.prefs=prefs
        self.thumbnail = thumbnail
        self.users=users
        self.subscribedusers = subscribedusers
        self.library = library

        if self.level < len(arkconfig.DEFAULTCONFIG["arklevels"]):
            self.friendlytype = arkconfig.DEFAULTCONFIG["arklevels"][self.level]
        else:
            self.friendlytype = "level%d"%self.level

    def __str__(self):
        return "%s(%s):%s"%(self.name,self.id,self.path)

    def getquery(self):
        if self.id:
            return {"id":self.id}
        else:
            return {"name":self.name,
                    "path": self.path}

    def resolvedb(self):
        query = self.db.query(self.getquery(),self.db.dbentries)
        if not self.id:
            self.id = query[0].id
			
        if self.arkvolumes and type(self.arkvolume[0]) == int:
            self.arkvolumes = query[0].arkvolumes

        return True
	
    def printdata(self):
        keys = self.__dict__.keys()
        for key in keys:
            if key == "parent":
                if self.parent and type(self.parent) == int:
                    print ("%15s" % "parent:", self.parent ,flush=True)
                else:
                    print ("%15s" % "parent:", self.parent and self.parent.name, flush=True)
            elif key == "children":
                print ("%15s"%"children:",list(map(lambda x: x["name"],self.children or [])), flush=True)
            else:
                print ("%15s"%("%s:"%key),getattr(self,key), flush=True)

    def insert(self):
        newentry = self.db.insertentry(self)

        if newentry:
            self.id = newentry["id"]
            self.prefs = newentry["prefs"]
            self.arkvolumes = newentry["arkvolumes"]
            return newentry

    def getdata(self):
        keys = app.models.arkentry.ArkEntry.getcolumns()
        data={}
        for key in keys:
            data[key] = getattr(self,key)
        # data = {"id": self.id,
        #         "name":self.name,
        #         "parent": self.parent,
        #         "children": self.children or [],
        #         "path":  self.path,
        #         "fullpath":  self.fullpath,
        #         "level":self.level,
        #         "arkvolumes": self.arkvolumes,
        #         "prefs": self.prefs,
        #         "thumbnail": self.thumbnail,
        #         "library": self.library,
        #        }
        if self.path:
            path = self.path
            for n in range(self.level,-1,-1):
                data["arklevel%d"%n] = os.path.basename(path)
                path = os.path.dirname(path)
        return data

    def getattributes(self):
        return self.getdata().keys()
	
    def getdirectory(self):
        return self.fullpath

    def getarkpath(self):
        path = []
        parentpath = os.path.dirname(self.path)
        parent = self.getparent()
        if parent:
            path.append(parent)
        while parentpath and self.arkvolumes and parentpath != self.arkvolumes[0].getprefix():
            parentpath = os.path.dirname(parentpath)
            parent = parent.getparent()
            parent and path.append(parent)
            if not parent:
                break

        return path

    def getparent(self):
        if self.parent:
            return self.parent
		
        parentpath = os.path.dirname(self.path)
        arkentry = None
        if parentpath and len(self.arkvolumes) and parentpath != self.arkvolumes[0].getprefix():
            record = self.db.queryone({"path":parentpath},
                                      self.db.dbentries)
            return record

    def getparentoftype(self,type):
        fullpath = self.getarkpath()
        index = 0
        parent = fullpath[-1]
        while (abs(index) < len(fullpath) and type != parent.type):
            index = index - 1
            parent = fullpath[index]

        return parent	

    def addchild(self,childid):
        childquery = self.db.queryone({'id':childid},
                                      self.db.dbentries)
        if childquery:
            self.children.append(childquery.id)
            self.db.update({"id":self.id},
                           {"children":self.children},
                           self.db.dbentries)

    def getchildren(self):
        if not self.children:
            return []
        else:
            if type(self.children[0]) == int:
                arkchildren = list(map(self.db.findbyid,self.children))
                return arkchildren
            else:
                return self.children

    def getalldescendants(self):
        if not self.children:
            return []
		
        morechildren = []
        children = self.getchildren()
        for x in children:
            if x:
                morechildren = morechildren + x.getalldescendants()
        return children + morechildren

    def getvalue(self):
        return self.id

    def ischild(self,arkentry):
        if arkentry.id == self.parent:
            return 1
        if self.parent:
            return self.getparent().ischild(arkentry)
        else:
            return 0

    def getpreferences(self):
        if self.prefs and type(self.prefs) == int:
            self.prefs = self.db.queryone({"id":self.prefs},self.db.dbprefs)
        return self.prefs
    
    def isHidden(self):
        return self.type == ArkHidden.type

class ArkVolume:
    def __init__(self, db=None,
                 name="", 
                 arkroot="",
                 mountpoint="/Volumes",
                 host=None,
                 libraries=[],
                 id=None,
                 storagetype=None):
        self.id=id
        self.db = db
        self.name = name # essentially disk name
        self.arkroot = arkroot  # the root path for the ark formerly arkhome
        self.mountpoint = mountpoint
        self.libraries=libraries
        self.host = host
        self.storagetype=storagetype

    def builddb(self):
        # populate local volumes by reading the directories
        # starting from arkroot
        for library in self.libraries:
            self.buildlibrary(library)
       # self._populate(self.arkroot,library,0)

    def buildlibrary(self,library):
        librarypath = os.path.join(self.arkroot,library["path"])
        return self._populate(librarypath,library,0)
    
    def updatelibrary(self,library):
        librarypath = os.path.join(self.arkroot,library["path"])
        return self._populate(librarypath,library,0)
        
    def getdata(self):
        data = {"id":self.id,
                "name":self.name,
                "arkroot":  self.arkroot,
                "mountpoint": self.mountpoint,
                "host":self.host,
                "storagetype":self.storagetype}
        return data

    def getattributes(self):
        return self.getdata().keys()
	
    def getquery(self):
        if self.id:
            return {"id":self.id}
        else:
            return {"name":self.name}

    def getprefix(self):
        return self.name+":"+self.arkroot
       
    def _islevel(self,root,d,dolevelfilter=arkconfig.LEVELFILTER):
        d=os.path.join(root,d)
        levelname = re.sub("^"+self.arkroot+"/","",d)
        dirname = os.path.basename(levelname)
        if dirname in arkconfig.ARKEXCLUDE:
       #     print "excluding",dirname
            return False
        for x in arkconfig.EXCLUDEREGEX:
            if re.search(x,dirname) != None:
              #  print "excluding",levelname, re.search(x,dirname)
                return False
            elif re.search(x,levelname) != None:
           #     print "excluding",levelname, re.search(x,levelname)
                return False
            
        if dolevelfilter:
            if not levelname in arkconfig.ALLLEVELS:
         #       print "excluding alllevels ",levelname
                return False
        #print "keeping",levelname 
        return True

    def insert(self):
        entrydata = self.db.insertvolume(self)
        if type(entrydata) == type({}):
            self.id = entrydata["id"]
        else:
            self.id = entrydata.id

    def resolvedb(self):
        query = self.db.query(self.getquery(),self.db.dbvolumes)
        if not self.id:
            self.id = query[0].id

    def _populate(self, path, library, parent=None, tags=[]):
        # given a path, walk its subdirs and populate db
        DEBUG( "\narkvolume._populate: volume=%s"%self.name)
        DEBUG( "arkvolume._populate: walking library path \"%s\"..."%path) 
        allsubdirs = []
        for root, dirs, files in os.walk(path, followlinks=True,topdown=True):
            dirs[:] = [os.path.join(root,d) for d in dirs if self._islevel(root,d)]
            allsubdirs = allsubdirs + \
                list(map(lambda x:os.path.join(re.sub("^"+self.arkroot+"/","",root),x),dirs))
        allsubdirs.sort()

        DEBUG( "arkvolume._populate: Done! Stepping through subdirectories...")

        # the initial toplevel entry
        DEBUG("arkvolume._populate: inserting topvolume - %s"%path)
        name = os.path.basename(path)
        relativepath = getrelativepath(self.arkroot,path)
        arkentry = ArkEntry(name=name,
                            path = relativepath,
                            fullpath=path,
                            arkvolumes=[self.id],
                            db=self.db,
                            level=0,
                            parent=None,
                            children=[],
                            library=library["id"],
                            users=[],
                            subscribedusers=[])
        arkentry.insert()

        for current in allsubdirs:
            # -- figure out the parent, because the list is ordered it should've been 
            #    previously entered in DB
            parent = None
            level = 1
            currentpath = current
            while currentpath:
                p = os.path.dirname(currentpath)
                if p == self.arkroot:
                    break
                id = re.sub("^"+self.arkroot,"",p)
                if len(id) > 1 and id[0] == "/":
                    id = id[1:]

                arkdata = self.db.queryone({"path":id},
                                           self.db.dbentries)
                if arkdata:
                    level = arkdata.level + 1
                    parent = arkdata
                    break
                    
                currentpath = p
                if currentpath == "/":
                    break

            volumepath = self.arkroot
            if volumepath == current:
                continue
            relativepath = getrelativepath(volumepath,current)

            # --------- ARKENTRY ----------- #

            # -- edit parent, add to it's children
            name = os.path.basename(current)
            arkentry = ArkEntry(name=name,
                                path = relativepath,
                                fullpath=current,
                                arkvolumes=[self.id],
                                db=self.db,
                                level=level,
                                parent=parent and parent.id,
                                children=[],
                                library=library["id"],
                                subscribedusers=[])

            # ingest thumbnail data
            if os.path.exists("_data/%s.png"%name):
                arkentry.thumbnail = getthumbnail("_data/%s.png"%name,"png")
            elif os.path.exists("_data/%s.jpg"%name):
                arkentry.thumbnail = getthumbnail("_data/%s.jpg"%name)
            elif os.path.exists("%s/_images/thumbnail.jpg"%current):
                arkentry.thumbnail = getthumbnail("%s/_images/thumbnail.jpg"%current)
            elif os.path.exists("%s/_images/thumbnail.png"%current):
                arkentry.thumbnail = getthumbnail("%s/_images/thumbnail.png"%current)

            # arcbook file data
            jsondata = current+"/"+"arcdata.json"
            if os.path.exists(jsondata):
                with open(jsondata) as json_data:
                    print (name,": adding jsondata",jsondata)
                    d = json.load(json_data)
                    for key in d.keys():
                        if key in self.db.dbentries.getcolumns():
                            setattr(arkentry, key, d[key])

            newentry = arkentry.insert()

            PROJECTS = defaults.getprojects()  # - inherit PROJECTS data
            if newentry:
                # -- Set ArkEntryPrefs
                # if PROJECTS.has_key(arkentry.name) and PROJECTS[arkentry.name].has_key("applications"):
                #     applications = PROJECTS[arkentry.name]["applications"]
                # else:
                #     applications = self.db.query({"id":"*"},
                #                                  self.db.dbapplications)
                # applications = [ (type (a) == dict and a["id"]) or a for a in applications]
                # arkentryapps = []
                # for app in applications:
                #     arkentryapp = self.db.insert({"application":app,
                #                                   "arkentry":arkentry.id},
                #                                  self.db.dbarkentryapps)
                #     arkentryapps.append(arkentryapp)
											   		
                updateprefs = {"longname":(name in PROJECTS and
                                           PROJECTS[name]["longname"]) or name,
                               "shortname":name,
                               "welcomemessage":((name in PROJECTS) and \
                                                 ("welcomemessage" in PROJECTS[name]) and \
                                                 PROJECTS[name]["welcomemessage"]) or ""
                          #     "applications":[x["id"] for x in arkentryapps]
                              }
                if os.path.exists("_data/%s.png"%name):
                    updateprefs["thumbnail"] = getthumbnail("_data/%s.png"%name)
                elif os.path.exists("_data/%s.jpg"%name):
                    updateprefs["thumbnail"] = getthumbnail("_data/%s.jpg"%name)
                if os.path.exists("_data/%s-poster.png"%name):
                    updateprefs["poster"] = getthumbnail("_data/%s-poster.png"%name)
                elif os.path.exists("_data/%s-poster.jpg"%name):
                    updateprefs["poster"] = getthumbnail("_data/%s-poster.jpg"%name)
                elif ("thumbnail" in updateprefs ):
                    updateprefs["poster"] = updateprefs["thumbnail"]

                # -- Set TAGS
                if tags:
                    tagids = []
                    for tag in tags:
                        print ("Searching tag %s"%tag)
                        tagentry = self.db.queryone({"name": tag},
                                                    self.db.dbtags)
                        if tagentry:
                            tagids.append(tagentry["id"])
                        else:
                            print ("Adding tag %s"%tag)
                            newtag = self.db.insert({"name":tag},
                                           self.db.dbtags)
                            tagids.append(newtag["id"])
                    updateprefs["tags"] = tagids

                # -- Copy images
                sourceimagedir = current+"/_images"
                if os.path.exists(sourceimagedir):
                    targetimagedir = "app/static/images/%s-%s.%s"%(self.db.dbentries.url,
                                                      arkentry.name,
                                                      arkentry.id)
                    userimageindex = 0
                    for imagefile in os.listdir(sourceimagedir):
                        if imagefile.split(".")[-1] in IMAGESUFFIXES and userimageindex <= 10:
                            if not os.path.exists(targetimagedir):
                                os.makedirs(targetimagedir)

                            targetfilename = targetimagedir+"/"+imagefile
                            shutil.copy(sourceimagedir+"/"+imagefile,
                                        targetfilename)
                            if "thumbnailanimated" in imagefile.lower():
                                updateprefs["thumbnailanimatedfilename"] = targetfilename.decode("utf-8").replace("app/","")
                            elif "thumbnail" in imagefile.lower():
                                updateprefs["thumbnailfilename"] = targetfilename.decode("utf-8").replace("app/","")
                            elif "poster" in imagefile.lower():
                                updateprefs["posterfilename"] = targetfilename.decode("utf-8").replace("app/","")
                            elif "banner" in imagefile.lower():
                                updateprefs["bannerfilename"] = targetfilename.decode("utf-8").replace("app/","")
                            elif "background" in imagefile.lower():
                                updateprefs["backgroundfilename"] = targetfilename.decode("utf-8").replace("app/","")
                            else:
                                updateprefs["userimage%dfilename"%userimageindex] = targetfilename.decode("utf-8").replace("app/","")
                                userimageindex += 1

                # -- Commit preferences
                prefsid = arkentry.getpreferences()["id"]
                self.db.update({"id": prefsid},
                               updateprefs,
                               self.db.dbprefs)

                # # -- Copy Assets
                # sourceassetdir = current + "/_assets"
                # updatedict={}
                # if os.path.exists(sourceassetdir):
                #     tartgetassetdir = "app/static/assets/%s-%s.%s" % (self.db.dbentries.url,
                #                                                       arkentry.name,
                #                                                       arkentry.id)
                #     if not os.path.exists(tartgetassetdir):
                #         os.makedirs(tartgetassetdir)
                #
                #     i = 0
                #     assets=[]
                #     try:
                #         assets = sorted(next(os.walk(sourceassetdir))[1])
                #     except StopIteration:
                #         pass  # Some error handling here
                #
                #     for asset in assets: #sorted(next(os.walk(sourceassetdir))[1]):
                #         #only grab fbx/objs
                #         objfile = glob.glob(sourceassetdir+"/"+asset+"/*.obj")
                #         fbxfile = glob.glob(sourceassetdir+"/"+asset+"/*.fbx")
                #         modelfile = (objfile or fbxfile)
                #
                #         # textures
                #         textures = []
                #         for ext in ('*.gif', '*.png', '*.jpg',"*.txt","*.tif","*.tga"):
                #             textures.extend([x for x in glob.glob(sourceassetdir+"/"+asset+"/"+ext) if not "/preview." in x.lower()])
                #
                #         # Materials
                #         materials = glob.glob(sourceassetdir+"/"+asset+"/*.mtl")
                #
                #         if modelfile:
                #             modelfile=modelfile[0]
                #             targetfilename = tartgetassetdir + "/" + asset
                #             if os.path.exists(targetfilename):
                #                 os.system("rm -rf %s"%targetfilename)
                #             cmd = "cp -rf '%s' '%s'" % (sourceassetdir + "/" + asset,
                #                                    targetfilename)
                #             print(cmd)
                #             os.system(cmd)
                #             updatedict["assetmodelfilename%d"%i] = tartgetassetdir[4:] +"/"+ asset +"/" + os.path.basename(modelfile)
                #
                #             if textures:
                #                 updatedict["assettexturesfilename%d" % i] = ""
                #                 for texture in textures:
                #                     updatedict["assettexturesfilename%d" % i] += tartgetassetdir[4:] + "/" + asset + "/" + os.path.basename(texture) +";"
                #
                #             if materials:
                #                 updatedict["assetmaterialsfilename%d" % i] = ""
                #                 updatedict["assetmaterialsfilename%d" % i] += tartgetassetdir[4:] + "/" + asset + "/" + os.path.basename(materials[0])
                #
                #             # preview image
                #             previewimages = glob.glob(sourceassetdir+"/"+asset+"/preview.*")
                #             if previewimages:
                #                 updatedict["assetpreviewfilename%d" % i] = tartgetassetdir[4:] + "/" + asset + "/" + os.path.basename(previewimages[0])
                #
                #             i=i+1
                #         else:
                #             print ("No valid FBX or OBJ found")
                #
                # # -- copy arcdata
                # datafile = current + "/arcdata.json"
                # if os.path.exists(datafile):
                #     targetdir = "app/static/assets/%s-%s.%s" % (self.db.dbentries.url,
                #                                                 arkentry.name,
                #                                                 arkentry.id)
                #     if not os.path.exists(targetdir):
                #         os.makedirs(targetdir)
                #
                #     targetfilename = targetdir + "/" + os.path.basename(datafile)
                #     shutil.copy(datafile,
                #                 targetfilename)
                #     updatedict["arcdatafilename"] = targetfilename.decode("utf-8").replace(
                #         "app/", "")

                # if updatedict:
                #     self.db.update({"id":arkentry.id},
                #                    updatedict,
                #                    self.db.dbentries)


            else:
               # print "arkdb._populate: skipped entry",arkentry.name
                continue

            if parent and arkentry.id:
                parent.addchild(arkentry.id)
        
        return True

class ArkDb:
    """ web app API... all database connections and queries through here """
    ARKLEVEL_ENV_VAR = "ARKLEVEL"
	
    def __init__ (self, url=webconfig.SERVER_URL,verbose=arkconfig.VERBOSE):        
        self.dbentries = app.models.arkentry.ArkEntry
        self.dbprefs = app.models.arkentry.ArkEntryPrefs
        self.dbvolumes = app.models.arkvolume.ArkVolume
        self.dbhosts = app.models.arkvolume.ArkHost
        self.dblibraries = app.models.arkvolume.Library
        self.dbtags = app.models.arkentry.Tag
        self.dbusers = app.models.user.User
        self.dbapplications = app.models.application.Application
        self.dbarkentryapps = app.models.application.ArkEntryApplication
        self.dbentriescache = {}
        self.verbose = verbose
        self.url=url
        if self.verbose:
            print ("arkdb.init: Connecting to server: %s"%url)

    def create(self):
        print ("\narkdb.create: creating database.")

        # -- create applications from defaults
        arkvolumes = []
        APPLICATIONS = defaults.getapplications()
        if self.verbose:
            print ()
            print ("arkdb.create: processing default applications ")
        for application in APPLICATIONS.keys():
            entrydata = APPLICATIONS[application]
            self.insert(entrydata, self.dbapplications)
        if self.verbose: print ()

        # -- create tags from defaults
        for tag in defaults.gettags():
            if self.verbose:
                print ("arkdb.create: adding tag %s"%list(tag)[0])
            self.insert(tag[list(tag)[0]], self.dbtags)

        # -- create users from defaults
        for user in defaults.getusers():
            if self.verbose:
                print ("arkdb.create: adding user %s"%user["username"])
            self.insert(user, self.dbusers)

        # -- add this host
        hostname = platform.node()
        if self.verbose:
            print ("arkdb.create: adding this host --> ", hostname)
            print ()
        hostjson = DataSubmit({"name":hostname.split(".")[0],
                               "hostname": hostname,
                              },self.dbhosts,self.url).sendadd()
        if not hostjson:
            return
        
        # -- set the volumes
        for arkvolumelocal in arkconfig.ARKVOLUMESLOCAL:
            if self.verbose:
                print ("arkdb.create: adding volume --> ", arkvolumelocal["name"])
            arkvolume = ArkVolume(db=self,
                                  name=arkvolumelocal["name"],
                                  arkroot=arkvolumelocal["arkroot"],
                                  libraries=arkvolumelocal["libraries"],
                                  host = hostjson["id"],
                                  storagetype="Internal")
            arkvolume.insert()
            arkvolume.resolvedb()
            arkvolumes.append(arkvolume)
        
        if self.verbose:
            print

        # -- now build the libraries for each of these volumes
        for arkvolume in arkvolumes:
            if self.verbose:
                print ("arkdb.create: processing volume --> ", arkvolume.name, arkvolume.id)

            # create library
            for library in arkvolume.libraries:
                libraryjson = DataSubmit({"path":library["path"],
                                          "name": library["name"],
                                          "arkvolumes":[arkvolume.id],
                                         },Library,self.url).sendadd()
                if not libraryjson:
                    return
                
                print ("arkdb.create: building library --> ", library["path"])
                arkvolume.buildlibrary(libraryjson)

    def find_mount_point(self,path):
        path = os.path.realpath(path)
        while not os.path.ismount(path):
            path = os.path.dirname(path)
        return path

    def addlibrary (self, librarypath):
        hostname = platform.node()
        name = hostname.split(".")[0]
        arkhost = self.query({"name":name},
                             app.models.arkvolume.ArkHost)
        if not arkhost:
            # create host
            print ("arkdb.addlibrary: creating host - "+hostname)
            arkhost = DataSubmit({
                                  "name":name,
                                  "hostname":hostname
                                 },app.models.arkvolume.ArkHost,
                                self.url).sendadd()
            if not arkhost:
                return
        else:
            print ("arkdb.addlibrary: using host "+hostname)
            arkhost= arkhost[0]
            
        arkvolumename = None
        if platform.platform().startswith("Darwin"):
            volumes = os.listdir("/Volumes")
            for v in volumes:
                if is_subdir(librarypath, os.path.join("/Volumes",v)):
                    arkvolumename = v

        arkvolume = self.query({"name":arkvolumename,
                                "host":arkhost["id"]},self.dbvolumes) or \
                    self.query({"name":arkvolumename},self.dbvolumes) 

        if not arkvolume:
            print ("arkdb.addlibrary: creating volume - "+arkvolumename )

            arkvolume = ArkVolume(db=self,
                                  name=arkvolumename,
                                  arkroot=os.path.dirname(librarypath),
                                  storagetype="Internal",
                                  host = arkhost["id"])

            arkvolume.insert()
            arkvolume.resolvedb()
        else:
            arkvolume=arkvolume[0]
            print ("arkdb.addlibrary: using volume - "+arkvolume.name   )
        
        library = self.query({"path": librarypath,
                              "arkvolume":arkvolume.id},
                        Library)
        if library:
            print ("arkdb.addlibrary: error - library already exists --> %s:%s"%(arkvolume.name,
                                                                      librarypath))
            return False
        
        # create library
        librarypath=os.path.realpath(librarypath)
        libraryjson = DataSubmit({"path":librarypath,
                                  "name":os.path.basename(librarypath),
                                  "arkvolumes":arkvolume.id
                                 },Library,self.url).sendadd()
        if not libraryjson:
            return
        print ("arkdb.addlibrary: library created - "+librarypath)
        if self.verbose:
            print ("arkdb.addlibrary: populating library - ", librarypath)

        return arkvolume.buildlibrary(libraryjson)

    def updatelibrary (self, libraryname, volume=None):
        hostname = platform.node()
        arkhost = self.query({"name":hostname.split(".")[0].lower()},
                             app.models.arkvolume.ArkHost)
        if not arkhost:
            # create host
            print ("arkdb.updatelibrary: creating host - "+hostname)
            arkhost = DataSubmit({
                                  "name":hostname.split(".")[0],
                                  "hostname":hostname
                                 },app.models.arkvolume.ArkHost,self.url).sendadd()
            if not arkhost:
                return
        else:
            print ("arkdb.updatelibrary: using host - "+hostname )
            arkhost= arkhost[0]
        
        if not volume:
            volume = arkconfig.LOCALVOLUME
            
        arkvolume = self.query({"name":volume["name"],
                                "host":arkhost["id"]},
                               self.dbvolumes) or \
                    self.query({"name":volume["name"]},
                               self.dbvolumes)

        if not arkvolume:
            print ("arkdb.updatelibrary: creating volume - "+volume["name"]    )

            arkvolume = ArkVolume(db=self,
                                  name=volume["name"],
                                  arkroot=("arkroot" in volume) and volume["arkroot"] or arkconfig.ARKROOT,
                                  storagetype=("storagetype" in volume) and volume["storagetype"] or "Internal",
                                  host = arkhost["id"])
            arkvolume.insert()
            arkvolume.resolvedb()
        else:
            arkvolume=arkvolume[0]
            print ("arkdb.updatelibrary: using volume - "+arkvolume.name )
        
        library = self.queryone({"name": libraryname},
                                Library)
        if library:
            print ("arkdb.updatelibrary: using library --> %s"%(library["path"]))
            if not arkvolume.id in [x["id"] for x in library["arkvolumes"]]:
                # need to add this volume to library
                print ("arkdb.updatelibrary: adding volume %s to library"%(arkvolume.name))
                arkvolumes = [x["id"] for x in library["arkvolumes"]]
                arkvolumes.append(arkvolume.id)
                libraryjson = DataSubmit({"id":library["id"]},
                                         Library,self.url).sendupdate({"arkvolumes":arkvolumes}) 
                if not libraryjson:
                    return
                
            return arkvolume.updatelibrary(library)
        else:
            return False
    
    def getentrypath(self,defined=False):
        path = []
        for n in range(0, len(arkconfig.DEFAULTCONFIG["arklevels"])):
            var = "arklevel%d"%n
            if defined:
                (var.upper() in os.environ) and path.append(var)
            else:
                path.append(var)
        return path

    def printdb(self,dovolumes=True,
                doentries=True,
                dohosts=True,
                dousers=True,
                deep=False):
        print ("""____________________________""")
        print ("""|                           |""")
        print ("""|        arkdb.printdb      |""")
        print ("""|___________________________|""")
        print ()
        port = int(os.environ.get('PORT', webconfig.SERVER_PORT))
        print ("webconfig:", webconfig.SERVER_ADDRESS, port, webconfig.SQLALCHEMY_DATABASE_URI)

        if dousers:
            print ("_______________________   USERS   ________________________")
            users = self.query({"id":"*"}, app.models.user.User, deep=deep)
            for u in users:
                _printdata(u)
                print ()
            print ()

        if dohosts:
            print ("_______________________ ARKHOSTS _____________________")
            hosts = self.query({"id":"*"},self.dbhosts, deep=deep)
            for h in hosts:
                _printdata(h)
                print ()
            print ()

        if dovolumes:
            print ("_______________________ ARKVOLUMES ___________________")
            volumes = self.query({"id":"*"},self.dbvolumes, deep=deep)
            for v in volumes:
                _printdata(v)
                print()
            print ()
            print ("_______________________ LIBRARIES ___________________")
            libraries = self.query({"id":"*"},self.dblibraries, deep=deep)
            for l in libraries:
                _printdata(l)
                print()
            print()

        if doentries:
            print ("_______________________ ARKENTRIES ____________________")
            entries = self.query({"id":"*"}, self.dbentries, deep=deep)
            for entry in entries:
                print ("\t----- ArkEntry %s:%s -----"%(entry.name,entry.id), flush=True)
                _printdata(entry)
                prefs = entry.getpreferences()
                if prefs:
                    print ("\t   (ArkEntryPrefs)")
                    _printdata(prefs, tab=1, filterout=("arkentry",))
                print()

    def insert(self, classdata, db):
        if type(classdata) != type({}):
            querydata = classdata.getquery()
            data = classdata.getdata()
        else:
            querydata = classdata
            data = classdata

        if "password" in querydata: del querydata["password"]
        if "administrator" in querydata: del querydata["administrator"]
        if "thumbnailfilename" in querydata: del querydata["thumbnailfilename"]
        query = self.query(querydata,db)
        if query:
            if self.verbose:
                name = "name" in data and data["name"] or "username" in data and data["username"] or data.keys()
                print ("arkdb.insert: %s already exists.  skipping"%name)

            return

        if self.verbose:
            if ("name" in data):
                print ("arkdb.insert: %s"% data["name"])
        else:
            print ("arkdb.insert: failed to insert")

        return DataSubmit(data,db,self.url).sendadd()

    def insertvolume(self, volume):
        query = self.queryone(volume.getquery(),self.dbvolumes)
        if query:
            print ("arkdb.insertvolume: %s,%s already found"%(query.name,query.id))
            return query.getdata()
		
        if self.verbose:
            print ("arkdb.insertvolume: %s"%volume.name)

        return DataSubmit(volume.getdata(),self.dbvolumes,self.url).sendadd()

    def insertentry(self,arkentry):
        query = self.queryone(arkentry.getquery(),
			      self.dbentries)
        if query:
            if not arkentry.id:
                arkentry.id = query.id
            volumes = query.arkvolumes
            volumeints = []
            for v in volumes:
                val = v
                if type(val) != int:
                    val = val.id

                if val not in volumeints:
                    volumeints.append(val)

            if arkentry.arkvolumes[0] in volumeints:
                if self.verbose:
                    print ("arkdb - entry already found.  Skipping: %s"%query.path)
                return
            else:
                # entry already there.  update arkvolumes
                if self.verbose:
                    print ("arkdb - updating entry %s, adding volume: %s"%(query.name,arkentry.arkvolumes[0]))
                    
                volumes.append(arkentry.arkvolumes[0])
                query.arkvolumes.append(arkentry.arkvolumes[0])
                volumeints = []
                for v in volumes:
                    val = v
                    if type(val) != int:
                        val = val.id
                    if val not in volumeints:
                        volumeints.append(val)

                arkentry.arkvolumes = volumeints
                data = self.update({"id":query.id},
                                   {"arkvolumes":volumeints},
                                   self.dbentries)
            return None
			
        data = arkentry.getdata()
        if self.verbose:
            print ("arkdb.insertentry: %s"%arkentry.path)
        data = arkentry.getdata()
        entrydata = DataSubmit(data,self.dbentries,self.url).sendadd()
        return entrydata

    def update(self,query,updatedata,db):
        result = DataSubmit(query,db,self.url).sendupdate(updatedata)
        return result

    # --- querying database
    def query(self,query,db, deep=False):
        # query database and return arkdata
        data = DataSubmit(query, db, self.url).sendquery()
        if db == self.dbentries:
            arkdata = self._querytoarkdata(data,ArkEntry, deep=deep)
        elif db == self.dbvolumes:
            arkdata = self._querytoarkdata(data,ArkVolume, deep=deep)
        elif db == self.dbprefs:
            arkdata = self._querytoarkdata(data,ArkEntryPrefs, deep=deep)
        else:
            arkdata = data and data["data"]
        return arkdata

    def queryone(self, query, db, deep=False):
        # query database and return arkdata
        data = self.query(query,db,deep=deep)
        if data:
            return data[0]
        else:
            return None

    def querybyregex(self,query,db, deep=False):
        data = DataSubmit(query,db,self.url).sendquerybyregex()
        if db == self.dbentries:
            arkdata = self._querytoarkdata(data,ArkEntry, deep=deep)
        else:
            arkdata = self._querytoarkdata(data,ArkVolume, deep=deep)
        return arkdata

    def querybyid(self,id,db=None, deep=False):
        # return all elements that match the following
        data = DataSubmit({"id":id}, db,self.url).sendquery({"id":id})
        if not data:
            return None
        if db == self.dbentries:
            arkdata = self._querytoarkdata(data, ArkEntry, deep=deep)
        else:
            arkdata = self._querytoarkdata(data, ArkVolume, deep=deep)
        if arkdata:
            return arkdata[0]
        else:
            return None

    def querybyids(self,ids,db=None, deep=False):
        # return all elements that match the following
        data = DataSubmit(ids,db,self.url).sendqueryids()
        if not data:
            return None
        if db == self.dbentries:
            arkdata = self._querytoarkdata(data,ArkEntry, deep=deep)
        else:
            arkdata = self._querytoarkdata(data,ArkVolume, deep=deep)
        return arkdata

    def _querytoarkdata(self, data, arkclass, usecache=True, deep=False):
        """return a list of arkdata classes"""
        if not data:
            return
        
        result = []
        data = data["data"] or []

        for record in data:
            if arkclass == ArkEntry:
                if (record["id"] in self.dbentriescache) and usecache:
                    result.append( self.dbentriescache[record["id"]])
                    continue

            arkdata = arkclass(db=self)
            attributes = arkdata.getattributes()
			
            for attribute in attributes:
                if attribute == "parent" and ("parent" in record):
                    parent = record["parent"]
                    if deep:
                        if record["parent"] and type(record["parent"]) == int:
                            parent = self.getarkentry(record["parent"], deep=deep)
                        setattr(arkdata,attribute,parent)
                        continue

                elif attribute == "arkvolumes" and ("arkvolumes" in record):
                    if deep:
                        arkvolumes = [ self.getarkvolume(x["id"]) for x in record["arkvolumes"]]
                        setattr(arkdata,attribute,arkvolumes)
                        continue

                elif attribute == "prefs" and ("prefs" in record):
                    if deep:
                        arkdata.prefs = record["prefs"]
                        arkdata.getpreferences()
                        continue

                elif attribute == "level" and ("level" in record):
                    if deep:
                        arkdata.arklevel = ArkLevel(record["level"])
                        arkdata.level = record["level"]
                        arkdata.getpreferences()
                        continue
					
                elif (attribute in record):
                    setattr(arkdata,attribute,record[attribute])

            if arkclass == ArkEntry:
                self.dbentriescache[record["id"]] = arkdata

            result.append(arkdata)

        return result
	
    def find(self,name,**args):
        # return all elements that match the following:
        # name is an entryname or a partial name or list of names
        # if name is a list of names we return the 'smart intersection'
        # eg.  
        # args is a dictionary of type to value (project->name,etc)
        query = args
        if name and isinstance(name, str):
            query["name"] = name

        elif name and type(name) in (list,tuple) and len(name) == 1:
            query["name"] =  name[0]

        # todo - regex in sql web api
        #		elif name and type(name) in (list,tuple):
        #		 	for n in name[1:]:
        #				regex = regex+"%s/.*"%name[i-1]
        #				tosql = tosql.filter(or_(self.dbentries.name.op('-')(regex)))
		
        result = self.query(query,self.dbentries)
        return result

    def findbyprefix(self, prefix, **args):
        if prefix == "*":
            query = {'name': "*"}
        else:
            query = {'name': "%s"%prefix+"%"}

        if args:
            keys = args.keys()
            keys.sort()
            for x in keys:
                if x.startswith("arklevel"):
                    if ("path" in query):
                        query["path"] = query["path"] +args[x] +"/%"
                    else:
                        query["path"] = "%"+args[x] +"/%"
                else:
                    query[x] = args[x]
        arkdata = self.querybyregex(query,
									self.dbentries) or []

		# filter out current
        keys = args.keys()
        for x in arkdata:
            if keys and x.type == keys[0]:
                if args[keys[0]] == x.name:
                    arkdata.remove(x)

        return arkdata

    def findarkvolume(self,name):
        return self.queryone({"name":name},
                             self.dbvolumes)

    def findbyid(self,id):
        return self.queryone({"id":id},
						self.dbentries)

    def getarkvolume(self,id):
        if not id:
            return
        return self.queryone({"id":id},
                             self.dbvolumes)

    def getarkentry(self,id,usecache=True, deep=False):
        if not id:
            returns

        if self.dbentriescache and (id in self.dbentriescache) and usecache:
            result = self.dbentriescache[id]
        else:
            result  = self.queryone({"id":id},self.dbentries,deep=deep)
        return result

    def getentries(self):
        entries = self.query({"name":"*"},
                             self.dbentries)
        return entries

    def gettoplevel(self):
        defaultlibrary = arkconfig.LOCALVOLUME["libraries"][0]
        query = {"path":"%s"%(defaultlibrary["path"])}
        return self.queryone(query,self.dbentries)

    def addfrompath(self, volume, path):
        volume = self.queryone()

class shellcmd:
    def __init__(self,data,*args):
        self.isvalid = 0
        self.args = args
        self.cmds = []

        self.data = data

    def setdata(self,data,validflag):
        self.clear()
        self.data = data
        if validflag==1:
            self.printdata(data)
			
    def printdata(self,data):
        self.addentrycmd(self.data[0])

    def addcmd(self,cmd):
        self.cmds.append("%s"%cmd)

    def addentrycmd(self,arkentry):
        self.clear()
			
        # cmds to set the environment
        self.exportenv(arkentry)
        self.exportprefsenv(arkentry)
        self.echoentry(arkentry)
		
        targetdir = arkentry.fullpath
        targetdir = re.sub(" ","\ ",targetdir)
        self.addcmd ("echo %s; cd %s"%(targetdir,targetdir))

        self.setvalid(True)

    def clear(self):
        del self.cmds[:]

    def getcmd (self):
        return ";".join(self.cmds)

    def setvalid(self,isvalid):
        self.isvalid = isvalid

    def echoinfo(self, arkpath):
        if not arkpath:
            return
        cmd = "print %s: %s"%(arkpath[0].arklevel.name,arkpath[0].name)
        if len(arkpath) > 1:
            i=1
            for current in arkpath[1:]:
                cmd = cmd +"\ \ \ ;echo %s: %s"%(current.arklevel.name,current.name)
                i=i+1
			
        self.addcmd(cmd)

    def echoentry(self, entry):
        arkpath = entry.getarkpath()
		
        self.echoinfo(arkpath+[entry])

    def echoinfofromdict(self, **args):
        l = PROTOTYPE
        output = "echo "
        for x in l:
            if (x in args):
                output = output + "%s: %s\ \ \ "%(x,args[x])
        self.addcmd(output)
		
    def setenv(self, entry):
        self.addcmd("setenv ARKCLIENT %s"%entry.getclient() or "")
        self.addcmd("setenv ARKPROJECT %s"%entry.getproject() or "")
        self.addcmd("setenv ARKTASK %s"%entry.gettask() or "")
        self.addcmd("setenv ARKELEMENT %s"%entry.getelement() or "")
        self.addcmd("setenv ARKSHOT %s"%entry.getelement() or "")
        self.addcmd("setenv ARKGROUP %s"%entry.getgroup() or "")
        self.addcmd("setenv ARKSCENE %s"%entry.getgroup() or "")
        taskdir = re.sub(" ","\ ",entry.gettask().getdirectory())		
        self.addcmd("setenv ARKTASKDIR %s"%etaskdir or "")		

    def clearenv(self):
        for x in range(0,len(arkconfig.DEFAULTCONFIG["arklevels"])):
            if ("ARKLEVEL%d"%x in os.environ):
                self.addcmd("unset ARKLEVEL%d"%x)
                self.addcmd("unset ARKLEVELDIR%d"%x)
        self.addcmd("unset ARKDIR; unset ARKVOLUME")
		
    def exportenv(self, entry):
        self.clearenv()

        path = entry.getarkpath()
        for x in path+[entry]:
            self.addcmd("export %s=%s;export %s=%s"%(x.arklevel.environmentvar,x.name,
                                                     x.arklevel.environmentdirvar,x.fullpath))
            self.addcmd("export %s=%s;export %s=%s"%(x.arklevel.environmentnamevar,x.name,
                                                     x.arklevel.environmentnamedirvar,x.fullpath))

        arkdir = re.sub(" ","\ ",entry.fullpath)
        self.addcmd("export ARKDIR=%s; "%arkdir+\
                    "export ARKCURRENT=%s"%entry.id)
        self.addcmd("export ARKELEMENT=%s"%entry.name)
        if len(entry.arkvolumes):
            self.addcmd("export ARKVOLUME=%s; " % entry.arkvolumes[0].name)
        if len(path) > 0:
            self.addcmd("export ARKGROUP=%s"%path[-1].name)
        else:
            self.addcmd("export ARKGROUP=%s"%"")

    def exportprefsenv(self, entry):

        preferences = entry.getpreferences()
        if preferences:
            environment = preferences["environment"]
            if environment:
                self.addcmd("export ARKENVFROMPREFS='%s'"%environment)
                self.addcmd("eval $ARKENVFROMPREFS")
        

    def errorambiguous(self):
        self.clear()
        self.addcmd("echo Error: %s ambiguous"%" ".join(self.args))

        self.done = {}
        for x in self.data:
            if not (x in self.done):
                self.addcmd("echo \t%s"%x.fullpath)
            self.done[x] = True

    def errornotfound(self,search=None):
        self.clear()
        if not search:
            search = " ".join(self.args)
        self.addcmd("echo Error: %s not found"%search)		
			
    def clear(self):
        self.setvalid(0)
        self.cmds = []

class ArkEntryCommand:
    def __init__(self,arkentry):
        self.arkentry = arkentry

    def sidebarcmd(self):
        lines = os.popen(SIDEBARCMD).readlines()
        newlines = []
        for line in lines:
            newlines.append(line.strip())

        newlines.append("%d\tarkdir\t%s"%(len(lines)+1,
                                          self.arkentry.fullpath))
        stream = open ("/tmp/arkdb.sidebar.txt","w")
        for l in newlines:
            stream.write(l+"\n")
        stream.close()

			
def searchdb(ark,
			 thefilter=None,
			 prefix=None,
			 getchildren=False,
			 searchstring=None,
			 grouptype=None,
			 *descriptors,
			 **tosearchin):
    """filter is a filter by type, i.e. arklevel #- its a list of  matching types
	descriptors is a list of parents at any level.  descriptors must be ordered
	eg.  descriptors = ["project0", "client0", "element1"]
	 """
    result = []

    if searchstring or prefix or tosearchin:
        # -- find all prefix matches
        if prefix:
            if prefix=="*" and searchstring:
                prefix=searchstring
            matches = ark.findbyprefix(prefix,**tosearchin) or []

            # if we're using a prefix need to use searchstring as descriptor
            descriptors = list(descriptors)
            searchstring and descriptors and descriptors.append(searchstring)
        else:
            matches = ark.find(searchstring,**tosearchin) or []

        primarychildren = flatten(list(map(lambda x: x.children,matches)))

        # -- descriptors
        #    these are any the descendants of matches in the database at any level given a 'descriptor'
        #    the final result is the intersection of all the descriptor sets that match the given searchstring/prefix
        # find the sets of the descriptors.  these are the supersets
        # need to build hiearchy here... i.e. descriptor[1] matches needs to be
        # children of descriptor[0] matches
        def isvalidchild(element, l):
            # l is a list of possible parents
            for x in l:
                if element.ischild(x):
                    return 1
            return 0
		
        supersets = {}

        if descriptors and getchildren:
            # the intersection of all the children of the descriptors and all the primary children
            queryand = {"$and":[]}
            regex = "%"
            for d in descriptors:
                regex += "%s/%s"%(d,"%")
            queryd = {"path":regex}
            queryand["$and"].append(queryd)

            if primarychildren:
                queryor = {"$or":[]}
                for child in primarychildren:
                    queryor["$or"].append({"id":child})
                queryand["$and"].append(queryor)

                results = ark.querybyregex(queryand,ark.dbentries, deep=True)

        elif descriptors:
            # intersection of all the children of the descriptors and the matches themselves
            regex = "%"
            for d in descriptors:
                regex += "%s/%s"%(d,"%")
            queryd = {"path":regex}
            # just step through matches and filter out who's not contained in descriptor
            supersets = ark.querybyregex(queryd,ark.dbentries)
            results = intersect(supersets,matches,"path")#filter(lambda x:isvalidchild(x,supersets),matches)
        else:
            if getchildren:
                results = flatten(list(map(lambda x:x.getalldescendants(),matches)))
            else:
                results = matches

    else:
        # no search defined.  filter through all entries
        results = ark.getentries()

    # prefer "grouptype" which is a hidden supergroup like _shots or _assets
    # but if not in grouptype, revert back to original search
    if grouptype:
        grouptype = string.lower(grouptype)
        def _isoftype(x,grouptype=grouptype):
            if not x.type in ["group","element"]:
                return 1
            if not x.gethidden():
                return 0
            supergroup = string.lower(re.sub("_","",x.gethidden().getvalue()))
            grouptype = re.sub("_","",grouptype)
            singular = (supergroup[-1] == "s") and supergroup[:-1]
            return grouptype in [supergroup, singular]
        grouptypeelems = [x for x in results if _isoftype(x)]
        results = grouptypeelems or results

    # need to filter again with dict
#	if not isemptydict(tosearchin) and not prefix:
#		elems = ark.filterwithdict(elems,None,**tosearchin)
	# filter  it for the type we're searching for
    if thefilter and thefilter != [None]:
        filtered = [x for x in results if (x.type in thefilter)]
        return filtered
    else:
        return results

def printdata(data,current=None):
    if not data:
        return
    prevtype = data[0].type
    friendlytype = arkconfig.DEFAULTCONFIG["arklevels"][data[0].level]
    print ("___%s___"%friendlytype,)
    for x in data:
        if 1 or x.type != prevtype:
            if len(arkconfig.DEFAULTCONFIG["arklevels"]) < (x.level-1):
                friendlytype = arkconfig.DEFAULTCONFIG["arklevels"][x.level]
                print ("___%s___"%friendlytype,)
            else:
                print ("___%s___"%x.type,)
        name = re.sub(" ","\ ",x.name)
#		name = x.name
        print (name,)
        prevtype = x.type

def _printdata(entry,tab=0,filterout=[], deep=False):
    if type(entry) == dict:
        data = entry
    else:
        data = entry.getdata()

    keys = list(data.keys())
    keys.sort()

    for key in keys:
        if key in filterout:
            continue

        print ("\t"*tab,end='')
        print ("%25s: "%key, end='')
        if key == "parent":
            if data["parent"]:
                print (data["parent"].id,":",data["parent"].name)
            else:
                print ("")
        else:
            if key == "thumbnail":
                print (data[key] and "..." or "None")
                #print data[key] or ""
            elif key == "prefs":
                print ("...")
            elif key == "arkvolumes":
                print (str(list(map(lambda x:str(hasattr(x,"name") and x.name or x["name"]),
                                    data[key]))))
            elif type(data[key]) == list:
                if len(data[key]) and type(data[key][0]) == dict:
                    print (str([(str(x["id"])+":"+((("name" in x)
                                                   and str(x["name"])) or ""))
                                for x in data[key]]))
                else:
                    print ("[]")
            else:
                print (data[key])

def printdatainfo(data):
    for entry in data:
        print("\t----- ArkEntry %s:%s -----" % (entry.name, entry.id))
        _printdata(entry)
        prefs = entry.getpreferences()
        if prefs:
            print("\t   (ArkEntryPrefs)")
            _printdata(prefs, tab=1, filterout=("arkentry",))
        print


def flatten(l, limit=900, counter=0):
    for i in range(len(l)):
        if (isinstance(l[i], (list, tuple)) and
            counter < limit):
          for a in l.pop(i):
              l.insert(i, a)
              i += 1
          counter += 1
          return flatten(l, limit, counter)
    return l

def printUniqueCombinations(alist,numb,blist=[]):
    if not numb: return blist
	

def findonematch(db,data,searchstring,*descriptors):
    # given a set of data and a list of descriptors, use different
    # test cases to see if there's a unique match
    # #order of search
    searchorder = db.getentrypath(defined=True)
    searchorder.reverse()
    origdesc = list(descriptors)
    descriptors = list(descriptors)
    descriptors.reverse()

    # get the testcases
    if len(descriptors) == 1:
        testcases = list(map(lambda x:(x,),searchorder))
    else:
        testcases = list(Combinations(searchorder,len(descriptors)))

    # test data.
    def testelement(testcase,element):
        # for a given element, see if it matches up with the testcase
        # if it does its still valid
        valid = 1
        i=0
        for t in testcase:
            if t:
                if element.type != t:
                    matchingtype = element.getparentoftype(t)
                else:
                    matchingtype = element
                if not matchingtype:
                    valid = 0
                    break
                if (matchingtype.name != descriptors[i]):
                    valid = 0
                    break
            i+=1
        return valid

    # see if each element passes any of the testcases.  if it does then its valid.
    stillvalid = []
    for element in data:
        valid =0
        for test in testcases:
            if not test:
                stillvalid.append(element)
            valid = testelement(test,element)
            if valid:
                stillvalid.append(element)

    # still too many maches, do more testing
    if (len(stillvalid) > 1 or not stillvalid):
        def explicitmatches(data,desc):
            # lets check for explicit matches i.e.
            # project generic is project->generic
            e = []
            for x in data:
                n = len(x.getarkpath()) - len(origdesc)
                descpath = desc+[searchstring]
                endpath = list(map(lambda x:x.name,x.getarkpath()))[n-1:]
                if (descpath==endpath):
                    e.append(x)
            return e
        ematches = explicitmatches(data,origdesc)
        if len(ematches) == 1:
            return ematches
        # last ditch effort.  lets check if its all really one path
        # project->generic->generic->generic->
        # return he deepest entry
        def isonetree(data):
            def func(x):
                x.getarkpath()
                data = sorted(data, key = func)
            deepest = data[0]
            for x in data[1:]:
                if not deepest.ischild(x):
                    return 0
            return 1
        if data and isonetree(data):
            return [data[0]]
    return list(set(stillvalid))

def getCurrentEnvironment():
	environment = []
	for x in range(0,len(arkconfig.DEFAULTCONFIG["arklevels"])):
		environment.append(((ArkDb.ARKLEVEL_ENV_VAR+str(x) in os.environ) and
				    os.environ[ArkDb.ARKLEVEL_ENV_VAR+str(x)]) or "")
	return environment

def _prefercurrentvolume(data,localvolume):
    pwd = os.path.realpath(os.getcwd())
    currentvolume = Utilities.find_mount_point(pwd)

    if currentvolume == "/":
        currentvolume = localvolume["name"]
    data = [x for x in data if (x.arkvolumes and x.arkvolumes[0].arkroot.startswith(currentvolume))]

    return data

def ark2cmd(db, data, cmd, searchstring, *descriptors):
    srcdata = data[:]

    # returns the command to "ark to" a given element
    if not (searchstring or descriptors):
        cmd.errornotfound()
    if (len(data) == 1):
        cmd.setdata(data,1)
        return cmd

    elif (len(data) > 1):
        # If the paths are in different volumes, we can 'prefer'
        # the current volume we're in.  TODO - do this after we've processed the environment!
        data = _prefercurrentvolume(data, arkconfig.LOCALVOLUME)
        if not data:
            data = srcdata[:]

        if len(data) == 1:
            cmd.setdata(data,1)
            return cmd
        else:
            # getmatchingelements returned the intersection of all
            # descriptors.  try limiting search to different combinations
            # of descriptors
            onematch = findonematch(db,data,searchstring,*descriptors)
            if (len(onematch) == 1):
                cmd.setdata(onematch,1)
                return cmd

        # ok descriptors didn't find a good match.  Try the environment.
        # and see if the descriptors still work
        # environment = environment[:-1] we rescursively go through environment
        environment = getCurrentEnvironment()
        environment = [x for x in environment if x]  # strip null env

        def quickdesccheck(elem,*descriptors):
            path = list(map(lambda n:n.name,elem.getarkpath()))
            for y in descriptors:
                if not y in path:
                    return 0
            return 1

        # try recursively going through environment to see if we get a match
        # we're in the recursive loop here b/c we're trying the environment
        def tryenvrecurse(data,searchstring,environment, *descriptors):
            onematch = findonematch(db,data,searchstring,*environment)
            if descriptors:
                onematch = [x for x in onematch if quickdesccheck(x,*descriptors) ]
			
            environment = environment[:-1]
            if len(onematch) == 1:
                cmd.setdata(onematch,1)
                return onematch

            if (len(onematch) > 1):
                if not environment:
                    return onematch
            else:
                if not environment:
                    return []
            return tryenvrecurse(data,searchstring,environment,*descriptors)

        onematch = tryenvrecurse(data,searchstring,environment,*descriptors)

        # no hope.  its ambiguous
        if len (onematch) == 1:
            cmd.setdata(onematch,1)
        elif len(onematch) > 1:
            cmd.setdata(onematch,0)
            cmd.errorambiguous()
        else:
            cmd.errorambiguous()
    else:
        cmd.errornotfound()

    return cmd

def intersectl(a, b):
    """ return the intersection of two lists """
    return list(set(a) & set(b))
