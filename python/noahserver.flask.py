import sys,os
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from app import app,application
import webconfig

if __name__ == '__main__':
	app.run(host=webconfig.SERVER_ADDRESS, port=webconfig.SERVER_PORT )
