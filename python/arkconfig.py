import string
import os
import sys
import re
import Utilities
import platform
import yaml

def getbootvolume():
	return "archelon"
#     startupvolume=None
#     try:
#         from Foundation import NSFileManager
#         startupvolume = str(NSFileManager.defaultManager().displayNameAtPath_(u'/'))
#     except ImportError:
# #        startupvolume = os.popen("/usr/sbin/diskutil info `bless --getBoot` |sed '/^ *Volume Name: */!d;s###'").readlines()[0].strip()
#         if platform.system() == "Darwin":
#             startupvolume = os.popen("/usr/sbin/diskutil info \"/\" |sed '/^ *Volume Name: */!d;s###'").readlines()[0].strip()
#         elif platform.system() == "Linux":
#             startupvolume=os.popen("cat /etc/fstab | grep \"/ \" | cut -f 1 -d \" \"").readlines()[0].strip()
#     return startupvolume

# ----------------------------------------------
# - Global Configuration from config file
# ----------------------------------------------

configfile = None
ARKVOLUMESLOCAL = None
LOCALVOLUME = None
LEVELFILTER = False

if "HOME" in os.environ:
    configfile = os.environ["HOME"] +"/.arkconfig"

if os.getcwd().startswith("/app"):
	ENV = "../env"
else:
	ENV = "/theark/dist/projects/frank/theark/code/noah/env"
configfile = ENV + "/dotarkconfig"


# -- source config file
if configfile and os.path.exists(configfile):
	exec(open(configfile).read())

# -- arkvolumeslocal and local volume
ARKROOT = "ARKROOT" in globals() and ARKROOT or "/theark"
LIBRARIES = "LIBRARIES" in globals() and LIBRARIES or [{"name":"projects",
                                                        "path":"projects"}]
if ("LOCALVOLUME" not in globals()) or not LOCALVOLUME:
    LOCALVOLUME = {"name":getbootvolume() or "",
                   "arkroot": ARKROOT,
                   "mountpoint":"/Volumes",
                   "libraries": LIBRARIES
                  }

if ("ARKVOLUMESLOCAL" not in globals()) or not ARKVOLUMESLOCAL:
    ARKVOLUMESLOCAL = [LOCALVOLUME]
if LOCALVOLUME["name"] not in [x["name"] for x in ARKVOLUMESLOCAL]:
    ARKVOLUMESLOCAL = [LOCALVOLUME] + ARKVOLUMESLOCAL

# --- configure globals 
ARKEXCLUDE = (("ARKEXCLUDE" in globals() and ARKEXCLUDE) or []) +\
            ["scriv","docs","maya","xml","msoffice",
             "scenes","nk","nuke","work","vue",
             "fcp","images","export",
             "incoming","rapidweaver",
             "mayaproject","realflow",
             "ai","mudbox","photoshop","textures",
             "txt","mov","build","old","deprecated",
	     "edl","hro","media","avid","EBLaunchServices","cache",
	     "data","dist","icons","doc","docs","examples","versions",
	     "backup","old","clutter","flask-social-example","version",
	     "lib","bin","env","imageflow-master","sidebarFinagler-master-2",
	     "dotconfig","python","PyObjCExample-SimpleService","_tmp","MacNavBar",
	     "src","flask-social-example-master","pyqt","Config",
		"rndr","comp","elem","scripts","logs","maya","mov","tmp","temp",
		"dotnuke","tex","Library","metadata","Packages","Tools","Temp","ThirdParty",
		"Database", "Versions","Version","ProjectSettings","Scripts",
	 	"MayaProjects","NukeProjects","BDMV","MPEG","DVDBUILD","PAR","Cache",
		"Sources","MemorialBlueray","Python","Nuke","Maya","AUDIO_TS",
		"VIDEO_TS","Trailers","Quicktimes","Movie","Movies","movies",
		"Dropbox","Public","pics","research","tga","tif","exr","exrs",
		"final","ae","Final","pcomp","AE","Audio MediaFiles",
		"renderData","ref","ma","autosave","roto","plateCC","plates","plate",
		"jpg","Audio files","Audio Render Files","audio","wip","master",
		"thumb","render","renders","DISC","music","dsp","Audio","Help",
		"Image","SWF","Video","Release","x64","setphotos","photos","precomp",
		"mattePainting","outgoing","incoming","fonts","psd",
		"stills","element","mocha","encore","Avid MediaFiles","sprites",
		"exports","fullresstills","cleanplate","maya2nuke",
		"iPod Photo Cache", "bios","misc","oscommerce","max","thumbs",
		"Contents","Aperture Library","mud","ps","fbx","obj",
		"zbrush","mari","elements","afterEffects","mpeg","mpeg2","DISC_PAL",
		"ScrivenerSync","pdf","fdr","rv","maxproject","Trashed Files","DISC_PAL",
		"assets","images","model","fbx","obj","maya","ps","Assets", "data",
		"garageband","bower_components","ngwidgets-ver1","pysidex","pyside","virenv","virenv.linux","virenv.win" #arcbook
	     ]
EXCLUDEREGEX = ["/\..*",
		"\.scriv",
                "\.RDC",
                "\.mayaSwatches",
                "\.git",
                "\.bak",
                "\.xcodeproj",
		"\.app",
		"/vir_env/",
		"/virenv/",
		"\.log",
		"/tmp",
		"/bak",
		"\.Conflict",
                "\.lproj",
		"\.deprecated",
		"gtk-doc",
		"\.egg-info",
		"\.old",
		"\.wdgtproj",
		"\.nib",
		"iPhoto Library",
		"\.aplibrary",
		"\.colorproj",
		"\.iMovieProject",
		"\.dspproj",
		"iphoto",
		"iPhoto",
		"\.arkconfig",
		"cinzia/memorialRecording",
		"cinzia/memorial/Memorial",
		"cinzia-archives/.*",
		"reel/THE_ONE",
		"reel/Showreel Cinzia",
		"reel/CMEMMO",
		"reel/DVD_ROSE",
		"cinz - 2010/.*",
		"cinz-archives/.*",
		".*DVD",
		"preprod/presentation/.*",
		"unity/.*/Assets",
		"Logs",
		"\.tif",
		"STEELHEAD",
		"chimera/Recorder/.*",
		"chimera/Research/.*",
		"chimera/Web/.*",
		"chimera/Production/Atrax/.*",
		"lucacycle3/.*",
		"frontiermos/.*",
		"fontiermos/.*",
		"\.sync",
		"prodtools/packages/.*",
		"textures",
		"\.RDM",
		"\.key",
		"website/web/2011/.*",
		"vfx/Warehouse/.*",
		"roto_v[0-9]+",
		"deserted/editing/tim/.*",
		"deserted/editing/anders/.*",
		"shoot/shoot.*",
		"share/generic/deadline",
		"spaghettiFX/.*/assets",
		"\.TemporaryItems",
		"cancer/cancer-steve/.*",
		"spaghettiredux/blueray/.*",
		"\.rtfd",
		"weta-dl-.*",
		"sutton/.*/Ripple/.*",
		"spaghetti/preprod/.*",
		"spaghetti/vfx/_shots/test/.*",
		"website/.*/v[0-9]+/.*",
		"darkness\(old\)/Primal/.*",
		"deserted/audio_lancaster2/.*",
		"code/noah/app/.*",
		"afi/transplant/TRANSPLANT.*",
		"base/.*/packages/.*",
		"spaghetti/test/.*",
		"transplant/test/.*",
		"transplant/testf/.*",
#		"^_.*",
		"bower_components/*",
		"pyside",
		"pyside/.*",
		"pysidex/.*",
		"pysidex",
		"ngwidgets-ver1/.*",
		"virenv.*/.*"
		]

ARKGROUPTYPES = (("ARKGROUPTYPES" in globals() and ARKGROUPTYPES) or []) + \
                ["ASSETS","SHOTS","_assets","_shots"]
    
ARKTASKS = (("ARKTASKS" in globals() and ARKTASKS) or []) +\
            ["vfx","editing","screenwriting",
             "directing","bid","legal","dev","web",
             "pub","edit","preprod","art",
             "artwork","artdept","sound",
             "soundfx","audio","shoot","dp",
             "cine","previz","music","writing",
             "design","layout","colorcorrect","colorcorrection",
             "festival","final","dcp","blueray","dvd",
             "code","coding","programming"]

DEFAULTCONFIG = {"arklevels": ["toplevel","client","project",
                               "task","supergroup","group","element"]}

VERBOSE = False

# yaml config
#configfile = "%s/.config/arkinfo-config.yaml"%LOCALVOLUME["arkroot"]
configfile = ENV+"/arkinfo-config.yaml"
if configfile and os.path.exists(configfile):
	stream = open(configfile, 'r')
	yaml.warnings({'YAMLLoadWarning': False})
	CONFIG = yaml.load(stream)

else:
	CONFIG = {}

# todo get rid of this..
#ALLLEVELS =   map(string.strip,
#                  open("/theark/dist/projects/frank/theark/code/theark1.0/arkdb/alllevels.txt").readlines())
#ALLLEVELS.sort()
ALLLEVELS = []
