#!/usr/bin/env python

def pad (n,length):
	zeros = ""
	length = (length+1) - len("%s"%n)
	if (length > 0):
		for x in range(1,length):
			zeros = zeros+"0"
	return zeros+"%s"%n

import sys
if ('__main__' == __name__):
	print (pad(atoi(sys.argv[1]), int(sys.argv[2])))
