import datetime
import os
import json
import string,re

from app.models.arkentry import ArkEntry,ArkEntryPrefs,Tag
from app.models.application import ArkEntryApplication
from app.models.arkvolume import ArkVolume
from app.models.application import Application
from app.models.user import User, Connection
from app.models.arkmodel import ArkModel

from datetime import timedelta
from flask import make_response, request, current_app
from functools import update_wrapper


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

def _json_builder(x):
	ret = {}
	for c in x.keys():
		if c in ("arkvolumes","children"):
			val = getattr(x,c)
			if len(val) and not type(val[0]) == int:
				ret[c] = list(map(lambda x:_json_builder(x),val))
				continue
		ret[c] = x[c]
	return ret
	
def _json_builder_from_model(data,model=ArkEntry,  level=0):
    columns = model.getcolumns()
    result = {}
    for c in columns:
        if not hasattr(data,c):
            continue

        if c in ("arkvolumes","children","prefs","arkentry","applications","arkentryprefs",
                "connections","roles","user","application","tags"):
            val = getattr(data,c)
            
            # these would be backrefs, use id to avoid recursion
            
            if issubclass (val.__class__,ArkModel):
                if level > 2:
                    result[c] = val.id
                else:
                    result[c] = _json_builder_from_model(val,
                                                         model=val.__class__,
                                                         level = level +1 )
                continue
            elif c == "application" and type(val) == int:
                result[c] = _json_builder_from_model(Application.query.get(val),
                                                     Application,
                                                     level=level+1 )
                continue
            elif c == "tags" and type(val) == int:
                result[c] = _json_builder_from_model(Tag.query.get(val),
                                                     Tag,
                                                     level=level + 1)
                continue
            elif type(val) == int:
                result[c] = val
                continue
            elif hasattr(val,"__iter__") and len(val) and not type(val[0]) == int:
                if level > 2:
                    result[c] = list(map(lambda x: getattr(x,"id"),val)      )
                else:
                    result[c] = list(map(lambda x:_json_builder_from_model(x,
                                                               model=x.__class__,
                                                                level=level+1),
                                val))
                continue
            elif hasattr(val,"__iter__") and len(val) and isinstance(val[0],ArkEntryPrefs):
                if level > 2:
                    result[c] = list(map(lambda x: getattr(x,"id"),val)  )
                else:
                    result[c] = list(map(lambda x:_json_builder_from_model(x,
                                                               model=x.__class__,
                                                               level=level+1),
                                 val))
                continue
                
        result[c] = getattr(data,c)
    return result

def getthumbnail(filename):
	return encodeforweb(filename)

def encodeforweb(filename,ext="jpeg"):
    fin = open(filename, "rb")
    #img = filename.read()
    #binary = sqlite3.Binary(img)

    img = fin.read()
    return 'data:image/jpeg;base64,'+img.encode("base64")
