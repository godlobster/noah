from setuptools import setup

##
## 1. sudo apt-get install python-dev
##
required = [
	'sqlalchemy',
	'flask',
	'flask-sqlalchemy',
	'flask-wtf',
	'sqlalchemy-migrate',
	'paste',
	'pyexcel',
	'pyexcel-ods',
	'PyYAML',
	"flask_login",
	"flask_oauth",
	"Flask_Social",
	"google-api-python-client",
	"flask_cors",
	"flask-restless",
	"tornado",
	"bcrypt",
	'flask-migrate',
	'gunicorn',
	'py-trello',
	'flask_triangle',
	'pyperclip'
]
print required
setup(name='Noah',
	version='1.0',
	description='Uber Project Management System',
	author='baltazar',
	install_requires=required
)
