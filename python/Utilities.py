import sys, os
import re
import string


def singledashargs(options):
    # allow for -client etc..
    for i in range(1, len(sys.argv)):
        if sys.argv[i] in options:
            sys.argv[i] = "-" + sys.argv[i]


def printmyhelp(parser, options, example=""):
    perr = os.sys.stderr.write
    args = parser.parse_args()

    if parser.usage:
        perr("usage:  " + parser.usage + "\n")
    perr("\tSLATE " + "-/-h[elp] - Print Usage\n")
    perr("\n")
    if parser.description:
        perr(parser.description + "\n\n")
    if example:
        perr(example + "\n\n")
    perr("options:\n")

    for x in options:
        opt = parser.get_option("-" + x)
        perr("\t" + opt.help + "\n")


def getValueIfKeyExists(dict, key, default=False):
    if key in dict:
        return dict[key]
    return default


def getrelativepath(src, dest):
    src = os.path.realpath(src)
    dest = os.path.realpath(dest)

    # determine intersection
    intersect = []
    sourcepath = src.split("/")
    destpath = dest.split("/")
    i = 0
    for x in sourcepath:
        if x == destpath[i]:
            intersect.append(x)
        else:
            break
        i = i + 1
    intersectpath = string.join(intersect, "/")
    #    intersectpath = intersectpath and ("/"+intersectpath)

    srcbasepath = string.join(sourcepath[sourcepath.index(intersect[-1]) + 1:], "/")

    # returns the relative path of dest to src

    basepath = dest.split(intersectpath)[-1]

    if basepath == dest:
        return basepath

    if basepath[0] == "/":
        basepath = basepath[1:]

    if basepath[-1] == "/":
        basepath = basepath[:-1]

    relativepath = (len(basepath.split("/")) - 1) * "../"

    return os.path.join(relativepath, srcbasepath)


# load a UI file
def loadUiType(uiFile):
    import pysideuic
    import xml.etree.ElementTree as xml
    from cStringIO import StringIO
    import PySide.QtGui as QtGui

    parsed = xml.parse(uiFile)
    widget_class = parsed.find('widget').get('class')
    form_class = parsed.find('class').text
    with open(uiFile, 'r') as f:
        o = StringIO()
        frame = {}

        pysideuic.compileUi(f, o, indent=0)
        pyc = compile(o.getvalue(), '<string>', 'exec')
        exec (pyc in frame)

        form_class = frame['Ui_%s' % form_class]
        base_class = eval('QtGui.%s' % widget_class)
        # Fetch the base_class and form class based on their type in the xml from designer
    return form_class, base_class


def find_mount_point(path):
    if path[0] != "/":
        path = os.path.abspath(path)
    while not os.path.ismount(path):
        path = os.path.dirname(path)
    return path


def setIconWidgetAsPixmap(icon, height, widget, ICONPATH=""):
    from PySide import QtGui, QtCore

    if icon and icon[0] != "/":
        icon = os.path.join(ICONPATH, icon)

    pixmap = QtGui.QPixmap(icon)
    pixmap = pixmap.scaledToHeight(height, QtCore.Qt.SmoothTransformation)
    widget.setPixmap(pixmap)


def setIconWidget(icon, height, widget, ICONPATH=""):
    from PySide import QtGui, QtCore

    if icon[0] != "/":
        icon = os.path.join(ICONPATH, icon)

    pixmap = QtGui.QPixmap(icon)
    pixmap = pixmap.scaledToHeight(height, QtCore.Qt.SmoothTransformation)
    buttonicon = QtGui.QIcon(pixmap)
    widget.setIcon(buttonicon)
    widget.setIconSize(pixmap.rect().size())
    widget.pixmap = pixmap
    widget.icon = buttonicon

    return buttonicon


def isImage(path):
    ext = os.path.splitext(path)[-1]
    print (ext)
    if ext and ext.lower()[1:] in ["jpg", "tif", "bmp", "jpeg", "gif",
                                   "png"]:
        print ("here")
        return True
    return False


if __name__ == "__main__":
    a = "/Volumes/FX/theark/proj/frank/loggerheadbook/vfx/mayaproject/loggerheadbook/lib/character/test10/"
    b = "/Volumes/FX/theark/proj/frank/loggerheadbook/vfx/mayaproject/loggerheadbook/lib/character/test10/../../../../../_assets/character/test10/maya/scenes"
    print (getrelativepath(a, b))




