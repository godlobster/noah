import sys,os
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from webconfig import arkdbfile
from app import db

import os.path

if os.path.exists(arkdbfile):
	os.remove(arkdbfile)

db.create_all()
db.session.commit()
