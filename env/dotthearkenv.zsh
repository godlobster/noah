#cmd
r2() {eval `ark2.1 $*`}
r2p() {eval `ark2.1 -filter project -p $*`}
r2c() {eval `ark2.1 -filter client -client $*`}
r2s() { if [[ $#ARGV > 1 ]]; then 
	    eval `ark2.1 -filter element -e $*`;
	 else
	    eval `ark2.1 $*`;
	 fi
	    }
r2g() {eval `ark2.1 -filter group -g $*`}
r2t() {eval `ark2.1 -filter task -t $*`}

#aliases
alias shot=r2s
alias client=r2c
alias proj=r2p
alias task=r2t
alias sequence=r2g

alias scn='cd $ARKDIR/maya/scenes'
alias scn.l='cd $ARKDIR/maya/scenes/components/light/wip'
alias scn.c='cd $ARKDIR/maya/scenes/components'
alias scn.a='cd $ARKDIR/maya/scenes/components/anim/wip'
alias scn.b='cd $ARKDIR/maya/scenes/components/light/wip/baltazar'
alias rndr='cd $ARKDIR/images/render'
alias elem='cd $ARKDIR/images/elem'
alias shk='cd $ARKDIR/shk'
alias nk='cd $ARKDIR/nk'
alias cmp='cd $ARKDIR/images/comp'
alias mov='cd $ARKDIR/mov'
alias img='cd $ARKDIR/images'
alias scripts='cd $ARKDIR/scripts'
alias logs='cd $ARKDIR/tmp/logs'

alias mkshot='mkdir -p $ARKDIR/rndr $ARKDIR/elem $ARKDIR/nk $ARKDIR/mov $ARKDIR/comp $ARKDIR/scripts $ARKDIR/logs'
 
arkcomplete() { if [[ $PREFIX == '' ]]; then
	      	      	cmd="ark2.1 ${words[2,-2]} -complete -children";
		else
	      	      	cmd="ark2.1 ${words[2,-2]} -complete -prefix $PREFIX";
		fi
#		 echo \\n $cmd
		output=`zsh -c $cmd`;
		n=${#${(s.___.)output}}

		while (( i++ < n)); do
		    group=<${${(s.___.)output}[i++]}
		    query=${${(s.___.)output}[i]}
		    compadd -J $group -X %B$group%b ${(s. .)query} ;
		done
		}

arkcomplete2() { 
                          if [[ -z $ARKCLIENT ]]; then
			        clientstr="";
			  else
			        clientstr="-client $ARKCLIENT";
		          fi
                          if [[ -z $ARKPROJECT ]]; then
			        projectstr="";
			  else
				projectstr="-p $ARKPROJECT";
		          fi
#                           if [[ $ARKGROUP == '' ]]; then
# 			        groupstr="";
# 			  else
# 				groupstr="-g $ARKGROUP";
# 		          fi
			  if [[ $ARKTASK == '' ]]; then
 			        taskstr="";
 			  else
 				taskstr="-t $ARKTASK";
 		          fi

			  if [[ $PREFIX == '' ]]; then
			      prefixstr="";
			      childrenstr="-children";
			  else
			      prefixstr="-prefix $PREFIX";
			      childrenstr="";
			  fi 

			  if [[ $clientstr == '' && $projectstr == '' && ${#words[2,-2]} != 1 ]]; then
					childrenstr="";
			  fi

#                          cmd="ark2.1 ${words[2,-2]} -complete $prefixstr $childrenstr $projectstr $taskstr $groupstr $clientstr ";
                          cmd="ark2.1 ${words[2,-2]} -complete $prefixstr $childrenstr $taskstr $projectstr   $clientstr ";
#			  echo \\n $cmd
			  output=`zsh -c $cmd`;

			  n=${#${(s.___.)output}}

			  while (( i++ < n)); do
			      group=${${(s.___.)output}[i++]}
			      query=${${(s.___.)output}[i]}

			      compadd -J $group -X %B$group%b ${(s. .)query} ;
			  done
		}


arkcompleteshot() { 
                          if [[ -z $ARKCLIENT ]]; then
			        clientstr="";
			  else
			        clientstr="-client $ARKCLIENT";
		          fi
                          if [[ -z $ARKPROJECT ]]; then
			        projectstr="";
			  else
				projectstr="-p $ARKPROJECT";
		          fi
                           if [[ $ARKGROUP == '' ]]; then
 			        groupstr="";
 			  else
 				groupstr="-g $ARKGROUP";
 		          fi
#                           if [[ $ARKTASK == '' ]]; then
# 			        taskstr="";
# 			  else
# 				taskstr="-t $ARKTASK";
# 		          fi

#                           if [[ $ARKELEMENT == '' ]]; then
# 			        elementfilter="";
# 			  else
#				elementfilter="element"
# 		          fi

			  if [[ $PREFIX == '' ]]; then
			      prefixstr="";
			      childrenstr="-children";
			  else
			      prefixstr="-prefix $PREFIX";
			      childrenstr="";
			  fi 

			  if [[ $clientstr == '' && $projectstr == '' && ${#words[2,-2]} != 1 ]]; then
					childrenstr="";
					filterstr="";
#			  elif [[ $projectstr == '' && ${#words[2,-2]} != 1 ]]; then
			  elif [[ $projectstr == '' ]]; then
				filterstr="project task group element";
#			  elif [[ $ARKTASK == '' && ${#words[2,-2]} != 1 ]]; then
			  elif [[ $ARKTASK == '' ]]; then
				filterstr="task group element";
			  elif [[ $ARKGROUP == '' && ${#words[2,-2]} != 1 ]]; then
				filterstr="group element";
			  else
				filterstr="element";
			  fi

#                          cmd="ark2.1 ${words[2,-2]} -complete $prefixstr $childrenstr $projectstr $taskstr $groupstr $clientstr -filter element ";
			
                          cmd="ark2.1 ${words[2,-2]} -complete $prefixstr $childrenstr $projectstr  $clientstr $groupstr -filter $filterstr";
#			  echo \\n $cmd
			  output=`zsh -c $cmd`;
			  n=${#${(s.___.)output}}

			  while (( i++ < n)); do
			      group=${${(s.___.)output}[i++]}
			      query=${${(s.___.)output}[i]}
			      compadd -J $group -X %B$group%b ${(s. .)query} ;
			  done
		}


arkcompletegroup() { 
                          if [[ -z $ARKCLIENT ]]; then
			        clientstr="";
			  else
			        clientstr="-client $ARKCLIENT";
		          fi
                          if [[ -z $ARKPROJECT ]]; then
			        projectstr="";
			  else
				projectstr="-p $ARKPROJECT";
		          fi
			  if [[ $PREFIX == '' ]]; then
			      prefixstr="";
			      childrenstr="-children";
			  else
			      prefixstr="-prefix $PREFIX";
			      childrenstr="";
			  fi 

			  if [[ $clientstr == '' && $projectstr == '' && ${#words[2,-2]} != 1 ]]; then
					childrenstr="";
			  fi

#                          cmd="ark2.1 ${words[2,-2]} -complete $prefixstr $childrenstr $projectstr $taskstr $groupstr $clientstr -filter element ";
                          cmd="ark2.1 ${words[2,-2]} -complete $prefixstr $childrenstr $projectstr  $clientstr -filter group ";
#			  echo \\n $cmd
			  output=`zsh -c $cmd`;
			  n=${#${(s.___.)output}}

			  while (( i++ < n)); do
			      group=${${(s.___.)output}[i++]}
			      query=${${(s.___.)output}[i]}
			      compadd -J $group -X %B$group%b ${(s. .)query} ;
			  done
		}


arkcompleteproj() {if [[ $PREFIX == '' ]]; then
	      	      	cmd="ark2.1 ${words[2,-2]} -complete -filter project";
		else
	      	      	cmd="ark2.1 ${words[2,-2]} -complete -prefix $PREFIX -filter project";
		fi
#		echo \\n $cmd
		output=`zsh -c $cmd`;
		n=${#${(s.___.)output}}

		while (( i++ < n)); do
		    group=${${(s.___.)output}[i++]}
		    query=${${(s.___.)output}[i]}
		    compadd -J $group -X %B$group%b ${(s. .)query} ;
		done
		}

arkcompleteclient() { 
 		    if [[ $PREFIX == '' ]]; then
	      	      	cmd="ark2.1 ${words[2,-2]} -complete -filter client";
		else
	      	      	cmd="ark2.1 ${words[2,-2]} -complete -prefix $PREFIX -filter client";
		fi
#		echo \\n $cmd
		output=`zsh -c $cmd`;
		n=${#${(s.___.)output}}

		while (( i++ < n)); do
		    group=${${(s.___.)output}[i++]}
		    query=${${(s.___.)output}[i]}
		    compadd -J $group -X %B$group%b ${(s. .)query} ;
		done
		}

arkcompletetask() { 
                          if [[ -z $ARKCLIENT ]]; then
			        clientstr="";
			  else
			        clientstr="-client $ARKCLIENT";
		          fi
                          if [[ -z $ARKPROJECT ]]; then
			        projectstr="";
			  else
				projectstr="-p $ARKPROJECT";
		          fi

			  if [[ $PREFIX == '' ]]; then
	      	      	     	cmd="ark2.1 ${words[2,-2]} -complete $clientstr $projectstr -filter task";
			  else
				cmd="ark2.1 ${words[2,-2]} -complete $clientstr $projectstr -prefix $PREFIX -filter task";
			  fi
#		echo \\n $cmd
		output=`zsh -c $cmd`;
		n=${#${(s.___.)output}}

		while (( i++ < n)); do
		    group=${${(s.___.)output}[i++]}
		    query=${${(s.___.)output}[i]}
		    compadd -J $group -X %B$group%b ${(s. .)query} ;
		done
		}

compdef arkcomplete2 ark2.1
compdef arkcomplete2 r2
compdef arkcompleteshot r2s
compdef arkcompleteshot shot
compdef arkcompleteproj r2p
compdef arkcompleteproj proj
compdef arkcompleteclient r2c
compdef arkcompleteclient client
compdef arkcompletegroup r2g
compdef arkcompletegroup sequence
#compdef arkcompletegroup seq
compdef arkcompletetask r2t
compdef arkcompletetask task

zstyle ':completion:*:ark2.1:*:*' menu yes select interactive
zstyle ':completion:*:ark2.1:*:*' menu yes=long select=long interactive
zstyle ':completion:*:ark2.1:*:*' menu yes=10 select=10 interactive

#zstyle ':completion::*:ark2.1:*:' group-name ''
#autoload -U compinit
#compinit
zstyle ':completion:*' menu select=2

# add ark paths to environment
setenv ARKHOME "/theark"
setenv ARKPROJECTS "/theark/proj"
path=($path $ARKHOME/dist/bin)
setenv PYTHONPATH $ARKHOME/dist/python:$PYTHONPATH
setenv PRODTOOLS /theark/proj/base/prodtools
setenv OS_DARWIN_ROOTMAP /theark/proj/base/swr/vfx/base
