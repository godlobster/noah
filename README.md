# Noah #

Project Environment Management System
*v0.01a*

## Setup ##
```
#!sh

python/webconfig.py
python/arkconfig.py
~/arkconfig.py
$ARKHOME/.config/ark.config
$ARKHOME/.config/ark-config.yaml

```
## Run ##

### Web Application ###
```
#!sh

noahserver
noahclient

```
http://127.0.0.1:5600

### Command-Line Client ###
```
#!sh

noah [options] [-complete|-command] prefix/descriptors
 
positional arguments:
  descriptors

optional arguments:
  -h, --help            show this help message and exit
  --complete            -complete. Do completion mode - return list for shell
                        completions
  -l, --list            -list Print the list of findings.
  --command             -command Print the findings as a shell command for
                        eval.
  --info                -info Print the whole list of the database..
  -f [FILTER [FILTER ...]], --filter [FILTER [FILTER ...]]
                        -f[ilter] apply filter by type (project,task,group
                        ...)
  --prefix [PREFIX], --pre [PREFIX]
                        -pre[fix] <prefix> Search for a prefix for pattern
                        matching. Eg. shot returns shot01,shot02 etc...
  -c, --children        -c[hildren] show only all the children/descendents
                        (recursive).
  --arklevel0 [ARKLEVEL0]
                        level 0 name.
  --type [TYPE]         -type for a given element we can specify a 'type'. eg.
                        _shots,_assets.
  --cd [CD]             -cd specify a direct path
  -q, --query           -q[uery] info
  --queryid [QUERYID]   -query id
  --update              -update write the cache database
  --addlibrary [ADDLIBRARY]
                        -add library path
  --clear               -clear database
  --verbose             -verbose

```
**example:**

*Navigation
* 

* noah shot01
* noah weta avatar shot01
* noah /therark/proj/show/scene/shot
* noah .

*Completions
*

* noah shot -complete -filter element


*Database Update
*

* noah -addlibrary /theark/projects+
* noah -update -verbose+
 
 
## Contact ##

*dumbdoggie@gmail.com*