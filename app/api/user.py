from flask import render_template, flash, redirect, url_for, request, jsonify
from flask.ext.cors import CORS, cross_origin

import json

from app import app, db, bcrypt
from app.models.user import User
from app.api import arkmodel

@app.route('/api/%s/query'%User.url,methods=['GET','POST'])
def query_users():
    return arkmodel.query(User)

@app.route('/api/%s/querybyregex'%User.url,methods=['GET','POST'])
def querybyregex_users():
    return arkmodel.querybyregex(User)

@app.route('/api/%s/querybyids'%User.url,methods=['GET','POST'])
def querybyids_users():
    return arkmodel.querybyids(User)

@app.route('/api/%s/<int:id>'%User.url,methods=['GET',"POST"])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def getapi_user(id):
    entry = arkmodel.get(User,id)
    return entry

@app.route('/api/%s/update/<int:id>'%User.url,methods=['POST'])
def update_user(id):
    return arkmodel.update(User,id)

@app.route('/api/%s/updatepassword/<int:id>'%User.url,methods=['POST'])
def update_password(id):
    data = request.form
    if not "password" in data:
        return json.dumps({"success":False,
                           "message": "Invalid password"},
                          410,
                          {'ContentType':'application/json'})
        
    # query what needs updating
    
    user = User.query.get(id)
    if not user:
        return json.dumps({"success":False,
                           "message": "can't find user %d"%user.id},
                          410,
                          {'ContentType':'application/json'})
    # update each result
    user.set_password(bcrypt.generate_password_hash(data["password"]).decode('utf-8'))
    db.session.commit()
    print ("password updated")
    
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}  

@app.route('/api/%s/updatethumbnail/<int:id>'%User.url,methods=['GET',"POST"])
def update_user_thumbnail(id):
    return arkmodel.update_thumbnail(id,User)

@app.route('/api/%s/add'%User.url,methods=['POST'])
def api_add_user():
    data = request.get_json(force=True) or request.form
    if "password" in data:
        data["password"]  = bcrypt.generate_password_hash(data["password"]).decode('utf-8') #encrypt_password(data["password"])

    return arkmodel.add(User,column="username",modifieddata=data)

