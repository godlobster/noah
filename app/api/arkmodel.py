from flask import request, jsonify

import json
import string,re

from app import app, db
from app.models.application import Application,ArkEntryApplication
from app.models.arkentry import ArkEntry,Tag
from app.models.arkvolume import ArkVolume,ArkHost
from app.models.user import User


from utils import _json_builder, _json_builder_from_model,encodeforweb
    
SUCCESS = json.dumps({'success':True, "data":[{}]}), 200, {'ContentType':'application/json'}

def _getfieldsquery(args, model):
    # -- fields query 
    fieldsquery = ""
    result = ""
    if "fields" in args:
        fieldnames = args["fields"].split(",")
        for f in fieldnames:
            fieldsquery = fieldsquery + "%s.%s,"%(model.__name__,f)
        fieldsquery = fieldsquery[:-1]
        result = fieldsquery and "with_entities(%s)"%fieldsquery
    
    elif "fieldsexclude" in args and model:
        # TODO: this is slow with lots of entries
        fieldnames = model.getcolumns()
        exclude = [x.lower() for x in args["fieldsexclude"].split(",")]
        for f in fieldnames:
            if f.lower() in exclude:
                continue
            fieldsquery = fieldsquery + "%s.%s,"%(model.__name__,f)
        fieldsquery = fieldsquery[:-1]
        result = fieldsquery and "with_entities(%s)"%fieldsquery

    return result

def query(model):
    """ json/formdata -
            send in query as json or formdata.  Supports '*' for name and id.
            eg: query={'name':'*'} or {'id':1}
        
        args - supports 'fields' argument to specify fields to be returned
            eg.  /entry/1?fields=name,id
    """
    # -- form/json data
 #   print ("api.models.arkmodel: query", flush=True)
    data = request.args or request.form or request.get_json()
  #  print ("api.models.arkmodel: data=", data)
    datad = _json_builder(data)
    if "id" in datad and datad["id"] == "*":
        del(datad["id"])
    if "name" in datad and datad["name"] == "*":
        del(datad["name"])
    # zero out binary data for queries
    if "thumbnail" in datad:
        del(datad["thumbnail"])
    if "image" in datad:
        del(datad["image"])
    if "poster" in datad:
        del(datad["poster"])

    # -- fields query 
    args = request.args
    fieldsquery = _getfieldsquery(args,model)

    # -- execute query
    result = []

    if "id" in datad and len(datad.keys()) == 1:
        if type(datad["id"]) == dict:
            datad["id"] = datad["id"]["id"]

        if fieldsquery:
            querystr = "model.query.%s.filter_by(id="+str(datad["id"])+").all()"
            result = eval(querystr%fieldsquery)
        else:
            result=[model.query.get(datad["id"])]
    else:
        if fieldsquery:
            querystr = "model.query.%s.filter_by("+str(**data)+").all()"
            result = eval(querystr%fieldsquery)
        else:
            result=model.query.filter_by(**datad).all()

    jsondata =  jsonify(data=[_json_builder_from_model(x,model=model) for x in result])
    
    return jsondata

def _buildquery(d,operator=None, result="", model=""):
    """ build a query of ands and ors using $and and $or as operators"""
    if type(d) == type ({}):
        keys = d.keys()
        if operator in ("$and","$or"):
            return result+ string.join(list(map(lambda x:_buildquery(d[x],x,model=model),keys)))
        elif operator:
            if type(d[operator]) == int:
                return result + " %s.%s == %s,"%(model,operator,d[operator])
            val = d[operator]
            val = re.sub("*","%",val)
            if "%" in val:
                return result + "%s.%s.like('%s'),"%(model,operator,d[operator])
            else:
                return result + " %s.%s == '%s',"%(model,operator,d[operator])                  
        else:
            return result+ string.join(list(map(lambda x:_buildquery(d[x],x,model=model),keys)))
                
    elif type(d) == type([]):
        if operator == "$and":
            return result + "and_(" + string.join(list(map(lambda x:_buildquery(x,operator,model=model),d))[:-1]) +"),"
        elif operator == "$or":
            return result + "or_(" + string.join(list(map(lambda x:_buildquery(x,operator,model=model),d))[:-1]) +"),"

    else:
        if type(d) == int:
            return result + " %s.%s == %s,"%(model,operator,d)

        val = d
        val = re.sub("\*","%",val)
        if "%" in val:
            s = result + "%s.%s.like('%s'),"%(model,operator,d)
            return result + s
        s = result + " %s.%s == '%s',"%(model,operator,d)
        return s
        
    return result + ")"

def querybyregex(model):
    """ uses like for regex"""
    data = request.get_json()
    datad = _json_builder(data)

    if "id" in datad and datad["id"] == "*":
        del(datad["id"])
    if "name" in datad and datad["name"] == "*":
        del(datad["name"])

    result = []

    # TODO
    tosql = "%s.query.filter("%model.__name__
    q = _buildquery(datad,operator=None,result="",model="%s"%model.__name__).strip()
    if q and q[-1] == ",":
        q = q[:-1]
    tosql = tosql + q +").all()"
    result = eval(tosql)

    jsondata =  jsonify(data=[_json_builder_from_model(x,model=model) for x in result])
    return jsondata

def querybyids(model):
    data = request.get_json() or request.form
    
    if not data:
        return json.dumps({"success":False,
                           "message": "Must supply list of ids in json/formdata"},
                          410,
                          {'ContentType':'application/json'})

    # -- fields query 
    args = request.args
    fieldsquery = _getfieldsquery(args,model)
    
    # -- perform query
    if fieldsquery:
        querystr = "model.query.%s.filter(model.id.in_("+data+")).all()"
        result = querystr%fieldsquery
    else:
        result = model.query.filter(model.id.in_(data)).all()

    jsondata =  jsonify(data=[_json_builder_from_model(x,model=model) for x in result])
    return jsondata

def get(model,id):
    result = model.query.get(id)

    jsondata =  jsonify(data=[_json_builder_from_model(x,model=model) for x in [result]])
    return jsondata

def delete(model,id):
    entry = model.query.get(id)
    db.session.delete(entry)
    db.session.commit()
    return SUCCESS

def add(model,column="path",modifieddata=None):
    """ insert into database.  column is the attribute we pass in to check if its unique.  
    It can also be a list of attributes"""

    try:
        data = modifieddata or request.form or request.get_json(force=True)
    except Exception as e:
        print (e )
        print ("Error in json")
        return json.dumps({"success":False,
                           "message": "Error in json"},
                          410,
                          {'ContentType':'application/json'})

    result=None
    if column:
        if type(column) == list:
            filters=[]
            for c in column:    
                datakey = data[c]
                filter = 'eval("%s.%s"%(model.__name__,c)) == datakey'
                filters.append(filter)

            filterstr = eval(",".join(filters))
            result = model.query.filter(*filterstr).first()
        else:
            datakey = data[column]
            result = model.query.filter(eval("%s.%s"%(model.__name__,column)) == datakey).first()
    if result:
        print ("entry already exists.  Skipping: %s "%datakey)
        jsondata = jsonify(data=[_json_builder_from_model(result, model=model)])
        return jsondata

    else:
        entry = model()
        for c in model.getcolumns():
            if c in data:
                if data[c] == None:
                    continue

                if c == "thumbnail":
                    if data[c].startswith("data"):
                        data[c] = re.sub ("data:.*;.*,","",data[c])
                setattr(entry,c,data[c])
        db.session.add(entry)
        db.session.commit()

        jsondata =  jsonify(data=[_json_builder_from_model(entry,model=model)])

        return jsondata

def update(model,id=None,modifieddata=None):
    data = request.get_json() or request.form
	
    datad = _json_builder(data)
    if type(datad["_updatedata"]) == dict:
        updatedata = datad["_updatedata"]
    else:
        updatedata = json.loads(datad["_updatedata"])
    del (datad["_updatedata"])

    # query what needs updating
    if not id:
        if "id" in datad and datad["id"] == "*":
            del(datad["id"])
        if "name" in datad and datad["name"] == "*":
            del(datad["name"])

        result = []
        if "id" in datad and len(datad.keys()) == 1:
            result=[model.query.get(data["id"])]
        else:
            result=model.query.filter_by(**datad).all()
    else:
        entry = model.query.get(id)
        result = entry and [entry]

    if not result:
        return json.dumps({"success":False,
                           "message": "No matching rows to update, query=%s"%datad},
                          410,
                          {'ContentType':'application/json'})
    # update each result
    datatoprocess = modifieddata or updatedata
    for record in result:
        for c in model.getcolumns():
            if c in datatoprocess:
                if c.lower() == "password":
                    record.set_password(datatoprocess[c])
                else:
                    setattr(record,c,datatoprocess[c])

    db.session.commit()

    jsondata =  jsonify(data=[_json_builder_from_model(x, model=model) for x in result])
    return jsondata

def update_thumbnail(id,model):
    """ read in binary data from 'thumbnail' in form """
    filename = request.files['thumbnail']
    entry = model.query.get(id)
    if entry and filename:
        #		fin = open(filename, "rb")
        img = filename.read()
        if hasattr(entry,"prefs") and hasattr(entry.prefs,"thumbnail"):
            entry.prefs.thumbnail = 'data:image/jpeg;base64,'+img.encode("base64")
        else:
            entry.thumbnail = 'data:image/jpeg;base64,'+img.encode("base64")
                
    db.session.commit()
    jsondata = jsonify(data=_json_builder_from_model(entry, model=model))
    return jsondata

def update_poster(id,model):
    """ read in binary data from 'thumbnail' in form """
    filename = request.files['thumbnail']
    entry = model.query.get(id)
    if entry and filename:
        #		fin = open(filename, "rb")
        img = filename.read()
        if hasattr(entry,"prefs") and hasattr(entry.prefs,"poster"):
            entry.prefs.poster = 'data:image/jpeg;base64,'+img.encode("base64")
        else:
            entry.poster = 'data:image/jpeg;base64,'+img.encode("base64")
                
    db.session.commit()
    jsondata = jsonify(data=_json_builder_from_model(entry, model=model))
    return jsondata
