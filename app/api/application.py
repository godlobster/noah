from app import app, db
from app.models.application import Application
from app.models.application import ArkEntryApplication
from app.api import arkmodel

# -------- Custom API
@app.route('/api/%s/query'%Application.url,methods=['GET',"POST"])
def query_applications():
    return arkmodel.query(Application)

@app.route('/api/%s/query'%ArkEntryApplication.url,methods=['GET',"POST"])
def query_arkentryapplications():
	return arkmodel.query(ArkEntryApplication)

@app.route('/api/%s/querybyregex'%Application.url,methods=['GET',"POST"])
def querybyregex_applications():
	return arkmodel.querybyregex(Application)

@app.route('/api/%s/querybyids'%Application.url,methods=['GET',"POST"])
def querybyids_application():
	return arkmodel.querybyids(Application)

@app.route('/api/%s/<int:id>'%Application.url,methods=['GET',"POST"])
def get_application(id):
    return arkmodel.get(Application,id)

@app.route('/api/%s/<int:id>'%ArkEntryApplication.url,methods=['GET',"POST"])
def get_arkentryapplication(id):
    return arkmodel.get(Application,id)

@app.route('/api/%s/add'%Application.url,methods=['POST'])
def add_application():
    return arkmodel.add(Application,column=["name","path"])

@app.route('/api/%s/add'%ArkEntryApplication.url,methods=['POST',"GET"])
def add_arkentryapplication():
    data = arkmodel.add(ArkEntryApplication,column="")
    return data

@app.route('/api/%s/updatethumbnail/<int:id>'%Application.url,methods=["POST"])
def update_application_thumbnail(id):
    return arkmodel.update_thumbnail(id,Application)  

@app.route('/api/%s/update/<int:id>'%Application.url,methods=['POST'])
def update_application(id):
    return arkmodel.update(Application,id)


