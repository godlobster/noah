from flask import request
import urllib
from app import app
import webconfig
import json
from app.models.arkentry import ArkEntry

@app.route('/api/filebrowser',methods=['POST',"GET"])
def filebrowserquery():
    data = request.get_json()
    clientip = request.environ['REMOTE_ADDR']
    print (clientip)
    SEND_URL = "http://"+clientip+":%d/api/filebrowser"%webconfig.CLIENT_PORT
    request2 = urllib.request.Request(SEND_URL)

    request2.add_header('Content-Type', 'application/json')
    response = urllib.request.urlopen(request2,json.dumps(data))

    message   = response.read()

    data = json.loads(message)
    response.close()

    if "success" in data and data["success"] == False:
        return json.dumps({"success":False,
                           "message":data["message"]}), \
                          410, \
                          {'ContentType':'application/json'}
    elif "success" in data:
        return json.dumps({"success":True}), \
                          200, \
                          {'ContentType':'application/json'}
    else:        
        return json.dumps(data["data"][0]), \
                          200, \
                          {'ContentType':'application/json'}

@app.route('/api/filebrowser/setfavorites/<int:id>',methods=['GET','POST'])
def filbrowsersetfavorites(id):
    arkentry = ArkEntry.query.get(id)
    prefs = arkentry.prefs
    data = request.get_json()
    if arkentry and data and prefs and \
        "filebrowserfavorites" in data and \
        data["filebrowserfavorites"]:
        prefs.filebrowserfavorites = str(data["filebrowserfavorites"])
        db.session.commit()
        return json.dumps({'success':True}, 
                          200, 
                          {'ContentType':'application/json'})
    else:
        return json.dumps({'success':False,
                           "message":"Can't Find arkentry %d"%id},
                          410,
                          {'ContentType':'application/json'})
    
@app.route('/api/addlibrary',methods=['GET','POST'])
def addlibrary():
    data = request.get_json()
    clientip = request.environ['REMOTE_ADDR']
    SEND_URL = "http://"+clientip+":%d/api/addlibrary"%webconfig.CLIENT_PORT
    print ("app.api.main addlibrary: connecting to client -",clientip,webconfig.CLIENT_PORT)
    print ("app.api.main addlibrary: url -",SEND_URL)

    try:
        request2 = urllib2.Request(SEND_URL)

        request2.add_header('Content-Type', 'application/json')
        print ("app.api.main launchcommand: sending...")

        response = urllib2.urlopen(request2,json.dumps(data))

        code = response.code
        return json.dumps(data["data"][0]), \
                      200, \
                      {'ContentType':'application/json'}
    except URLError:
        print ("Error connecting to client")
        return json.dumps({"success":False,
                           "message":"Unable to connect to client"}), \
                            410, \
                            {'ContentType':'application/json'}

@app.route('/api/updatelibrary',methods=['GET','POST'])
def updatelibrary():
    data = request.get_json()
    clientip = request.environ['REMOTE_ADDR']
    clientport  = webconfig.CLIENT_PORT
    print ("app.api.main updatelibrary: connecting to client -",clientip,clientport)
    SEND_URL = "http://"+clientip+":%d/api/updatelibrary"%clientport
    print ("app.api.main updatelibrary: url -",SEND_URL)
    try:
        request2 = urllib2.Request(SEND_URL)

        request2.add_header('Content-Type', 'application/json')
        print ("app.api.main updatelibrary: sending...")

        response = urllib2.urlopen(request2,json.dumps(data))

        code = response.code
        return json.dumps({"success":True}), \
                200, \
                {'ContentType':'application/json'}
    except URLError:
        print ("Error connecting to client")
        return json.dumps({"success":False,
                           "message":"Unable to connect to client"}), \
                            410, \
                            {'ContentType':'application/json'}

@app.route('/api/launchcommand',methods=['GET','POST'])
def launchcommand():
    data = request.get_json()
    clientip = request.environ['REMOTE_ADDR']
    clientport  = webconfig.CLIENT_PORT
    print ("app.api.main launchcommand: connecting to client -",clientip,clientport)
    SEND_URL = "http://"+clientip+":%d/api/launchcommand"%clientport
    print ("app.api.main launchcommand: url -",SEND_URL)
    try:
        request2 = urllib2.Request(SEND_URL)

        request2.add_header('Content-Type', 'application/json')
        print ("app.api.main launchcommand: sending...")

        response = urllib2.urlopen(request2,json.dumps(data))

        code = response.code
        return json.dumps({"success":True}), \
                200, \
        {'ContentType':'application/json'}
    except URLError:
        print ("Error connecting to client")
        return json.dumps({"success":False,
                           "message":"Unable to connect to client"}), \
                            410, \
                            {'ContentType':'application/json'}

@app.route('/api/launchapp/<int:id>/<int:appid>',methods=['GET','POST'])
def launchapp(id,appid):
    data = request.get_json()
    clientip = request.environ['REMOTE_ADDR']
    clientport  = webconfig.CLIENT_PORT
    print ("app.api.main launchapp: connecting to client -",clientip,clientport)
    SEND_URL = "http://"+clientip+":%d/api/launchapplication/%d/%d/0"%(clientport,
                                                               id,
                                                               appid)
    print ("app.api.main launchapp: url -",SEND_URL)
    try:
        request2 = urllib2.Request(SEND_URL)

        request2.add_header('Content-Type', 'application/json')
        response = urllib2.urlopen(request2,json.dumps(data))

        code = response.code
        return json.dumps({"success":True}), \
                200, \
        {'ContentType':'application/json'}
    except URLError:
        print ("Error connecting to client")
        return json.dumps({"success":False,
                           "message":"Unable to connect to client"}), \
                            410, \
                            {'ContentType':'application/json'}
    
