from flask import render_template, flash, redirect, url_for, request, jsonify, send_from_directory,send_file

import json
import sqlite3

from app import app, db

from app.models.arkentry import ArkEntry,ArkEntryPrefs,Tag
from app.models.user import User
from app.models.arkvolume import ArkVolume
from app.models.application import Application
from app.models.application import ArkEntryApplication
from app import views
from app.api import arkmodel

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

import urllib

from utils import _json_builder, _json_builder_from_model,encodeforweb
from flask.ext.cors import CORS, cross_origin
import webconfig


#---------  custom API

# ----------------------
# Ark Entry 
# ----------------------

@app.route('/api/%s/query'%ArkEntry.url,methods=['GET','POST'])
def query_arkentries():
    return arkmodel.query(ArkEntry)

@app.route('/api/%s/querybyregex'%ArkEntry.url,methods=['GET','POST'])
def querybyregex_arkentries():
    return arkmodel.querybyregex(ArkEntry)

@app.route('/api/%s/querybyids'%ArkEntry.url,methods=['GET','POST'])
def querybyids_arkentries():
    return arkmodel.querybyids(ArkEntry)

@app.route('/api/%s/<int:id>'%ArkEntry.url,methods=['GET',"POST"])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def api_get_arkentry(id):
    print ("api_get_arkentry")
    entry = ArkEntry.query.get(id)

    parents = _getparents(entry) or []
    longname = ""
    for parent in parents[1:]:
        longname += parent.name + " "
    entry.longname = (longname and longname[:-1] or "") + " " + entry.name
    jsondata =  jsonify(data=[_json_builder_from_model(x,model=ArkEntry) for x in [entry]])

    return jsondata

@app.route('/api/%s/add'%ArkEntry.url,methods=['POST'])
def add_arkentry():
    data = request.get_json()
    path = data['path']
    result = ArkEntry.query.filter(ArkEntry.path == path).first()
    if result:
        print ("entry %s already exists.  Skipping"%path)
        return None
    else:
        arkpref = ArkEntryPrefs()
        arkpref.shortname = data["name"]
        db.session.add(arkpref)
        db.session.commit()
        
        arkentry = ArkEntry()
        for c in ArkEntry.getcolumns():
            if c in data:
                if data[c] == None:
                    continue
                elif c== "arkvolumes" and len(data[c]) and type(data[c][0]) == int:
                    arkvolumes = data[c]
                    arkvolumes = list(set(arkvolumes))
                    data[c] = list(map(ArkVolume.query.get,arkvolumes))
                setattr(arkentry,c,data[c])
        setattr(arkentry,"prefs",arkpref)
        db.session.add(arkentry)
        db.session.commit()

        resultdata =  jsonify(data=[_json_builder_from_model(arkentry,model=ArkEntry)])

        return resultdata

@app.route('/api/%s/update'%ArkEntry.url,methods=['POST'])
def update_entries():
    data = request.get_json() or request.form
	
    datad = _json_builder(data)
    updatedata = datad["_updatedata"]
    del (datad["_updatedata"])
    
    # query what needs updating
    if "id" in datad and datad["id"] == "*":
        del(datad["id"])
    if "name" in datad and datad["name"] == "*":
        del(datad["name"])

    result = []
    if "id" in datad and len(datad.keys()) == 1:
        result=[ArkEntry.query.get(data["id"])]
    else:
        result=ArkEntry.query.filter_by(**datad).all()

    # update each result
    for record in result:
        for c in ArkEntry.getcolumns():
            if c == "arkvolumes" and c in updatedata and \
                len(updatedata[c]) and type(updatedata[c][0]) == int:
                    arkvolumes = updatedata[c]
                    uniquevolumes = list(set(arkvolumes))
                    updatedata[c] = list(map(ArkVolume.query.get,uniquevolumes))
                    setattr(record,c,updatedata[c])
                    continue
                    
            elif c in updatedata:
                if c == "children" and updatedata[c] == None:
                    updatedata[c] = []
                elif c == "children" and len(updatedata[c]):
                    if type(updatedata[c][0]) == dict:
                        updatedata[c] = [(type(x)==dict and x["id"]) or x for x in updatedata[c]]
                    children = updatedata[c]
                    uniquechildren = list(set(children))
                    updatedata[c] = list(map(ArkEntry.query.get,uniquechildren))
                setattr(record,c,updatedata[c])
                
    db.session.commit()

    resultdata =  jsonify(data=[_json_builder_from_model(x, model=ArkEntry) for x in result])
    return resultdata

@app.route('/api/%s/updatethumbnail/<int:id>/<int:index>'%ArkEntry.url,methods=['POST'])
def update_thumbnail(id,index):
    filename = request.form["thumbnail.%d"%index]
    entry = ArkEntry.query.get(id)
    parent = ArkEntry.query.get(entry.id)
    if entry and filename:
        img = filename.read()
        entry.prefs.thumbnail = encodeforweb(img)
        entry.thumbnail = encodeforweb(img)
    db.session.commit()
    return views.arkentry.get_arksubentries(parent.level,parent.id)

@app.route('/api/%s/updateprefsthumbnail/<int:id>'%ArkEntry.url,methods=['POST'])
def update_prefs_thumbnail(id):
    return arkmodel.update_thumbnail(id,ArkEntryPrefs)

@app.route('/api/%s/launchfinder'%ArkEntry.url,methods=['POST',"GET"])
def arkentry_launchfinder():   
    data = request.get_json()
    id = data["id"]
    arkentry = ArkEntry.query.get(id)
    app = Application.query.get(3)
        
    SEND_URL = webconfig.CLIENT_URL+"/api/launchcommand"
    request2 = urllib.request.Request(SEND_URL)
    data = """{"command":"open -a %s %s"}"""%(app.path,arkentry.fullpath)
    request2.add_header('Content-Type', 'application/json')
    response = urllib.request.urlopen(request2,data)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}  

@app.route('/api/%s/launchapplication/<int:arkentryid>?<int:appid>?<int:arkentryappid>'%ArkEntry.url,methods=['POST',"GET"])
def arkentry_launchapplication(arkentryid, appid, arkentryapp):   
    arkentry = ArkEntry.query.get(arkentryid)
    app = Application.query.get(3)
    arkentryapp = None
    for a in arkentry.prefs.applications:
        if a.name.lower() == app.name.lower():
            arkentryapp = a
    cmd = "%s %s"%(app.fullpath,app.args)
    
    SEND_URL = webconfig.CLIENT_URL+"/api/launchcommand"
    request2 = urllib.request.Request(SEND_URL)
    data = """{"command":"%s"}"""%(cmd)
    request2.add_header('Content-Type', 'application/json')
    response = urllib.request.urlopen(request2,data)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}  


def arkentry_launchappold(id,arkentryid):    
    app = Application.query.get(id)
    path = str(app.path)
    strIO = StringIO.StringIO()
    #	arkfile = "{\"path\": \""+path+"\",\"args\" : \"\",\"environment\": \"\"}"
    
    pylaunchfile = """\
#!/usr/bin env python
import os

command = "%s"
args = "%s"

print command + " " + args

os.system(command + " " + args)
"""%(path,"")
    strIO.write(pylaunchfile)

    strIO.seek(0)
    return send_file(strIO,
                     attachment_filename="runapp.torrent",
                     as_attachment=True)


@app.route('/api/%s/updateapps/<int:id>?<int:appid>'%ArkEntry.url,methods=['POST','GET'])
def arkentry_updateapps(id,appid):
    arkentryapp = ArkEntryApplication.query.filter(ArkEntryApplication.application==appid,
                                                   ArkEntryApplication.arkentry==id).all()
    if arkentryapp:
        arkentry = ArkEntry.query.get(id)
        appids = list(map(lambda x: x.application, arkentry.prefs.applications))
        if appid in appids:
            return views.arkentry.get_arkentry(id)
        else:
            db.session.delete(arkentryapp[0])
		
    arkentryapp = ArkEntryApplication()
    arkentryapp.arkentry = id
    arkentryapp.application = appid
    db.session.add(arkentryapp)

    arkentry = ArkEntry.query.get(id)
    arkentry.prefs.applications.append(arkentryapp)
									 
    db.session.commit()

    return views.arkentry.get_arkentry(id)

@app.route('/api/%s/gotofolder/<int:id>'%ArkEntry.url,methods=['GET'])
def arkentry_gotofolder(id):
    arkentry = ArkEntry.query.get(id)
    path = arkentry.fullpath

    return app.send_static_file(path)

# ----------------------
# Ark Entry Prefs
# ----------------------

@app.route('/api/%s/query'%ArkEntryPrefs.url,methods=['GET','POST'])
def query_arkprefs():
    data = request.get_json()
    datad = _json_builder(data)

    if "id" in datad and datad["id"] == "*":
        del(datad["id"])

    result = []
    if "id" in datad and len(datad.keys()) == 1:
        if data["id"]:
            result=[ArkEntryPrefs.query.get(data["id"])]
    else:
        result=ArkEntryPrefs.query.filter_by(**datad).all()

    resultdata =  jsonify(data=[_json_builder_from_model(x,model=ArkEntryPrefs) for x in result])
    return resultdata

@app.route('/api/%s/getfilebrowserfavorites/<int:id>'%ArkEntryPrefs.url,methods=['GET','POST'])
def get_filebrowserfavorites(id):
    # return favorites as a dictionary.  it's stored as a string.
    arkentry = ArkEntry.query.get(id)
    if not arkentry:
        return json.dumps({'success':False,
                           "message":"Can't Find arkentry %d"%id}),\
               410,\
               {'ContentType':'application/json'}
    prefs = arkentry.prefs
    favorites = prefs.filebrowserfavorites

    if favorites:
        favoritesdictionary = eval(favorites)
        
    resultdata =  jsonify(data=[_json_builder(favoritesdictionary)])
    return resultdata


@app.route('/api/%s/setfilebrowserfavorites/<int:id>'%ArkEntryPrefs.url,methods=['GET','POST'])
def set_filebrowserfavorites(id):
    arkentry = ArkEntry.query.get(id)
    prefs = arkentry.prefs
    data = request.get_json()
    if arkentry and data and prefs and \
        "filebrowserfavorites" in data and \
        data["filebrowserfavorites"]:
        prefs.filebrowserfavorites = str(data["filebrowserfavorites"])
        db.session.commit()
        
    
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}  
    else:
        return json.dumps({'success':False,
                           "message":"Can't Find arkentry %d"%id}),\
                410,\
                {'ContentType':'application/json'}

@app.route('/api/%s/updateprefs/<int:id>'%ArkEntry.url,methods=['POST'])
def update_prefs_from_form(id):
    pref = ArkEntryPrefs.query.get(id)
    inherited = pref.inherit.split()
    inheriteddata = pref.getinheritance()
    columns = ArkEntryPrefs.getcolumns()
    for column in columns:
        omit = ["tags","applications"]
        if column in omit:
            continue

        if column in request:
            if column in inheriteddata and request.form[column] == inheriteddata[column]:
                continue
            setattr(pref,column,request.form[column])

    if "posterholder" in request.files and request.files["posterholder"]:
        filename = request.files["posterholder"]
        if pref:
            img = filename.read()
            setattr(pref,"poster", encodeforweb(img))
            print ("setting done.")
			
    if request.files and \
        "thumbnailholder" in request.files and \
        request.files["thumbnailholder"]:
        filename = request.files['thumbnailholder']
        if pref:
            img = filename.read()
            binary = sqlite3.Binary(img)
            setattr(pref,"thumbnail", encodeforweb(img))
		
    db.session.commit()
    return views.arkentry.get_arkentryprefs(pref.arkentry.id)

@app.route('/api/%s/update'%ArkEntryPrefs.url,methods=['POST'])
def update_prefs():
    """ given a query string to filter by using name or id, 
    update the corresponding data in _updatedata"""
    data = request.get_json() or request.form

    datad = _json_builder(data)
    updatedata = datad["_updatedata"]

    del (datad["_updatedata"])
    if isinstance(updatedata, str) or type(updatedata) == type(u''):
        updatedata = json.loads(updatedata,strict=False)
    
    # query what needs updating
    if "id" in datad and datad["id"] == "*":
        del(datad["id"])
    if "name" in datad and datad["name"] == "*":
        del(datad["name"])

    result = []
    if "id" in datad and len(datad.keys()) == 1:
        result=[ArkEntryPrefs.query.get(data["id"])]
    else:
        result=ArkEntryPrefs.query.filter_by(**datad).all()

    # update each result
    for record in result:
        for c in ArkEntryPrefs.getcolumns():
            if c in updatedata:
                if c=="applications":
                    apps = list(map(ArkEntryApplication.query.get,updatedata[c]))
                    setattr(record,c,apps)
                elif c=="tags":
                    tags = list(map(Tag.query.get,updatedata[c]))
                    setattr(record,c,tags)
                else:
                    setattr(record,c,updatedata[c])

    db.session.commit()
    resultdata =  jsonify(data=[_json_builder_from_model(x,model=ArkEntryPrefs) for x in result])
    return resultdata

@app.route('/api/%s/updateposter/<int:id>'%ArkEntryPrefs.url,methods=['POST'])
def update_prefs_poster(id):
    print ("updateprefsposter")
    return arkmodel.update_poster(id,ArkEntryPrefs)

# ----------------------
# Ark Entry Application
# ----------------------

@app.route('/api/%s/<int:id>'%ArkEntryApplication.url,methods=['GET'])
def api_get_arkentryapplication(id):
    return arkmodel.get(Application,id)

def _getparents(arkentry):
    parents = []
    parent = arkentry.parent
    while parent:
        parententry = ArkEntry.query.get(parent)
        parents.append(parententry)
        parent = parententry.parent
    parents.reverse()

    return parents

@app.route('/api/%s/subentries/<int:id>'%ArkEntry.url,methods=['GET'])
def api_get_subentries(id, tags=[], user=None):
    parentid=id

    # execute query using user if defined
    if user:
        userentry = User.query.get(id)
        if userentry.administrator == True:
            #skip user constraint for admins
            queryresult = ArkEntry.query.filter(ArkEntry.parent == parentid).order_by(ArkEntry.name).all()
        else:
            # eg. x = Dish.query.filter(Dish.restaurants.any(name=name)).all()
            queryresult = ArkEntry.query.filter(ArkEntry.users.any(id=user),
                              ArkEntry.parent==parentid).order_by(ArkEntry.name).all()
    else:
        queryresult=ArkEntry.query.filter(ArkEntry.parent==parentid).order_by(ArkEntry.name).all()

    # filter out only tagged items
    if tags:
        result = []
        for r in queryresult:
            alltags = [x.name for x in r.prefs.tags]
            if set(alltags).issubset(tags):
                result.append(r)
    else:
        result=queryresult

    parents = []
    if result:
        parents = _getparents(result[0])

    for record in result:
        # set longname
        longname = ""
        for parent in parents[1:]:
            longname += parent.name + " "
        record.longname = (longname and longname[:-1] or "") + " " + record.name
        # set thumbnail.  inherit parents
        if record.prefs.thumbnail:
            continue
        else:
            for i in range(len(parents)-1,-1,-1):
                parent = parents[i]
                if parent.prefs.thumbnail:
                    record.prefs.thumbnail = parent.prefs.thumbnail
                    break

    result = jsonify(data=[_json_builder_from_model(x, model=ArkEntry) for x in result])
    
    return result

@app.route('/api/%s/arksubentries/<int:id>'%ArkEntry.url,methods=['GET'])
def api_get_arksubentries(id):
    return api_get_subentries(id)

@app.route('/api/%s/parententries/<int:id>'%ArkEntry.url,methods=['GET'])
def api_get_parententries(id):
    print ("api_get_parententries",id)
    entry = ArkEntry.query.get(id)
    
    if entry.parent == None:
        result=ArkEntry.query.filter(ArkEntry.parent==None,
                                     ArkEntry.library==entry.library).order_by(ArkEntry.name).all()
        result = jsonify(data=[_json_builder_from_model(x, model=ArkEntry) for x in result])
        return result
    
    parentid= entry.parent
    
  #  level=parent.level+1
    result=ArkEntry.query.filter(ArkEntry.parent==parentid).order_by(ArkEntry.name).all()

    parents = []
    if result:
        parents = _getparents(result[0])

    thumbnails = {}
    for record in result:
        if record.prefs.thumbnail:
            continue
        else:
            for i in range(len(parents)-1,-1,-1):
                parent = parents[i]
                if parent.prefs.thumbnail:
                    record.prefs.thumbnail = parent.prefs.thumbnail
                    break

    result = jsonify(data=[_json_builder_from_model(x, model=ArkEntry) for x in result])
    
    return result

@app.route('/api/%s/getclipboard/<int:id>'%ArkEntry.url,methods=['GET','POST'])
def api_entry_getclipboard(id):
    print ("api_entry_getclipboard",id)

    arkentry = ArkEntry.query.get(id)
    clipboard = arkentry.prefs.clipboard
    
    print (clipboard)
    import requests
    if clipboard:
        SEND_URL = webconfig.CLIENT_URL+"/api/getclipboard"
        data = {"clipboard":clipboard}
        print (data)
        resp = requests.post(SEND_URL, params=data)
        
    
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}  


@app.route('/api/%s/setclipboard/<int:id>'%ArkEntry.url,methods=['GET','POST'])
def api_entry_setclipboard(id):
    print ("api_entry_setclipboard",id)

    arkentry = ArkEntry.query.get(id)
    
    SEND_URL = webconfig.CLIENT_URL+"/api/setclipboard"
    request2 = urllib2.Request(SEND_URL)
    request2.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(request2)

    data = json.load(response)   
    print ("clipboard response:")
    print (data)
    
    arkentry.prefs.clipboard = data["clipboard"]
#    setattr(arkentry,"prefs",arkpref)
    db.session.commit()

    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}  

@app.route('/api/%s/add'%Tag.url,methods=['POST'])
def api_add_tag():
    return arkmodel.add(Tag,column="name")

@app.route('/api/%s/query'%Tag.url,methods=['POST','GET'])
def api_query_tag():
    return arkmodel.query(Tag)
