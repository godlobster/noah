from flask import render_template, flash, redirect, url_for, request, jsonify

from sqlalchemy import desc, and_

import datetime
import os
import json
import subprocess
import string
import xml.etree.ElementTree

from app import app, db
from app.models.arkvolume import ArkVolume,Library,ArkHost
from app.api import arkmodel

# ----- Custom API
@app.route('/api/%s/query'%ArkVolume.url,methods=['GET',"POST"])
def query_arkvolumes():
    return arkmodel.query(ArkVolume)

@app.route('/api/%s/queryregex'%ArkVolume.url,methods=['GET',"POST"])
def querybyregex_arkvolumes():
    return arkmodel.querybyregex(ArkVolume)

@app.route('/api/%s/queryids'%ArkVolume.url,methods=['GET',"POST"])
def querybyids_arkvolumes():
    return arkmodel.querybyids(ArkVolume)

@app.route('/api/%s/add'%ArkVolume.url,methods=['POST'])
def add_arkvolume():
    return arkmodel.add(ArkVolume,column="name")

@app.route('/api/%s/update/<int:id>'%ArkVolume.url,methods=['POST'])
def update_arkvolume(id):
    modifieddata = None
    # modify the data if we have to, eg. to handle host
    data = request.get_json() or request.form
    if "_updatedata" in data:
        updatedata = json.loads(data["_updatedata"])
        if "host" in updatedata:
            modifieddata = updatedata
            if updatedata["host"].isdigit():
                hostid = string.atoi(updatedata["host"])
                host = ArkHost.query.get(hostid)
            else:
                host = ArkHost.query.filter(ArkHost.name == updatedata["host"]).first()
            if not host:
                return json.dumps({"success":False,
                                   "message":"Invalid host %s"%updatedata["host"]}), \
                                    410,{'ContentType':'application/json'}
            modifieddata["host"] = host.id
            
    return arkmodel.update(ArkVolume,id=id, modifieddata=modifieddata)

@app.route('/api/%s/updatethumbnail/<int:id>'%ArkVolume.url,methods=['GET',"POST"])
def update_arkvolume_thumbnail(id):
    return arkmodel.update_thumbnail(id,ArkVolume)  

@app.route('/api/%s/delete/<int:id>'%ArkVolume.url,methods=['POST'])
def delete_arkvolume(id):
    print ("deletevolume",id)
    return arkmodel.delete(ArkVolume,id)

@app.route('/api/%s/add'%Library.url,methods=['POST'])
def add_library():
    # need to do lookup for volumes
    modifieddata = None
    data = request.get_json(force=True) or request.form
    if "arkvolumes" in data and data["arkvolumes"]:
        modifieddata = data
        modifieddata["arkvolumes"] = list(map (lambda x: type(x) == int and ArkVolume.query.get(x) or x,data["arkvolumes"]))
    
    return arkmodel.add(Library,column=None,modifieddata=modifieddata)

@app.route('/api/%s/update/<int:id>'%Library.url,methods=['POST'])
@app.route('/api/%s/update'%Library.url,methods=['POST'])
def update_library(id=None):
    modifieddata = None
    # need to do lookup for volumes
    data = request.get_json() or request.form
    if "_updatedata" in data:
        if type(data["_updatedata"] == dict):
            updatedata = data["_updatedata"]
        else:
            updatedata = json.loads(data["_updatedata"])
        if "arkvolumes" in updatedata and updatedata["arkvolumes"]:
            modifieddata = updatedata
            modifieddata["arkvolumes"] = list(map (lambda x: type(x) == int and ArkVolume.query.get(x) or x,updatedata["arkvolumes"]))
    return arkmodel.update(Library,id=id,modifieddata=modifieddata)

@app.route('/api/%s/query'%Library.url,methods=['GET',"POST"])
def query_libraries():
    return arkmodel.query(Library)

@app.route('/api/%s/query'%ArkHost.url,methods=['GET',"POST"])
def query_arkhosts():
    return arkmodel.query(ArkHost)

@app.route('/api/%s/add'%ArkHost.url,methods=['POST'])
def add_arkhost():
    return arkmodel.add(ArkHost,column="name")

@app.route('/api/%s/delete/<int:id>'%ArkHost.url,methods=['POST'])
def delete_arkhost(id):
    return arkmodel.delete(ArkHost,id)

@app.route('/api/%s/update/<int:id>'%ArkHost.url,methods=['POST'])
def update_arkhost(id):
    return arkmodel.update(ArkHost,id)

@app.route('/api/%s/updatethumbnail/<int:id>'%ArkHost.url,methods=['GET',"POST"])
def update_arkhost_thumbnail(id):
    return arkmodel.update_thumbnail(id,ArkHost)  

