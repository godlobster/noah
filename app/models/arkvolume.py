from app import db
from app.models.arkmodel import ArkModel
from datetime import datetime

class ArkHost(ArkModel, db.Model):
    __tablename__ = "ArkHost"
    url = "host"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, nullable=False)
    hostname = db.Column(db.String(64), unique=True, nullable=False)
    description = db.Column(db.String(64))
    thumbnail = db.Column(db.LargeBinary)


    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(ArkHost)    

class ArkVolume(ArkModel, db.Model):
    __tablename__ = "ArkVolume"
    url = "volume"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=False, nullable=False)

    arkroot =  db.Column(db.String(64))
    mountpoint = db.Column(db.String(64))
    datecreated = db.Column(db.DateTime,default=datetime.now())
    host = db.Column(db.Integer,db.ForeignKey("ArkHost.id"))	
    thumbnail = db.Column(db.LargeBinary)

    storagetype = db.Column(db.Enum("Network Disk",
                                    "Internal",
                                    "Removable Media",
                                    "Dropbox",
                                    "Google Drive",
                                    "iCloud",
                                    "Other Cloud Service",
                                    name="arkvolumestoragetype"))

    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(ArkVolume)

arkvolumeslibraries = db.Table("arkvolumeslibraries", db.Model.metadata,
                             db.Column("arkvolumeid", db.Integer,
                                       db.ForeignKey('ArkVolume.id')),
                             db.Column("libraryid", db.Integer,
                                       db.ForeignKey('Library.id')),
)

class Library(ArkModel, db.Model):
    __tablename__ = "Library"
    url = "library"
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=False, nullable=False)
    path = db.Column(db.String(128), unique=False, nullable=False)
    
    # the home volume where this was created from
    arkvolumes = db.relationship("ArkVolume",uselist=True,
                                 secondary=arkvolumeslibraries)
    datecreated = db.Column(db.DateTime,default=datetime.now())
    thumbnail = db.Column(db.LargeBinary)
	

    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(Library)
