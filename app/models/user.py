from app import db,app
from datetime import datetime,timedelta
from app.models.arkmodel import ArkModel
from flask_security import RoleMixin,UserMixin
from flask_security.utils import encrypt_password,verify_password
import jwt
import sys,os

STATUSES = ["Active",
            "Inactive"]

# A base model for other database tables to inherit
class Base(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    createdat = db.Column(db.DateTime, default=db.func.current_timestamp())
    modifiedat = db.Column(db.DateTime, default=db.func.current_timestamp(),
                            onupdate=db.func.current_timestamp())

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(),
                                 db.ForeignKey('User.id')),
                       db.Column('role_id', db.Integer(),
                                 db.ForeignKey('auth_role.id')))
#                      info={'bind_key': 'users'} )


class Role(Base, RoleMixin):
    __tablename__ = 'auth_role'
   # __bind_key__ = 'users'

    name = db.Column(db.String(80), nullable=False, unique=True)
    description = db.Column(db.String(255))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Role %r>' % self.name


class User(Base, ArkModel, UserMixin):
    __tablename__ = 'User'
   # __bind_key__ = 'users'

    url = "user"
    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String)
    email = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), default="")
    firstname = db.Column(db.String(255))	
    lastname = db.Column(db.String(255))	
    administrator = db.Column(db.Boolean(),default=False)	

    # flask security
    active = db.Column(db.Boolean(), default=True)
    confirmed_at = db.Column(db.DateTime())
    registered_on = db.Column(db.DateTime, default=db.func.current_timestamp())
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(45))
    current_login_ip = db.Column(db.String(45))
    login_count = db.Column(db.Integer)
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', 
                                               lazy='dynamic'))
    connections = db.relationship('Connection',
                                  backref=db.backref('user', lazy='joined'), 
                                  cascade="all")
    authenticated = db.Column(db.Boolean(), default=False)

    # preferences
    filebrowserfavorites = db.Column(db.String(512))
    thumbnail = db.Column(db.LargeBinary)
    thumbnailfilename = db.Column(db.String(255))
    status = db.Column(db.Enum(*STATUSES,name="userstatuses"),default = "Active",name="statuses")
    group = db.Column(db.String)

    @property
    def serialise(self):
        columns = User.getcolumns()
        ret = {}
        for c in columns:
            if c=="id" and not getattr(self,c):
                continue
            if c=="connections":
                ret[c] = list(map(lambda x:x.serialise,getattr(self,c)))
                continue
            ret[c] = getattr(self,c)

        return ret

    
    def __repr__(self):
        return '<User %r>' % self.email
     
    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(User)

    def is_active(self):
        return self.active

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.email

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False

    def set_password(self, password):
        self.password = encrypt_password(password)

    # def set_password(self, password):
    #     """Set password."""
    #     self.password = bcrypt.generate_password_hash(password).decode('utf-8')
    #
    def check_password(self, password):
        return verify_password(self.password, password)

    def encode_auth_token(self, user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.utcnow() + timedelta(days=1, seconds=0),
                'iat': datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                app.config.get('SECRET_KEY'),
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Validates the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'))
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

class Connection(db.Model, ArkModel):
 #   __bind_key__ = 'users'
 
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('User.id'))
    provider_id = db.Column(db.String(255))
    provider_user_id = db.Column(db.String(255))
    access_token = db.Column(db.String(255))
    secret = db.Column(db.String(255))
    display_name = db.Column(db.String(255))
    full_name = db.Column(db.String(255))
    profile_url = db.Column(db.String(512))
    image_url = db.Column(db.String(512))
    rank = db.Column(db.Integer)
    email = db.Column(db.String)
    password = db.Column(db.String)
    name = db.Column(db.String)
    
    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(Connection)

    @property
    def serialise(self):
        columns = Connection.getcolumns()
        ret = {}
        for c in columns:
            print (c)
            if c=="id" and not getattr(self,c):
                continue
            if c=="user":
                ret[c] = getattr(self,c).id
                continue
            ret[c] = getattr(self,c)

        return ret

class BlacklistToken(db.Model):
    """
    Token Model for storing JWT tokens
    """
    __tablename__ = 'blacklist_tokens'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), unique=True, nullable=False)
    blacklisted_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.now()

    def __repr__(self):
        return '<id: token: {}'.format(self.token)

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted
        res = BlacklistToken.query.filter_by(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False
