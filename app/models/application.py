from app import db
from app.models.arkmodel import ArkModel

class Application(ArkModel,db.Model):
	__tablename__ = "Application"
	url = "application"
	
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	name = db.Column(db.String(100), nullable=False)
	thumbnail = db.Column(db.LargeBinary)
	path = db.Column(db.String(128))

	@staticmethod
	def getcolumns():
		return ArkModel.getcolumns(Application)

class ArkEntryApplication(ArkModel,db.Model):
	__tablename__ = "ArkEntryApplication"
	url = "arkentryapplication"
	
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	thumbnail = db.Column(db.LargeBinary)
	application = db.Column(db.Integer,db.ForeignKey("Application.id"))
	arkentry = db.Column(db.Integer,db.ForeignKey("ArkEntry.id"))
	environment = db.Column(db.String(1024))	

	@staticmethod
	def getcolumns():
		return ArkModel.getcolumns(ArkEntryApplication)	

