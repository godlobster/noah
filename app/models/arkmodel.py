import inspect
from app import db

class ArkModel:
	@staticmethod
	def getcolumns(derivedclass):
		attributes = inspect.getmembers(derivedclass, lambda a:not(inspect.isroutine(a)))
		attributes = filter(lambda x: not x[0][0].startswith("_"),attributes)
		attributes = list(map(lambda x:x[0],attributes))
		omit = ["serialise","query","query","metadata","query_class","url"]
		omit += ["thumbnail","poster","image"]
		for x in omit:
			if x in attributes:
				attributes.remove(x)

		return attributes

	@property
	def serialise(self):
		columns = self.__class__.getcolumns()
		ret = {}
		for c in columns:
			if c=="id" and not getattr(self,c):
				continue
			ret[c] = getattr(self,c)

		return ret

