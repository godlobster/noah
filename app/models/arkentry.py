from app import db
from app.models.arkmodel import ArkModel
from datetime import datetime

STATUSES = ["Active",
            "Complete",
            "Waiting",
            "On Hold"] 

prefstagstable = db.Table("PrefsTag", db.Model.metadata,
                          db.Column("prefsid", db.Integer,
                                    db.ForeignKey('ArkEntryPrefs.id')),
                          db.Column("tagid", db.Integer,
                                    db.ForeignKey('Tag.id')),
                         )
arkprefsapps = db.Table("ArkPrefsApps", db.Model.metadata,
                        db.Column("prefsid", db.Integer,
                                  db.ForeignKey('ArkEntryPrefs.id')),
                        db.Column("arkentryappid", db.Integer,
                                  db.ForeignKey('ArkEntryApplication.id')),
                       )
entryuserstable = db.Table("EntryUsers", db.Model.metadata,
                          db.Column("entryid", db.Integer,
                                    db.ForeignKey('ArkEntry.id')),
                          db.Column("userid", db.Integer,
                                    db.ForeignKey('User.id')),
                         )
subscribedentryuserstable = db.Table("SubscribedEntryUsers", db.Model.metadata,
                          db.Column("entryid", db.Integer,
                                    db.ForeignKey('ArkEntry.id')),
                          db.Column("userid", db.Integer,
                                    db.ForeignKey('User.id')),
                         )

class Tag(ArkModel, db.Model):
    __tablename__ = "Tag"
    url = "tag"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(50),nullable=False,unique=True)
    image = db.Column(db.LargeBinary)
    imagefilename = db.Column(db.String(128))

    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(Tag)

class ArkEntryPrefs(ArkModel,db.Model):
    __tablename__ = "ArkEntryPrefs"
    url = "arkentryprefs"
	
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    arkentryid = db.Column(db.Integer,db.ForeignKey("ArkEntry.id"))
    thumbnail = db.Column(db.LargeBinary)
    poster = db.Column(db.LargeBinary)
    thumbnailfilename = db.Column(db.String(128))
    thumbnailanimatedfilename = db.Column(db.String(128))
    posterfilename = db.Column(db.String(128))
    bannerfilename = db.Column(db.String(128))
    backgroundfilename = db.Column(db.String(128))
    userimage1filename = db.Column(db.String(128))
    userimage2filename = db.Column(db.String(128))
    userimage3filename = db.Column(db.String(128))
    userimage4filename = db.Column(db.String(128))
    userimage5filename = db.Column(db.String(128))
    userimage6filename = db.Column(db.String(128))
    userimage7filename = db.Column(db.String(128))
    userimage8filename = db.Column(db.String(128))
    userimage9filename = db.Column(db.String(128))
    userimage10filename = db.Column(db.String(128))

    status = db.Column(db.Enum(*STATUSES,name="arkentryprefsstatuses"),default = "Active")
    longname = db.Column(db.String(256))
    shortname = db.Column(db.String(128))
    description = db.Column(db.String(256))	
    welcomemessage = db.Column(db.String(300))
    inherit = db.Column(db.String(300),default="thumbnail poster welcomemessage description")
    tags =  db.relationship("Tag",
                            uselist=True,
                            secondary=prefstagstable,
                            backref="arkentryprefs")
    trellolink = db.Column(db.String(128))
    dropboxfolder = db.Column(db.String(128))
    googledrivefolder = db.Column(db.String(128))
    filebrowserfavorites = db.Column(db.String(512))
    userpref0 = db.Column(db.String(512))    
    userpref1 = db.Column(db.String(512))
    userpref2 = db.Column(db.String(512))
    userpref3 = db.Column(db.String(512))    
    userpref4 = db.Column(db.String(512))
    userpref5 = db.Column(db.String(512))
    userpref6 = db.Column(db.String(512))
    userpref7 = db.Column(db.String(512))
    userpref8 = db.Column(db.String(512))
    userpref9 = db.Column(db.String(512))
    applications = db.relationship("ArkEntryApplication",
                                   uselist=True,
                                   secondary=arkprefsapps,
                                   backref="arkentryprefs")
    environment = db.Column(db.String(1024))
    clipboard = db.Column(db.String(1024))

    def getinheritance(self):
        result = {}
        columns = self.inherit.split()
        def _getinherited(column,arkentry=self.arkentry):
            val = getattr(arkentry.prefs,column)
            if val:
                return val
            elif arkentry.parent:
                parent = ArkEntry.query.get(arkentry.parent)
                return _getinherited(column,arkentry=parent)
            else:
                return
		
        for column in columns:
            if not getattr(self,column):
                val = _getinherited(column)
                if val:
                    result[column] = val
        return result

    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(ArkEntryPrefs)

arkvolumesentries = db.Table("arkvolumesentries", db.Model.metadata,
                             db.Column("arkvolumesid", db.Integer,
                                       db.ForeignKey('ArkVolume.id')),
                             db.Column("arkentryid", db.Integer,
                                       db.ForeignKey('ArkEntry.id')),
)

class ArkEntry(ArkModel, db.Model):
    __tablename__ = "ArkEntry"
    url = "entry"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(512), nullable=False)
    path = db.Column(db.String(300),unique=True,nullable=False)
    fullpath = db.Column(db.String(512))
    arkvolumes = db.relationship("ArkVolume",uselist=True,
                                 secondary=arkvolumesentries)
    library = db.Column(db.Integer,db.ForeignKey("Library.id"), nullable=False)	
    parent = db.Column(db.Integer,db.ForeignKey("ArkEntry.id"))	
    children = db.relationship('ArkEntry',
                               uselist=True)
    level=db.Column(db.Integer)
    datecreated = db.Column(db.DateTime,default=datetime.now())
    lastworkedon = db.Column(db.DateTime,default=datetime.now())
    owner = db.Column(db.Integer)
    users =  db.relationship("User",
                             uselist=True,
                             secondary=entryuserstable,
                             backref="arkentries")  # these are the interested users or creators.
    subscribedusers =  db.relationship("User",
                             uselist=True,
                             secondary=subscribedentryuserstable,
                             backref="subscribedarkentries")  # these are the paid subscribers
    thumbnail = db.Column(db.LargeBinary)
    description = db.Column(db.String(100))
    status = db.Column(db.Enum("Active","Complete","Hidden","On Hold",name="arkentrystatuses"))
    prefs = db.relationship("ArkEntryPrefs",uselist=False,backref="arkentry")
    userdata0 = db.Column(db.String(512))    
    userdata1 = db.Column(db.String(512))
    userdata2 = db.Column(db.String(512))
    userdata3 = db.Column(db.String(512))
    userdata4 = db.Column(db.String(512))
    userdata5 = db.Column(db.String(512))
    userdata6 = db.Column(db.String(512))
    userdata7 = db.Column(db.String(512))
    userdata8 = db.Column(db.String(512))
    userdata9 = db.Column(db.String(512))
    
    @staticmethod
    def getcolumns():
        return ArkModel.getcolumns(ArkEntry)
