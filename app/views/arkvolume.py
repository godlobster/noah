from flask import render_template, flash, redirect, url_for, request, jsonify
from flask.ext.security import login_required

from app import app, db

from app.models.arkvolume import ArkVolume

@app.route('/%s/get'%ArkVolume.url,methods=['GET'])
@login_required
def get_arkvolumes():
	result=ArkVolume.query.order_by(ArkVolume.name).all()

	def json_builder(data):
		columns = ArkVolume.getcolumns()
		ret = {}
		for c in columns:
			ret[c] = getattr(data,c)
		return ret
#	data = jsonify(arkvolumes=[json_builder(x) for x in result])
	return render_template('arkvolumes.html', title='Atrax ArkVolumes', volumes=result)
