from flask import render_template, flash, redirect, url_for, request, jsonify, send_from_directory,send_file,\
    current_app
from flask.ext.security import LoginForm, current_user, login_required, \
    login_user,RegisterForm
from flask.ext.security.decorators import anonymous_user_required

from flask.ext.security.utils import get_post_login_redirect

from app.models.user import User,Role,Connection
from app.models.arkvolume import Library

from app import app,db
import webconfig

from app.views.arkentry import ArkEntry,get_arkentries
from utils import _json_builder_from_model

@app.login_manager.user_loader
def user_loader(user_id):
    """Given *user_id*, return the associated User object.

    :param unicode user_id: user_id (email) user to retrieve
    """
    return User.query.filter(User.email == user_id).first()

@app.route('/')
@app.route('/index')
def index():
    if current_user.is_authenticated:
        return get_arkentries()
 
    return render_template('index.html', title='The Ark')

    # if current_user.is_authenticated():
        #return render_template('index.html', title='The Ark',
        #                       form=LoginForm())
    #    pass
        #return redirect('/postlogin')
@app.route('/testlogin', methods=['GET', 'POST'])
def testlogin():
    print ("testlogin")
    form = LoginForm()
    if form.validate_on_submit():
        user = form.user
        print ("   validform, user=",user)
    return render_template('login.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return get_arkentries()
 
    return render_template('index.html', title='The Ark')

@app.route('/postlogin', methods=['GET', 'POST'])
def postlogin():
    current_user.authenticated = True
    db.session.commit()
    
    return get_arkentries()

@app.route('/mysecuritylogin', methods=['GET', 'POST'])
@anonymous_user_required
def mysecuritylogin():
    print ("my security login")
    form = LoginForm()
    if form.validate_on_submit():
        user = form.user
        print ("   validform, user=",user)
        if user:
            if user.check_password(form.password.data):
                print ("   password accepted. loggin in user")
                user.authenticated = True
                db.session.add(user)
                db.session.commit()
                login_user(user, form.remember.data)
                print ("   user logged in.  current_user==",current_user)

                return redirect(get_post_login_redirect())
    else:
        print ("Invalid Form")
        user = User.query.filter(User.email==form.email.data).first()
   #     print "user",user
    #    print "password",user.password
    #    print form.data
    #    print form.password.data
    #    print user.password == form.password.data         
        
    return render_template("login.html", form=form)

@app.route('/postlogout', methods=['GET', 'POST'])
def postlogout():
    print ("postlogout")
    current_user.authenticated=False
    db.session.commit()
    
    return redirect("/")

def _get_user_from_connection_values(connection_values):
    data= {}
            
    if connection_values["provider_id"] == "google":
        data["firstname"] = connection_values["full_name"].split(";")[0].strip()
        data["lastname"] = connection_values["full_name"].split(";")[-1].strip()
        data["username"] = connection_values["display_name"] or \
                            connection_values["email"].split("@")[0]
        
        data["thumbnail"] = connection_values["image_url"]    
        data["password"] = connection_values["password"]
        data["email"] = connection_values["email"]
        if not data["firstname"] and not data["lastname"]:
            data["firstname"] = data["username"]
        
            
    return data

@app.route('/register', methods=['GET', 'POST'])
@app.route('/register/<provider_id>', methods=['GET', 'POST'])
def register(provider_id=None):
    print ("app.views.register ",provider_id)
    
    if current_user.is_authenticated:
        return redirect(request.referrer or '/profile')

    form = RegisterForm()

    provider = None
    connection_values = None

    print ("app.views.register: provider=",provider)
    print ("app.views.register: validating...")
    if form.validate_on_submit():
        # using registration form valid.  User safe to create
        
        print ("app.views.register: validated")
        ds = current_app.security.datastore

        if connection_values:
            userdata=_get_user_from_connection_values(connection_values)

            user = ds.create_user(email=form.email.data, 
                                  username=userdata["username"] or "",
                                  firstname=userdata["firstname"] or "",
                                  lastname=userdata["lastname"] or "",
                                  thumbnail=userdata["thumbnail"] or "")
            user.set_password(form.password.data)
        else:
            user = ds.create_user(email=form.email.data)
            user.set_password(form.password.data)

        ds.commit()
        print ("app.views.register: user created = ",user)

        if login_user(user):
            ds.commit()
            print ("app.views.register: account created successfully")

            flash('Account created successfully', 'info')
            return redirect(url_for('postlogin'))

        return render_template('thanks.html', user=user)

    elif connection_values:
        print ("app.views.register: form invalid but we still have social provider info")

     #   userdata=_get_user_from_connection_values(connection_values)

        print ("app.views.register: check if there's already a social account registered: %s %s .."%(provider_id,
                                                                                                    connection_values["provider_user_id"]))

        # we might still be able to login if user has registered with his
        # social account.
        connection = current_app.social.datastore.find_connection(provider_id=provider_id,
                                                                  provider_user_id = connection_values["provider_user_id"])

        if connection:
            login_user(connection.user)
            ds.commit()
            return redirect(url_for('postlogin'))
    
        # no matching social account found.  Try and autoregister with just social account info
        
        print ("app.views.register: No account.  see if we can auto login or register with social account ..")
        ds = current_app.security.datastore
        
        # first we need to check if this user already has an account but no social
        # login connection
        user = ds.get_user(connection_values["email"])
        if not user:
            user = ds.create_user(email=connection_values["email"] or "", 
                                  password=connection_values["password"] or "",
                                  username=connection_values["display_name"] or "",
                                  firstname=connection_values["full_name"].split(";")[0].strip(),
                                  lastname=connection_values["full_name"].split(";")[-1].strip(),
                                  thumbnail=connection_values["image_url"])
            ds.commit()
        
            connection_values['user_id'] = user.id
            connect_handler(connection_values, provider)

            if login_user(user):
                ds.commit()
                print ("app.views.register: account created successfully")

                flash('Account created successfully', 'info')
                return redirect(url_for('postlogin'))
        else:
            print ("app.views.register: User account already exists for "+connection_values["email"])
            # we have user who is logged into google and has an account but no connection
            # account.  log him in??  
            login_user(user)
            ds.commit()
            return redirect(url_for('postlogin'))

            myloginerror = 1
    
  #  if myloginerror:
  #      login_failed=1
  #  else:
    login_failed = int(request.args.get('login_failed', 0))
    print ("app.views.register - invalid form",login_failed, provider)

    return render_template('register.html',
                           form=form,
                           provider=provider,
                           login_failed=login_failed,
                           connection_values=connection_values)

@app.route('/profile')
@login_required
def profile():
    return render_template('profile.html',
                           facebook_conn=current_app.social.facebook.get_connection(),
                           google_conn=current_app.social.google.get_connection())

@app.route('/users')
@login_required
def users():
    users = User.query.all()
    return render_template('users.html',
                           users = users)
@app.route('/test')
def test():
    users = User.query.all()
    for x in users:
        print (x.serialise)
    
    usersd = list(map (lambda x:x.serialise,users))
    print (usersd)
    return render_template('test.html')

@app.route('/plugin/archive')
def plugin_archive():
    return jsonify(path=webconfig.PLUGIN_ARCHIVE)

@app.route('/libraries', methods=['GET', 'POST'])
@login_required
def libraries():
    libraries = Library.query.all()
    libraries = list(map(lambda x: _json_builder_from_model(x,Library),libraries))
    return render_template('libraries.html',libraries=libraries)


