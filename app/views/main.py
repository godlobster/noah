from flask import render_template, flash, redirect, url_for, request, jsonify, make_response,current_app
from flask.ext.security import login_required
from flask.ext.security import current_user

from sqlalchemy import desc, and_

import datetime
import os
import json
import subprocess
import xml.etree.ElementTree
import types

from app import app, db

from app.models.arkentry import ArkEntry,ArkEntryPrefs
from app.models.arkvolume import ArkVolume,ArkHost,Library
from app.models.application import Application
import webconfig
from utils import _json_builder_from_model

def _getparents(arkentry):
	parents = []
	parent = arkentry.parent
	while parent:
		parententry = ArkEntry.query.get(parent)
		parents.append(parententry)
		parent = parententry.parent
	parents.reverse()

	return parents

@app.route('/systemprefs',methods=['GET'])
@login_required
def systemprefs():
    applications = Application.query.order_by(Application.name).all()
    volumes = ArkVolume.query.order_by(ArkVolume.name).all()
    hosts = ArkHost.query.order_by(ArkHost.name).all()
    libraries = Library.query.all()
    libraries = list(map(lambda x: _json_builder_from_model(x,Library),libraries))
    return render_template('systemprefs.html', title='Preferences',
                            applications=list(map(lambda x:x.serialise,applications)),
                            volumes=list(map(lambda x:x.serialise,volumes)),
                            hosts=list(map(lambda x:x.serialise,hosts)),
                            libraries=libraries,
                            facebook_conn = current_app.social.facebook and  current_app.social.facebook.get_connection(),
                            google_conn = current_app.social.google and current_app.social.google.get_connection())


@app.route('/environment',methods=['GET'])
@login_required
def environment():
    return render_template('environment.html', title='Environment')


@app.route('/filebrowser',methods=['GET'])
@login_required
def filebrowser():
    from urllib.request import urlopen, Request

    SEND_URL = webconfig.CLIENT_URL+"/api/filebrowser"
    request2 = Request(SEND_URL)
    data = """{"path":"/Users",
               "filters":["dironly","hidden"]}"""
    request2.add_header('Content-Type', 'application/json')

    try:
        response = urlopen(request2,data)
    except:
        return "Can't connect to client at: "+SEND_URL
    
    code = response.code
    message   = response.read()
    data = None

    if message:
        data = json.loads(message)
    response.close()

    return render_template('filebrowser.html', title='filebrowser',files=data["data"][0]["directorylisting"],
                           path="/Users")
