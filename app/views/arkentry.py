from flask import render_template
from flask.ext.security import login_required

from app import app, db

from app.models.arkentry import ArkEntry,ArkEntryPrefs
from app.models.arkvolume import ArkVolume, Library
from app.models.application import Application
from app import models

from utils import _json_builder, _json_builder_from_model

def _getparents(arkentry):
	parents = []
	parent = arkentry.parent
	while parent:
		parententry = ArkEntry.query.get(parent)
		parents.append(parententry)
		parent = parententry.parent
	parents.reverse()

	return parents

@app.route('/%s'%ArkEntry.url,methods=['GET'])
@login_required
def get_arkentries():
    result=ArkEntry.query.filter(ArkEntry.level==0).order_by(ArkEntry.name).all()
    
    parents = []
    if result:
        parents = _getparents(result[0])
    thumbnails = {}
    for record in result:
        if record.prefs.thumbnail:
            continue
        else:
            for i in range(len(parents)-1,0,-1):
                parent = parents[i]
                if parent.prefs.thumbnail:
                    record.prefs.thumbnail = parent.prefs.thumbnail 
                    thumbnails[record.id] = parent.prefs.thumbnail
                    break
                    
    entries = [_json_builder_from_model(x, model=ArkEntry) for x in result]
    #print "app.views.arkentry.get_arkentries:", entries[0]["thumbnail"]
    
    libraries = Library.query.all()
    libraries = [_json_builder_from_model(x, model=Library) for x in libraries]

    return render_template('arkentries.html', 
                           title='ArkEntries',
                           entries=entries,
                           parents=parents,
                           thumbnails=thumbnails,
                           libraries = libraries)

@app.route('/%s/<int:level>/<int:id>'%ArkEntry.url,methods=['GET'])
@login_required
def get_arksubentries(level,id,mode=1):
    print("mode",mode)
    parentid=id
    result=ArkEntry.query.filter(ArkEntry.level==level,
                                 ArkEntry.parent==parentid).order_by(ArkEntry.name).all()

    parents = []
    if result:
        parents = _getparents(result[0])

    thumbnails = {}
    for record in result:
        if record.prefs.thumbnail:
            continue
        else:
            for i in range(len(parents)-1,-1,-1):
                parent = parents[i]
                if parent.prefs.thumbnail:
                    record.prefs.thumbnail = parent.prefs.thumbnail
                    break
    result   = [_json_builder_from_model(x, model=ArkEntry) for x in result]

    libraries = Library.query.all()
    libraries = list(map(lambda x: _json_builder_from_model(x,Library),libraries))
    arkentry=len(result) and result[0]
    return render_template('arkentries.html', 
                           title='ArkEntries',
                           entries=result, 
                           parents=parents,
                           thumbnails=thumbnails,
                           arkentry=arkentry,
                           libraries=libraries)

@app.route('/%s/<int:id>'%ArkEntry.url,methods=['GET'])
@login_required
def get_arkentry(id):
    arkentry = ArkEntry.query.get(id)
    
    preferences = ArkEntryPrefs.query.filter(ArkEntryPrefs.arkentry == arkentry).first()
    parents = _getparents(arkentry)

    inheritedprefs = preferences.getinheritance()

    applications = list(map(lambda a: a.application,preferences.applications))
    applications = list(map(Application.query.get,applications))

    applications = list(map(lambda x: _json_builder_from_model(x,Application),applications))
    allapps = Application.query.all()
    allapps = list(map(lambda x: _json_builder_from_model(x,Application),allapps))
    
    preferences =  _json_builder_from_model(preferences,ArkEntryPrefs)
    arkentry =  _json_builder_from_model(arkentry,ArkEntry)
    # resolve inherited prefs
    return render_template('arkentry.html', title='ArkEntry',
                           arkentry=arkentry,
                           preferences=preferences,
                           parents=parents,
                           inherited = inheritedprefs,
                           applications=applications,
                           allapps=allapps)

@app.route('/%s/arkentryprefs/<int:id>'%ArkEntry.url,methods=['GET'])
@login_required
def get_arkentryprefs(id):
    arkentry = ArkEntry.query.get(id)
    preferences = ArkEntryPrefs.query.filter(ArkEntryPrefs.arkentry == arkentry).first()
    columns = preferences.getcolumns()
    parents = _getparents(arkentry)

    # resolve inherited prefs
    inheritedprefs = preferences.getinheritance()

    arkentry = _json_builder_from_model(arkentry,ArkEntry)
    arkentry["_statuses"] = models.arkentry.STATUSES
    
    preferences = _json_builder_from_model(preferences,ArkEntryPrefs)
    preferences["columns"] = columns

    return render_template('arkentryprefs.html', title='ArkEntry',
                           arkentry=arkentry,
                           preferences=preferences,
                           parents=parents,
                           inherited = inheritedprefs)

@app.route('/%s/environment/<int:id>'%ArkEntry.url,methods=['GET'])
@login_required
def get_arkentryenv(id):
    arkentry = ArkEntry.query.get(id)
    arkentry = arkentry and _json_builder_from_model(arkentry,ArkEntry)
    return render_template('environment.html', 
                           title='Environment',
                          arkentry=arkentry)

@app.route('/%s/addarkentrytree'%ArkEntry.url,methods=['GET'])
@login_required
def add_arkentrytree():
    return render_template('addarkentrytree.html')
