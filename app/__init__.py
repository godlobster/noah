from flask import Flask,session,redirect,url_for
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
from flask_bcrypt import Bcrypt


# -------   Primary Setup
app = Flask(__name__)
cors = CORS(app, resources={r"/foo": {"origins": "*"}})


app.config['CORS_HEADERS'] = 'Content-Type'

app.config.from_object('webconfig')
app.config['WTF_CSRF_ENABLED'] = False 

db = SQLAlchemy(app)
#print ("database connected...")
bcrypt = Bcrypt(app)

# from sqlalchemy import MetaData
#
# metadata = MetaData()
# metadata.reflect(bind=db.engine)
# for table in metadata.sorted_tables:
#     print(table)
#
#from app.momentjs import momentjs
#app.jinja_env.globals['momentjs'] = momentjs
app.jinja_env.globals['len'] = len

# -------   Tornado & Websockets Setup
from tornado.wsgi import WSGIContainer
from tornado import web
from tornado import websocket

class WSHandler(websocket.WebSocketHandler):
    clients = []
    def open(self):
        WSHandler.clients.append(self)
        print ('connection opened...')

    def on_message(self, message):
        # application logic
        print ('received:', message)
        self.write_message("Roger that")

    def on_close(self):
        WSHandler.clients.remove(self)
        print ('connection closed...')

tr = WSGIContainer(app)
application = web.Application([
    (r'/ws', WSHandler),
    (r".*", web.FallbackHandler, dict(fallback=tr)),
])

# -------- REST-less api
#import flask.ext.restless
#manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)

#---------   flask login
from . import models

from flask_security import Security
from flask_security.datastore import SQLAlchemyUserDatastore

user_datastore = SQLAlchemyUserDatastore(db, models.user.User, models.user.Role)
security = Security(app, user_datastore)
app.security = security


from . import views, api, auth

app.register_blueprint(auth.views.auth_blueprint)
