1. The SDK files are located in the ngwidgets directory
 
 In general you need to use files from this directory only.

 Files list & description:

  Files required in all projects using the SDK:

  ngxcore.js: Core ngwidgets framework

  Stylesheet files. Include at least one stylesheet Theme file and the images folder:

  styles/ngx.base.css: Stylesheet for the base Theme. The ngx.base.css file should be always included in your project.
  
  styles/ngx.glacier.css: Stylesheet for the Glacier Theme
  styles/ngx.arctic.css: Stylesheet for the Arctic Theme
  styles/ngx.web.css: Stylesheet for the Web Theme
  styles/ngx.black.css: Stylesheet for the Black Theme
  styles/ngx.blackberry.css: Stylesheet for the Blackberry Theme
  styles/ngx.bootstrap.css: Stylesheet for the Bootstrap Theme
  styles/ngx.classic.css: Stylesheet for the Classic Theme
  styles/ngx.darkblue.css: Stylesheet for the DarkBlue Theme
  styles/ngx.energyblue.css: Stylesheet for the EnergyBlue Theme
  styles/ngx.fresh.css: Stylesheet for the Fresh Theme
  styles/ngx.highcontrast.css: Stylesheet for the High Contrast Theme
  styles/ngx.metro.css: Stylesheet for the Metro Theme
  styles/ngx.metrodark.css: Stylesheet for the Metro Dark Theme
  styles/ngx.mobile.css: Stylesheet for the Mobile Theme
  styles/ngx.office.css: Stylesheet for the Office Theme
  styles/ngx.orange.css: Stylesheet for the Orange Theme
  styles/ngx.shinyblack.css: Stylesheet for the ShinyBlack Theme
  styles/ngx.summer.css: Stylesheet for the Summer Theme


  styles/images: contains images referenced in the stylesheet files
  
  Files for individual widgets and plug-ins. Include depending on project needs:
	
  ngxbutton.js: Button, RepeatButton, SubmitButton & ToggleButton widgets
  ngxbuttongroup.js: Button Group widget
  ngxcalendar.js: impements Calendar widget
  ngxchart.js: Chart widget
  ngxcheckbox.js: CheckBox widget   
  ngxcombobox.js: ComboBox widget
  ngxdata.js: Data Source plug-in  
  ngxdata.export.js: Data export plug-in  
  ngxdropdownbutton.js: DropDownButton widget
  ngxdatetimeinput.js: impements DateTimeInput widget
  ngxdropdownlist.js: DropDownList widget
  ngxgridview.js: GridView widget
  ngxlistbox.js: ListBox widget
  ngxmenu.js: Menu widget
  ngxnumberinput.js: NumberInput TextBox widget
  ngxprogressbar.js: ProgressBar widget
  ngxresponse.js: Response plug-in  
  ngxradiobutton.js: RadioButton widget   
  ngxswitchbutton.js: Switch Button widget
  ngxscrollbar.js: ScrollBar widget
  ngxslider.js: Slider widget
  ngxtabs.js: Tabs widget
  ngxtree.js: Tree widget
  ngxtooltip.js: ToolTip widget
  ngxwindow.js: Window widget

2.Examples

  Individual widget examples are located in the /demos directory
  Any examples that use Ajax or ngWidgets DataAdapter need to be on a Web Server in order to work correctly. 

3.Documentation

  Browse the documentation and examples through the index.htm file
  Individual documentation files are located in the /documentation directory
   
4.Other files

  The /scripts, /images, /styles folders contain the files used by the demo and documentation only.

5.License & Purchase

   For more information regarding the licensing, please visit: http://www.ngwidgets.com/license

