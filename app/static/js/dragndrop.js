var dragSrcEl = null;

function handleDragStart(e) {
  // Target (this) element is the source node.
  this.style.opacity = '0.4';

  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
}

function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault(); 
  }

  e.dataTransfer.dropEffect = 'move';

  return false;
}

function handleDragEnter(e) {
    this.classList.add('over');
}

function handleDragLeave(e) {
    this.classList.remove('over'); 
}

function handleDrop(e) {
  // this/e.target is current target element.
	e.preventDefault();
    e.stopPropagation(); // Stops some browsers from redirecting.
//  if (e.stopPropagation) {
//    e.stopPropagation(); // Stops some browsers from redirecting.
  //}

  // Don't do anything if dropping the same column we're dragging.
  if (dragSrcEl != this && dragSrcEl) {
	  e.preventDefault();

	  if (e.dataTransfer.files.length) {
		  // handle a file being dropped
		  readfiles(e.dataTransfer.files)
	  }
	  else {
		  // Set the source column's HTML to the HTML of the column we dropped on.
		  dragSrcEl.innerHTML = this.innerHTML;
		  this.innerHTML = e.dataTransfer.getData('text/html');
	  }
  }

	this.style.opacity = '1';  // this / e.target is the source node.
	this.classList.remove('over'); 	

	if (dragSrcEl)
		dragSrcEl.style.opacity = '1';  // this / e.target is the source node.

	return false;
}

function readfiles(files) {
//	console.log("filename:"+files[0].FileName)
//    var formData = tests.formdata ? new FormData() : null;
//    for (var i = 0; i < files.length; i++) {
//      if (tests.formdata) formData.append(theholder, files[i]);
//		previewfile(files[i],theholder,index);
//    }   

    // now post a new XHR request 
//    if (tests.formdata) {
//		var xhr = new XMLHttpRequest();
//		if (arkentryid)
//			xhr.open('POST', '/api/arkentry/updateprefs/2'.replace('2',arkentryid.content));
//		else {
//			console.log("here");			
//			xhr.open('POST', '/api/arkentry/updatethumbnail/2'.replace('2',arkentriesids[i].content) +
//					"/"+index);
//		}
//		xhr.onload = function() {
//			if (progress)
//				progress.value = progress.innerHTML = 100;
//      };
//    };
//      xhr.send(formData);
}

function handleDragEnd(e) {
  // this/e.target is the source node.
	e.preventDefault();
    var cols = document.querySelectorAll('#columns .column');

  [].forEach.call(cols, function (col) {
      col.classList.remove('over');
	    col.style.opacity = '1';  // this / e.target is the source node.

  });
}

function Init() {
    var cols = document.querySelectorAll('#columns .column');
    console.log(cols);
    [].forEach.call(cols, function(col) {
      col.addEventListener('dragstart', handleDragStart, false);
      col.addEventListener('dragenter', handleDragEnter, false)
      col.addEventListener('dragover', handleDragOver, false);
      col.addEventListener('dragleave', handleDragLeave, false);
      col.addEventListener('drop', handleDrop, false);
      col.addEventListener('dragend', handleDragEnd, false);
    });
}

$(document).ready(function () {
    Init();
});
