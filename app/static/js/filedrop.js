
// call initialization file
if (window.File && window.FileList && window.FileReader) {
	Init();
}

//
// initialize
function Init() {
    console.log("Init")
    var posterholder = document.getElementById('filedropholder')

        acceptedTypes = {
            'image/png': true,
            'image/jpeg': true,
            'image/gif': true 
        },

	
		// file drop
		filedropholder.addEventListener("dragover", FileDragHover, false);
		filedropholder.addEventListener("dragleave", FileDragHover, false);
		filedropholder.addEventListener("drop", FileSelectHandler, false);
		filedropholder.style.display = "block";
		
}

function remove(arr, item) {
      for(var i = arr.length; i--;) {
          if(arr[i] === item) {
              arr.splice(i, 1);
          }
      }
  }

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
    var current = e.target.className.split(" ")
	remove(current,"hover")
	classes = current.join(" ")
	e.target.className = (e.type == "dragover" ? "hover "+classes : classes);

    var current = filedropholder.className.split(" ")
	remove(current,"hover")
	classes = current.join(" ")
	filedropholder.className = (e.type == "dragover" ? "hover "+classes : classes);
}


// file selection
function FileSelectHandler(e) {

	// cancel event and hover styling
	FileDragHover(e);

	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;

	// process all File objects
	for (var i = 0, f; f = files[i]; i++) {
		ParseFile(f);
	}

}


function ParseFile(file) {
	
}