angular.module("slickselect", [])
    
.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol("[[");
    $interpolateProvider.endSymbol("]]");
})

.directive('slickselect', ["$timeout", function () {
    return {
        restrict: 'AEC', 
        replace: false,
        transclude:false,
        templateUrl:"/static/templates/slickselect2.html",
        scope:  {data: '=',
                 maxelements:'@',
                 currentelement:'=',
                 addbutton:'@',
                 searchbutton:'@',
                 vertical:'@',
                 showlabel:'@',
                 imgwidth:'@',
                 onenter:'&',
                 onkeypress:'&',
                 onbuttonclick:'&',
                 classname:'@',
                 centerModeOffset:'@',
                },
        controller:"slickselectController",
        link: function(scope,elem,attrs) {

        },
        compile: function(elem, attrs, transclude) {
            return function link(scope,elem,attrs,ctr) {
                $(document).ready(function () {
                    scope.elements = scope.data;
                    $("."+scope.classname).slick({
                        centerMode: true,
                        infinite: true,
                        centerPadding: '0px',
                        slidesToShow: scope.maxelements,
                        speed: 500,
                        vertical: scope.vertical == "true" ? true : false,
                        draggable: true,
                        focusOnSelect: true,
                        arrows: false,
                        useCss:true,
                        useArrowKeys:false,
                        centerModeOffset:scope.centerModeOffset,
                    })
            for (i=0;i<scope.data.length;i++) {
                var thumbnail = scope.data[i].thumbnail ? scope.data[i].thumbnail : "/static/images/disabledlibrary.png"
                $("."+scope.classname).slick("slickAdd",'\
                <div><h3 ng-show="true">\
                    <div class ="slickbutton"\
                         style=\'padding:5px; margin:0px\'>\
                        <img src="'+thumbnail+'" \
                             width='+scope.imgwidth+' \
                             style="float:left;\
                                     padding-top:5px;\
                                     margin:0px;\
                                     object-fit:contain;\
                                     max-height:100%">\
                        <div ng-show="'+scope.showlabel+'" \
                             style="height:10px"></div>\
                        <span ng-show="'+scope.showlabel+'"> \
                            '+scope.data[i].name +' \
                        </span> \
                    </div>\
                </h3></div>\
                ')
            }
            });
            }
        }
    }
}])

.controller("slickselectController", function ($scope) {
    $scope.slickchild = {} 
    $scope.isinitialized = false
    if (!$scope.data)
        $scope.data = []
    if ($scope.addbutton == "true") {
        $scope.data.push({
            "name": "Add",
            "thumbnail": "/static/images/plusicon.png",
            "disabled":false,
            "slick" :true,
        })
    }
    if ($scope.searchbutton == "true") {
        $scope.data.push({
            "name": "Search",
            "thumbnail": "/static/images/search.png",
            "disabled":false,
            "slick":true
        })
    }
    $scope.nvalidelements = $scope.data.length;
    $scope.currentelement= $scope.data[0]
    for (i=0;i<$scope.maxelements-$scope.nvalidelements+1;i++) {
        $scope.data.push({
            "name": "",
            "thumbnail": "/static/images/disabledlibrary.png",
            "disabled":true,
            "slick":true
        })
    }
    $("."+$scope.classname).on('beforeChange',
                    function (event, slick, currentSlide, nextSlide) {
                        if (nextSlide > $scope.nvalidelements-1)
                            slick.disabled= true;
                            else 
                        slick.disabled = false
                    });
    $("."+$scope.classname).on('afterChange',
                    function (event, slick, currentSlide) {
                        if (slick.disabled) {
                            return
                    }
                    $scope.currentelement = $scope.data[currentSlide]
                    $scope.$apply();
    });
    
    $scope.keydownhandler = function(e) {
        if ($scope.onkeypress() != undefined) {
            var result = $scope.onkeypress()(e);
            if (result == -1) // a return value of -1 stops propagation
                return; 
        }
        
        var current = $("."+$scope.classname).slick('slickCurrentSlide');                    
        if (e.keyCode == '38' || e.keyCode == '37') { // up & left
            if (current == 0)
                $("."+$scope.classname).slick('slickGoTo',$scope.nvalidelements-1);
            else if (current > ($scope.nvalidelements-1))
                $("."+$scope.classname).slick('slickGoTo',0);
            else
                $("."+$scope.classname).slick('slickPrev');
        } 
        else if (e.keyCode == '40' || e.keyCode == '39') { //down & right
            if (current >= ($scope.nvalidelements-1))
                $("."+$scope.classname).slick('slickGoTo',0);
            else
                $("."+$scope.classname).slick('slickNext');
        } 
    }
    
    $("."+$scope.classname).keydown(function (e) {
        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();
        
        if (e.keyCode == '13') { //enter
            $scope.onenter()(e);
            return;
        }
        $scope.keydownhandler(e)
    });

    $("."+$scope.classname).click( function(e){ 
        $scope.onbuttonclick();
    });
    
    $scope.$watch('data', function(newValue, oldValue) {
        var parentid = $scope.currentelement.parent
        if ($scope.isinitialized) {
            var nslides = $("."+$scope.classname).slick('getSlick').slideCount;
            for (i=0;i<nslides;i++) {
                $("."+$scope.classname).slick("slickRemove",0,"removeAll")
            }
          //  $(".slider").slick("unslick")
        if ($scope.addbutton == "true") {
            $scope.data.push({
                "name": "Add",
                "thumbnail": "/static/images/plusicon.png",
                "disabled":false,
                "slick" :true,
            })
        }
        if ($scope.searchbutton == "true") {
            $scope.data.push({
                "name": "Search",
                "thumbnail": "/static/images/search.png",
                "disabled":false,
                "slick":true
            })
        }
        $scope.nvalidelements = $scope.data.length;
        $scope.currentelement= $scope.data[0]
        for (i=0;i<$scope.maxelements-$scope.nvalidelements+1;i++) {
            $scope.data.push({
                "name": "",
                "thumbnail": "/static/images/disabledlibrary.png",
                "disabled":true,
                "slick":true
            })
        }
        for (i=0;i<$scope.data.length;i++) {
        var thumbnail = $scope.data[i].thumbnail ?  $scope.data[i].thumbnail : "/static/images/disabledlibrary.png"

            $("."+$scope.classname).slick("slickAdd",'\
            <div><h3 ng-show="true">\
                <div class ="slickbutton"\
                     style=\'padding:5px; margin:0px\'>\
                    <img src="'+thumbnail+'\" \
                         width='+$scope.imgwidth+' \
                         style="float:left;\
                                 padding-top:5px;\
                                 margin:0px;\
                                 object-fit:contain;\
                                 max-height:100%">\
                    <div ng-show="'+$scope.showlabel+'" \
                         style="height:10px"></div>\
                    <span ng-show="'+$scope.showlabel+'"> \
                        '+$scope.data[i].name +' \
                    </span> \
                </div>\
            </h3></div>\
        ')
        }
        // set the current slide to the parent of the previous current
        for (i=0; i<$scope.data.length;i++) {
            if ($scope.data[i].id == parentid) {
                $('.slider').slick("slickGoTo",i)
            }
        }            
 //       var slide = $("."+$scope.classname).slick('getSlick')
//        console.log($("."+$scope.classname).find('.slick-list')[0])
//        $("."+$scope.classname).find('.slick-list')[0].focus()
        }
        else {
            $scope.isinitialized=true; 
        }
        //$('.slider').slick()
    });
});
