    acceptedTypes = {
      'image/png': true,
      'image/jpeg': true,
      'image/gif': true 
    };

    var thumbnaildrop = document.getElementById("thumbnaildrop"+"{{thumbnailindex}}")
    console.log(thumbnaildrop)
    if (thumbnaildrop) {
      thumbnaildrop.addEventListener("dragover", function(e){ThumbFileDragHover(e,"{{thumbnailindex}}")}, false);
      thumbnaildrop.addEventListener("dragleave", function(e){ThumbFileDragHover(e,"{{thumbnailindex}}")}, false);
      thumbnaildrop.addEventListener("drop", function(e){ThumbFileSelectHandler(e,"{{thumbnailindex}}")}, false);
      thumbnaildrop.style.display = "block";
    }

// file drag hover
function ThumbFileDragHover(e,index) {
    e.stopPropagation();
    e.preventDefault();
    var thumbnaildrop = document.getElementById("thumbnaildrop"+index)
    console.log(index)
    thumbnaildrop.className = (e.type == "dragover" ? "hover" : "");
}


// file selection
function ThumbFileSelectHandler(e,index) {
    // cancel event and hover styling
  //  FileDragHover(e);
    e.stopPropagation();
    e.preventDefault();
    var thumbnaildrop = document.getElementById("thumbnaildrop"+index)

    thumbnaildrop.className = "";

    // fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // process all File objects
    thumbprocessFile(files[0],index);
}

function previewfile(file,index) {

  if (acceptedTypes[file.type] === true) {

      var reader = new FileReader();
      reader.onload = function (event) {
        var image = new Image();
        image.src = event.target.result;
        image.width = 250; 
        var thumbnailimg  = document.getElementById("thumbnailimg"+index)
        thumbnailimg.src = image.src; // = 'url(' + event.target.result + ') no-repeat center'

      };

      reader.readAsDataURL(file);
  }  else {
//    posterholder.innerHTML += '<p>Uploaded ' + file.name + ' ' + (file.size ? (file.size/1024|0) + 'K' : '');
  }
}

function thumbprocessFile(file,index) {

    previewfile(file,index);
    postthumbnail(file,index);
}
    
function postthumbnail(file,index) {
    var fd = new FormData();
    fd.append("thumbnail",file);

    // now post a new XHR request 
    var id = $('#thumbnaildata'+index).data()["id"];
    var posturl = $("#thumbnaildata"+index).data()["posturl"]
    var xhr = new XMLHttpRequest();
    xhr.open('POST', posturl);
    xhr.send(fd);
}