'use strict';

angular.module("filebrowser", ["ngwidgets"])
    
.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol("[[");
    $interpolateProvider.endSymbol("]]");
})

.directive('filebrowser', function () {
    return {
        restrict: 'AEC', 
        replace: false,
        templateUrl: '/static/templates/filebrowser.html',
        scope:  {path: '@path',
                 selectcb: '&'},
        controller:"filebrowserController",
        link: function(scope, element, attrs) {
           // scope.selectcb({path: scope.path});
        },
    };
})

.controller("filebrowserController", function ($scope) {
    $scope.history = [];
    $scope.historyindex = -1;
    $scope.favorites = [];
    $scope.favoritesdictionary = {};

    $scope.dragEndLog = $scope.dragStartLog = "";

    // Create a ngxListBox
    $scope.listBoxASettings = {
        allowDrop: true,
        allowDrag: true,
        source: $scope.favorites,
        width: '100%',
        height: '100%',
        dragStart: function (item) {
            if (item.label == "Breve")
                return false;
        },
        renderer: function (index, label, value) {
            var iconsize = 10;
            var textsize = 12;
            var iconcolor = "#999999";
            var textcolor = "black";

            var html = "<i style=\"font-size:" + iconsize + 
                        "px; color: " + iconcolor + 
                        "\" class=\"glyphicon glyphicon-folder-close\"> " +
                        "<span style=\"font-size:" + textsize + 
                        "px; color: " + textcolor + "\">" + 
                        label + "<\span>" + "</i>";
            return html;
        },
    //    created: function (args) {
    //        $scope.listboxa = args.instance;
    //    },
        
    };
    
    var ListBoxA = new ngxListBox("#ListBoxA", $scope.listBoxASettings);

    var data = []

    // prepare the data
    $scope.source =
        {
        dataType: "json",
        dataFields: [
            { name: 'filename' },
        ],
        localData: data
    };
    $scope.dataAdapter = new ngWidgets.ngx.dataAdapter($scope.source);
    $scope.dataAdapter.dataBind()

    $scope.listBoxBSettings = {
        allowDrop: false,
        allowDrag: false,
        source: $scope.dataAdapter,
        width: '100%',
        height: '100%',
        renderer: function (index, label, value) {            
            var iconsize = 10;
            var textsize = 12;
            var iconcolor = "#4488ff";
            var textcolor = "black";

            var html = "<i style=\"font-size:" + iconsize + "px; color: " + iconcolor + "\" class=\"glyphicon glyphicon-folder-close\"> " +
                "<span style=\"font-size:" + textsize + "px; color: " + textcolor + "\">" + $scope.dataAdapter.records[index].filename + "<\span>" +
                "</i>";
            return html;
        },
    };
    $scope.ListBoxB = new ngxListBox("#ListBoxB", $scope.listBoxBSettings);

    $scope.onSelectFavorite = function (event) {
        console.log("selectfavorite")
        var targetpath = $scope.favoritesdictionary[event.args.item];
        $scope.postquery(targetpath)
    }

    $scope.ListBoxB.on("select",function (event) {
        if ((event.args) && (event.args.type != "none")) {
            var targetpath = $scope.path + "/" + event.args.item.originalItem.filename;
            $scope.postquery(targetpath)
            console.log("select done")
        }
    });

    $scope.dragStart = function (event) {
        $scope.dragStartLog = "Drag Start: " + event.args.label;
        $scope.dragEndLog = "";
    };

    $scope.dragEnd = function (event) {
        $scope.dragEndLog = "Drag End";
        if (event.args.label) {
            var ev = event.args.originalEvent;
            var x = ev.pageX;
            var y = ev.pageY;
            if (event.args.originalEvent && event.args.originalEvent.originalEvent && event.args.originalEvent.originalEvent.touches) {
                var touch = event.args.originalEvent.originalEvent.changedTouches[0];
                x = touch.pageX;
                y = touch.pageY;
            }

            var offset = $("#textarea").offset();
            var width = $("#textarea").width();
            var height = $("#textarea").height();
            var right = parseInt(offset.left) + width;
            var bottom = parseInt(offset.top) + height;

            if (x >= parseInt(offset.left) && x <= right) {
                if (y >= parseInt(offset.top) && y <= bottom) {
                    $scope.textAreaValue = event.args.label;
                }
            }
        }
    };

    $scope.postquery = function (targetpath) {
        var d = {
            "path": targetpath,
            "filters": ["dironly", "hidden"]
        };
        $.ajax({
            type: "POST",
            url: "/api/filebrowser",
            data: JSON.stringify(d),
            dataType: "json",
            contentType: 'application/json; charset=UTF-8', // This is the money shot
            traditional: true,

            success: function (data) {
                $scope.setdata(data)
                $scope.$apply;

                $scope.history.push({
                    "path": data["path"],
                    "directorylisting": data["directorylisting"]
                })
                $scope.historyindex = $scope.history.length - 1
                if ($scope.history.length > 1)
                    document.getElementById("leftarrow").disabled = false;
                document.getElementById("rightarrow").disabled = true;

            },
            error: function (message) {
                $scope.message = "Can't Access " + targetpath
                $(".message").removeClass("hidden")
            }
        });
    }
    
    $scope.setdata = function (data) {
        var newdata = []
        for (var i in data.directorylisting) {
            newdata.push({"filename":data.directorylisting[i]})   
        };

        $scope.source.localdata = newdata
        $scope.dataAdapter.dataBind()

        $scope.files = data["directorylisting"]
        $scope.path = data["path"];
        $scope.message = ""
    }

    $scope.upclickHandler = function () {
        var targetpath = $scope.path + "/.."
        $scope.postquery(targetpath);

    }

    $scope.homeHandler = function () {
        $scope.postquery($scope.homepath);
    }

    $scope.leftclickHandler = function () {
        if ($scope.historyindex - 1 >= 0) {
            $scope.historyindex--;
            var data = $scope.history[$scope.historyindex];
            $scope.setdata(data)
            $scope.$apply;
            document.getElementById("rightarrow").disabled = false;

            if ($scope.historyindex == 0)
                document.getElementById("leftarrow").disabled = true;

        } else {
            document.getElementById("leftarrow").disabled = true;
        }
    }

    $scope.rightclickHandler = function () {
        if ($scope.historyindex + 1 < $scope.history.length) {
            $scope.setdata($scope.history[$scope.historyindex + 1])
            $scope.$apply;
            
            $scope.historyindex++;
            document.getElementById("leftarrow").disabled = false;

            if ($scope.historyindex == ($scope.history.length - 1))
                document.getElementById("rightarrow").disabled = true;

        } else {
            document.getElementById("rightarrow").disabled = true;
        }
    }

    $scope.finderHandler = function () {
        var req = new XMLHttpRequest();
        var formdata = new FormData();

        formdata.append("path", $scope.path)
        req.open('POST', 'http://127.0.0.1:5601/api/filebrowser/openfinder');
        req.send(formdata);
        return false;
    };

    $scope.refreshHandler = function () {
        var targetpath = $scope.path
        $scope.postquery(targetpath);
    };

    $scope.addfavoriteClick = function () {
        document.getElementById("favoritepath").value = $scope.path;
        document.getElementById("favoritename").value = $scope.basename($scope.path);
    }

    $scope.addfavoriteHandler = function () {
        var name = document.getElementById("favoritename").value
        var path = document.getElementById("favoritepath").value
        if (name in $scope.favoritesdictionary) {
            alert(name + " already exists")
        } else {

            $scope.favorites.push(name)
            $scope.favoritesdictionary[name] = path
            $scope.$apply;
            
            console.log(name + " " + path)

            $("#addfavoriteModal").modal("hide")

            favoritesurl = "/api/filebrowser/setfavorites/"+id
            // submit to database
            if ($("#elementdata") && $("#elementdata") != "undefined") {
                var id = $("#elementdata").data("id")
                var data = {
                    "filebrowserfavorites": favoritesdictionary
                }
                $.ajax({
                    type: "POST",
                    url: favoritesurl,
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {},
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        }
    }

    $scope.removefavoriteHandler = function () {
        var items = $scope.listboxa.getSelectedItems();
        $scope.favorites.splice(items[0].index, 1)
        delete $scope.favoritesdictionary[items[0].label]
    };

    $scope.editfavoriteHandler = function () {

    };
    
    $scope.plusfolderHandler = function () {
        var targetpath = $scope.path;
        var directory = document.getElementById("newfolder").value
        $scope.postcommand(targetpath, "mkdir", directory)
    };
    
    $scope.basename = function (path) {
        return path.split('/').reverse()[0];
    };
    
    $scope.selectfilehandler = function () {
        console.log("selectfilehandler()")
        $scope.selectcb({path: $scope.path});
    }
    
    // init query
    $scope.homepath = $scope.path;
    $scope.postquery($scope.path)

})
