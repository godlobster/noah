


// getElementById
function $id(id) {
	return document.getElementById(id);
}

//
// output information
function Output(msg) {
//	var m = $id("messages");
//	m.innerHTML = msg + m.innerHTML;
}

// call initialization file
if (window.File && window.FileList && window.FileReader) {
	Init();
}

//
// initialize
function Init() {

	var fileselect = $id("fileselect"),
		filedrag = $id("filedrag"),
		submitbutton = $id("submitbutton");

	// file select
	if (fileselect)
		fileselect.addEventListener("change", FileSelectHandler, false);

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if (xhr.upload) {
	
		// file drop
		if (filedrag) {
			filedrag.addEventListener("dragover", FileDragHover, false);
			filedrag.addEventListener("dragleave", FileDragHover, false);
			filedrag.addEventListener("drop", FileSelectHandler, false);
			filedrag.style.display = "block";
			}
		}
}

function remove(arr, item) {
      for(var i = arr.length; i--;) {
          if(arr[i] === item) {
              arr.splice(i, 1);
          }
      }
  }
// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	var current = filedrag.className.split(" ")
	remove(current,"hover")
	classes = current.join(" ")
	filedrag.className = (e.type == "dragover" ? "hover "+classes : classes);
}


// file selection
function FileSelectHandler(e) {

	// cancel event and hover styling
	FileDragHover(e);

	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;

	// process all File objects
		ProcessFile(files[0]);
}

function ProcessFile(file) {
	// define this function in your html source file

	processfilecb(file);
}