angular.module("slickselect", [])
    
.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol("[[");
    $interpolateProvider.endSymbol("]]");
})

.directive('slickselect', ["$timeout", function () {
    return {
        restrict: 'AEC', 
        replace: false,
        transclude:false,
        template: "<div class=\"slider\">\
                        <div ng-repeat='element in elements'>\
                        <div>\
                        <h3 ng-show=\"true\">\
                            <div class =\"slickbutton\" style='padding:5px; margin:0px'>\
                                <img src=\"[[ element.thumbnail ||\
                                          '/static/images/Library.png']]\" \
                                     width=[[imgwidth]] \
                                     style=\"float:left;padding-top:5px;margin:0px;object-fit:contain;max-height:100%\">\
                                <div ng-show=\"[[showlabel]]\" style=\"height:10px\"></div>\
                                <span ng-show=\"[[showlabel]]\"> [[element.name]] </span> \
                            </div>\
                        </h3>\
                    </div>\
                    </div>\
                </div>",
        scope:  {elements: '=',
                 maxelements:'@',
                 currentelement:'=',
                 addbutton:'@',
                 searchbutton:'@',
                 vertical:'@',
                 showlabel:'@',
                 imgwidth:'@',
                 onenter:'&',
                 onbuttonclick:'&',
                },
        controller:"slickselectController",
        link: function(scope,elem,attrs) {
            scope.onenter();
            scope.onbuttonclick();

        },
        compile: function(elem, attrs, transclude) {
            return function link(scope,elem,attrs,ctr) {

                $(document).ready(function () {
                    $(".slider").slick({
                        centerMode: true,
                        infinite: true,
                        centerPadding: '0px',
                        slidesToShow: scope.maxelements,
                        speed: 500,
                        vertical: scope.vertical == "true" ? true : false,
                        draggable: true,
                        focusOnSelect: true,
                        arrows: false,
                    })
                });
            }
        }
    }
}])

.controller("slickselectController", function ($scope) {
    if (!$scope.elements)
        $scope.elements = []
    if ($scope.addbutton == "true") {
        $scope.elements.push({
            "name": "Add",
            "thumbnail": "/static/images/plusicon.png",
            "disabled":false,
            "slick" :true,
        })
    }
    if ($scope.searchbutton == "true") {
        $scope.elements.push({
            "name": "Search",
            "thumbnail": "/static/images/search.png",
            "disabled":false,
            "slick":true
        })
    }
    $scope.nvalidelements = $scope.elements.length;
    $scope.currentelement= $scope.elements[0]
    for (i=0;i<$scope.maxelements-$scope.nvalidelements+1;i++) {
        $scope.elements.push({
            "name": "",
            "thumbnail": "/static/images/disabledlibrary.png",
            "disabled":true,
            "slick":true
        })
    }
    $('.slider').on('beforeChange',
                    function (event, slick, currentSlide, nextSlide) {
                        if (nextSlide > $scope.nvalidelements-1)
                            slick.disabled= true;
                            else 
                        slick.disabled = false
                    });
    $('.slider').on('afterChange',
                    function (event, slick, currentSlide) {
                        if (slick.disabled) {
                            return
                    }
                    $scope.currentelement = $scope.elements[currentSlide]
                    $scope.$apply();
    });
    $(".slider").keydown(function (e) {
        console.log("keydown")
        e = e || window.event;
        e.preventDefault()
        e.stopPropagation()
        var current = $('.slider').slick('slickCurrentSlide');                    
        if (e.keyCode == '38' || e.keyCode == '37') { // up
            if (current == 0)
                $('.slider').slick('slickGoTo',$scope.nvalidelements-1);
            else if (current > ($scope.nvalidelements-1))
                $('.slider').slick('slickGoTo',0);
            else
                $('.slider').slick('slickPrev');
        } else if (e.keyCode == '40' || e.keyCode == '39') { //down
            if (current >= ($scope.nvalidelements-1))
                $('.slider').slick('slickGoTo',0);
            else
                $('.slider').slick('slickNext');
        } else if (e.keyCode == '13') { //down
            $scope.onenter();
        }
    });
    $(".slider").click( function(e){ 
        console.log("clickbuttonclick");
        $scope.onbuttonclick();
    });
    
});
