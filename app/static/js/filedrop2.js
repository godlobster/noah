var posterholder = document.getElementById('posterholder'),
    thumbnailholder = document.getElementById('thumbnailholder'),
    poster=document.getElementById('poster'),
    thumbnail=document.getElementById('thumbnail'),
    thumbnails = document.getElementsByClassName("thumbnailarrayholder"),
    arkentriesids = document.getElementsByClassName("arkentries"),
    arkentryid = document.getElementById("arkentryid"),

    acceptedTypes = {
        'image/png': true,
        'image/jpeg': true,
        'image/gif': true 
    };

// call initialization file
if (window.File && window.FileList && window.FileReader) {
	Init();
}

//
// initialize
function Init() {

    // file drop
    if (posterholder) {
        posterholder.addEventListener("dragover", function(e){FileDragHover(e,posterholder,-1)}, false);
        posterholder.addEventListener("dragleave", function(e){FileDragHover(e,posterholder,-1)}, false);
        posterholder.addEventListener("drop", function(e){FileSelectHandler(e,posterholder,-1)}, false);
        posterholder.style.display = "block";
    }
    
    if (thumbnailholder) {
        thumbnailholder.addEventListener("dragover", function(e){FileDragHover(e,thumbnailholder,-1)}, false);
        thumbnailholder.addEventListener("dragleave", function(e){FileDragHover(e,thumbnailholder,-1)}, false);
        thumbnailholder.addEventListener("drop", function(e){FileSelectHandler(e,thumbnailholder,-1)}, false);
        thumbnailholder.style.display = "block";
    }
    
    if (thumbnails) {
        for (i = 0; i < thumbnails.length; i++) {
           thumbnails[i].addEventListener("dragover", function(e){FileDragHover(e,thumbnails,i-1)}, false);
           thumbnails[i].addEventListener("dragleave", function(e){FileDragHover(e,thumbnails,i-1)}, false);
           thumbnails[i].addEventListener("drop", function(e){FileSelectHandler(e,thumbnails,i-1)}, false);
           thumbnails[i].style.display = "block";

       }
    }   
}


function FileDragHover(e, holder, index) {
    e.preventDefault();
    if (index != -1)
        e.target.className = (e.type == "dragover" ? "thumbnailarrayholder hover" : "thumbnailarrayholder");
    else
	   e.target.className = (e.type == "dragover" ? "hover" : "");

    if (index == -1)
        holder.className  = (e.type == "dragover" ? "hover" : "");
    else if (holder[index]) {
        holder[index].className  = (e.type == "dragover" ? "thumbnailarrayholder hover" : "thumbnailarrayholder");
    }
        
}

// file selection
function FileSelectHandler(e,theholder,index) {
    e.preventDefault();

	// cancel event and hover styling
	FileDragHover(e,theholder,index);

	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;

	// process all File objects
    readfiles(files,theholder,index);

}

function previewfile(file,theholder,index) {
    if (acceptedTypes[file.type] === true) {
        var reader = new FileReader();
        reader.onload = function (event) {

            var image = new Image();
            image.src = event.target.result;
            image.width = 250; 
            if (theholder.id == "posterholder")
                poster.src = image.src; // = 'url(' + event.target.result + ') no-repeat center'
		    else if (theholder.id == "thumbnailholder") {
                thumbnail.src = image.src; // = 'url(' + event.target.result + ') no-repeat center'

            }
		    else {
                if (thumbnails[index]) {
				    document.getElementById("thumbnail."+index).src = image.src; // = 'url(' + event.target.result + ') no-repeat center'
                }   
            }
      };
      reader.readAsDataURL(file);
  }  else {
//    posterholder.innerHTML += '<p>Uploaded ' + file.name + ' ' + (file.size ? (file.size/1024|0) + 'K' : '');
  }
}

function readfiles(files, theholder,index) {
    var formData = new FormData();
    for (var i = 0; i < files.length; i++) {
        formData.append(theholder.id, files[i]);
		previewfile(files[i],theholder,index);
    }   

    // now post a new XHR request 
    var xhr = new XMLHttpRequest();
    if (arkentryid) {
        xhr.open('POST', '/api/arkentry/updateprefs/2'.replace('2',arkentryid.content));
        xhr.send(formData);
    }
    else if (arkentriesids.length && index != -1) {
        xhr.open('POST', '/api/arkentry/updatethumbnail/2'.replace('2',arkentriesids[index].content) +
                 "/"+index);
        xhr.send(formData);

    }
    else {
    }
    xhr.onload = function() {
//			if (progress)
//				progress.value = progress.innerHTML = 100;
      };
}