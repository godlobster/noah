var posterholder = document.getElementById('posterholder'),
	thumbnailholder = document.getElementById('thumbnailholder'),
    tests = {
      filereader: typeof FileReader != 'undefined',
      dnd: 'draggable' in document.createElement('span'),
      formdata: !!window.FormData,
      progress: "upload" in new XMLHttpRequest 
    }, 
    support = {
      filereader: document.getElementById('filereader'),
      formdata: document.getElementById('formdata'),
      progress: document.getElementById('progress') 
    },
    acceptedTypes = {
      'image/png': true,
      'image/jpeg': true,
      'image/gif': true 
    },
    progress = document.getElementById('uploadprogress'),
    fileupload = document.getElementById('upload'),
	poster=document.getElementById('poster'),
	thumbnail=document.getElementById('thumbnail'),
	thumbnails = document.getElementsByClassName("thumbnailarrayholder"),
	arkentriesids = document.getElementsByClassName("arkentries");

var arkentryid = document.getElementById("arkentryid");

"filereader formdata progress".split(' ').forEach(function (api) {
  if (tests[api] === false) {
    support[api].className = 'fail';
  } else {
    // FFS. I could have done el.hidden = true, but IE doesn't support 
    // hidden, so I tried to create a polyfill that would extend the 
    // Element.prototype, but then IE10 doesn't even give me access 
    // to the Element object. Brilliant. 
//    support[api].className = 'hidden';
  }
});

function previewfile(file,theholder,index) {
  if (tests.filereader === true && acceptedTypes[file.type] === true) {
      var reader = new FileReader();
      reader.onload = function (event) {
      var image = new Image();
      image.src = event.target.result;
      image.width = 250; 

	  console.log(theholder);
		  if (theholder == "posterholder")
			  poster.src = image.src; // = 'url(' + event.target.result + ') no-repeat center'
		  else if (theholder == "thumbnailholder")
		  thumbnail.src = image.src; // = 'url(' + event.target.result + ') no-repeat center'
		  else {
			  console.log(thumbnails[index]);
			  console.log(thumbnails);			  			  
			  if (thumbnails[index]) {
				  console.log("HERE"+index);			  
				  thumbnails[index].src = image.src; // = 'url(' + event.target.result + ') no-repeat center'
			  }
		  }
      };

      reader.readAsDataURL(file);
  }  else {
//    posterholder.innerHTML += '<p>Uploaded ' + file.name + ' ' + (file.size ? (file.size/1024|0) + 'K' : '');
    console.log(file);
  }
}
 
function readfiles(files, theholder,index) {
    var formData = tests.formdata ? new FormData() : null;
    for (var i = 0; i < files.length; i++) {
      if (tests.formdata) formData.append(theholder, files[i]);
		previewfile(files[i],theholder,index);
    }   

    // now post a new XHR request 
    if (tests.formdata) {
		var xhr = new XMLHttpRequest();
		if (arkentryid)
			xhr.open('POST', '/api/arkentry/updateprefs/2'.replace('2',arkentryid.content));
		else {
			console.log("here");			
			xhr.open('POST', '/api/arkentry/updatethumbnail/2'.replace('2',arkentriesids[i].content) +
					"/"+index);
		}
		xhr.onload = function() {
//			if (progress)
//				progress.value = progress.innerHTML = 100;
      };
    };
      xhr.send(formData);
}

if (tests.dnd) {
    if (posterholder) {
        posterholder.ondragover = function () { console.log("hover");this.className = 'hover'; return false; };
        posterholder.ondragend = function () { console.log("dragend");this.className = ''; return false; };
        posterholder.ondragleave = function () { console.log("dragleave");this.className = ''; return false; };

        posterholder.ondrop = function (e) {
    this.className = '';
    e.preventDefault();
      readfiles(e.dataTransfer.files,"posterholder",0);
  }
	}

	if (thumbnailholder) {
		thumbnailholder.ondragover = function () { this.className = 'hover';return false; };
		thumbnailholder.ondragend = function () { this.className = ''; console.log("dragend"); return false; };
		thumbnailholder.ondrop = function (e) {
			this.className = '';
			e.preventDefault();
			readfiles(e.dataTransfer.files,"thumbnailholder",0);
		}
	}	
	console.log(thumbnails);
	for (i = 0; i < thumbnails.length; i++) {
		thumbnails[i].ondragover = function () { console.log(this.id);this.className = 'hover'; return false; };
		thumbnails[i].ondragend = function () { this.className = '';console.log("dragend"); return false; };
		thumbnails[i].ondrop = function (e) {
			console.log("ondrop: "+this.id)
			this.className = '';
			e.preventDefault();
			readfiles(e.dataTransfer.files,"thumbnail."+i,i);
	}
}
	
//	thumbnailarrayholder.ondragover = function () { this.className = 'hover'; return false; };
//	thumbnailarrayholder.ondragend = function () { this.className = ''; return false; };
//	thumbnailarrayholder.ondrop = function (e) {
//    this.className = '';
//    e.preventDefault();
//		readfiles(e.dataTransfer.files,"thumbnailholder");
//  }	

} else {
  fileupload.className = 'hidden';
  fileupload.querySelector('input').onchange = function () {
      readfiles(this.files,"",0);
  };
}
