'use strict';

angular.module("thumbnail", [])
    
.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol("[[");
    $interpolateProvider.endSymbol("]]");
})

.controller("thumbnailController", function ($scope) {
    $scope.acceptedTypes = {
      'image/png': true,
      'image/jpeg': true,
      'image/gif': true 
    };
    
    $scope.processfile = function(target, file) {
        console.log("angular-thumbnail processing thumbnail")

        // -- first preview file
        if ($scope.acceptedTypes[file.type] === true) {
            var reader = new FileReader();
            reader.onload = function (event) {
                var image = new Image();
                image.src = event.target.result;
                var thumbnailimg  = target
                thumbnailimg.src = image.src; // = 'url(' + event.target.result + ') no-repeat center'
                };
            
            reader.readAsDataURL(file);
        }
        
        // -- post file
        if (this.posturl) {
            var posturl = this.posturl.replace("999",$scope.elementid);
            console.log("angular-thumbnail posting thumbnail..."+posturl)

            var fd = new FormData();
            fd.append("thumbnail",file);

            // now post a new XHR request 
            var xhr = new XMLHttpRequest();
            xhr.open('POST', posturl);
            xhr.send(fd);
            
            return true;
        }
        return false;
    } //-- end processfile
    $scope.getthumbnail = function () {
        if ($scope.elementsrc)
            return $scope.elementsrc;
        else
            return $scope.defaultsrc;
    }
}) //-- end controller


.directive('thumbnail', function () {
    return {
        restrict: 'AEC', 
        replace: false,
        templateUrl: "/static/templates/angularthumbnail.html",
      //  template: "hi",

        scope:  {
                 width: '@width',
                 height: '@height',
                 maxheight: '@maxheight',
                 posturl: '@posturl',
                 defaultsrc: '@',
                 elementsrc:'=',
                 elementid:'=',
                 onpost: '&'}, //-- default cb posts file to this url
        
        controller:"thumbnailController",
        link: function(scope, element, attrs) {
            scope.onpost({data:scope.elementsrc ? scope.elementsrc : scope.defaultsrc});

            element.bind("dragover dragleave", function(e) {
                e.stopPropagation();
                e.preventDefault();
                               
               if (e.target.nodeType == 1) {

                   var holder = (e.target.tagName.toLowerCase() == "img" ? 
                                 e.target.parentElement : 
                                 e.target)
                   var classes = holder.className.replace("hover","")
                   holder.className = (e.type == "dragover" ?  
                                       "hover "+classes : 
                                       classes); 
               }
            })
            
            element.bind("drop", function (e) {
                console.log("angular-thumbnail dropevent")
                e.stopPropagation();
                e.preventDefault();
                
                var holder = (e.target.tagName.toLowerCase() == "img" ? 
                              e.target.parentElement : 
                              e.target)
                var classes = holder.className.replace("hover","")
                holder.className = (e.type == "dragover" ?  
                                    "hover "+classes : 
                                    classes); 

            //    if (scope.ondrop) {
            //        return scope.ondrop(e);    
            //    }
                
                if (e.target.nodeType == 1) {
                    var thumbnailimg = e.target
                    if (e.target.tagName.toLowerCase() == "div")
                        thumbnailimg = e.target.firstElementChild

                    var file = event.dataTransfer.files[0];
                
                    if (scope.processfile(thumbnailimg, file)) {
                        console.log("file processed.  posting..")
                        scope.onpost();
                    }
               }
            })
        },
    } // - end return
}); // end directive


function remove(arr, item) {
      for(var i = arr.length; i--;) {
          if(arr[i] === item) {
              arr.splice(i, 1);
          }
      }
  }
