var myApp = angular.module('myApp', []);
function Main($scope, $http){
  console.log("here");
  $http.get('http://api.randomuser.me/?results=20').success(function(data) {
    $scope.users = data.results;
    $('#loader').hide();
    $('#userList').show();
  }).error(function(data, status) {
    alert('get data error!');
  });
  
  $scope.showUserModal = function(idx){
    var user = $scope.users[idx].user;
    $scope.currUser = user;
    $('#myModalLabel').text(user.name.first
         + ' ' + user.name.last);
    $('#myModal').modal('show');
  }
  
  // default view is list
  $scope.mode = 1;
 
}

//$(document).ready(function() {});