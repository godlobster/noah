from flask import render_template, flash, redirect, url_for, request, jsonify, make_response
from flask.ext.cors import CORS, cross_origin 

from sqlalchemy import desc, and_

import datetime
import os
import json
import subprocess
import xml.etree.ElementTree
import types

from appd import app
from flask.ext.cors import cross_origin

def _json_builder(x):
	ret = {}
	for c in x.keys():
		ret[c] = x[c]
	return ret


@app.route('/api/filebrowser',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def filebrowser():
    data= request.get_json()
    print data
    path = (data and data.has_key("path") and data["path"]) or "/"
    filters = (data and data.has_key("filters") and data["filters"]) or []
    command = (data and data.has_key("cmd")) and data["cmd"] or None
    args = (data and data.has_key("args")) and data["args"] or ""
    
    files = []
    if os.path.exists(path):
        path=os.path.realpath(path)
        if command:
            if command == "mkdir":
                d = os.path.join(path,args)
                if not os.path.exists(d):    
                    try:
                        os.mkdir(d)
                        return json.dumps({"success":True},
                                           200, 
                                          {'ContentType':'application/json'})

                    except:
                        return json.dumps({"success":False,
                                           "message": "Unable to make directory: %s"%args}, 
                                            400, {'ContentType':'application/json'})
            print "no command",command

            return json.dumps({"success":False,
                               "message": "Command not supported: %s"%command}, 
                               400, {'ContentType':'application/json'})
        else:
            try:
                files = os.listdir(path)
            except OSError:
                print "invalid path",path
                return json.dumps({"success":False,
                                   "message":"Invalid Path: %s"%path}, 400,
                                  {'ContentType':'application/json'})

            if "dironly" in filters:
                os.chdir(path)
                files = filter(os.path.isdir,files)
            if "hidden" in filters:
                files = filter(lambda x:not x.startswith("."),files)
            resultdata =  jsonify(data=[{"directorylisting":files,
                                         "path":path}])
            return resultdata
    else:
        print "invalid path2",path

        return json.dumps({"success":False,
                           "message": "Path not found: %s"%path}, 400,     
                          {'ContentType':'application/json'})

@app.route('/api/filebrowser/openfinder',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def filebrowser_openfinder():
    data= request.form
    print data
    if data.has_key("path") and os.path.exists(data["path"]):
        prefix = "open "
        cmd = prefix+ " " +data["path"] + " &"
        print "OpenFinder Command: "+cmd 
        os.system(cmd)
        return json.dumps({"success":True},
                          200, 
                          {'ContentType':'application/json'})

    else:    
        return json.dumps({"success":False,
                           "message": "Path not found: %s"%path}, 410, {'ContentType':'application/json'})
   
