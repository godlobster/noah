from flask import render_template, flash, redirect, url_for, request, jsonify, send_from_directory,send_file
from flask.ext.cors import cross_origin

from sqlalchemy import desc, and_,or_

import datetime
import os
import json
import subprocess
import xml.etree.ElementTree
import types
import string
import sqlite3

from appd import app, db
import urllib2

import arkdb
import arkconfig
import webconfig

@app.route('/api/launchapplication/<int:id>/<int:appid>/<int:arkentryappid>',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def launchapplication(id,appid,arkentryappid):
    # --- first lets fork and send a response
#    pid=os.fork()
#    if not pid==0:
#        return
#       # return json.dumps({"success":True},
    #                      200, 
    #                      {'ContentType':'application/json'})
    
    print "launchapplication"
    SEND_URL = "%s/api/application/%d"%(webconfig.SERVER_URL,appid)
    print SEND_URL
    request = urllib2.Request(SEND_URL)
    request.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(request)
    message   = response.read()
    appdata = None
    if message:
        appdata = json.loads(message)
    response.close()

    SEND_URL = "%s/api/entry/%d"%(webconfig.SERVER_URL,appid)
    request = urllib2.Request(SEND_URL)

    request.add_header('Content-Type', 'application/json')
    
    response = urllib2.urlopen(request)

    message   = response.read()
    entrydata = None
    if message:
        entrydata = json.loads(message)
    response.close()
        
    if len(appdata["data"] and entrydata["data"]):
        appdata = appdata["data"][0]
        path = appdata["path"]
        name = appdata["name"]
        entrydata = entrydata["data"][0]
        
        prefix = ""
        args = (appdata.has_key("args") and appdata["args"]) or ""
        if name == "finder":
            prefix = "open -a"
            args = entrydata["fullpath"]
            
        cmd = prefix + " " + path + " " + args +"&"
        print "Launch Application: ",cmd
        os.system(cmd)

 #   return json.dumps({"success":True},
#                      200, 
#                      {'ContentType':'application/json'})

@app.route('/api/getclipboard',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def getclipboard():
    print "getclipboard"
    clientip = request.environ['REMOTE_ADDR']
    print clientip
    data = request.args
    
    if data.has_key("clipboard"):
        import pyperclip
        clipboard = pyperclip.copy(data["clipboard"])
    
    return json.dumps({"success":True,
                       "message":""}),200,{'ContentType':'application/json'}

@app.route('/api/setclipboard',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def setclipboard():
    print "setclipboard"
    clientip = request.environ['REMOTE_ADDR']
    
    import pyperclip
    clipboard = pyperclip.paste()
    print "clipboard",clipboard

    return json.dumps({"clipboard":clipboard})
#                       "message":""}),200,{'ContentType':'application/json'}


@app.route('/api/launchcommand',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def launchcommand():
    data = request.get_json()
    command = os.system(data["command"])
    return json.dumps({"success":True,
                           "message":""}),200,  {'ContentType':'application/json'}

@app.route('/api/addlibrary',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def addlibrary():
    clientip = request.environ['REMOTE_ADDR']
    print clientip
    print "appd.api.addlibrary ip =",clientip

    # init db if it doesn't exist
    if not os.path.exists(webconfig.arkdbfile):
        db.create_all()
        db.session.commit()

    url = "http://"+clientip+":%d"%webconfig.SERVER_PORT
    adb = arkdb.ArkDb(url=url,verbose=True)
    
    data = request.get_json() or request.form
    
    success = adb.addlibrary(data["path"])

    if success:
        print "addlibrary: success"
        return json.dumps({"success":True},
                          200, 
                          {'ContentType':'application/json'})
    else:
        print "addlibrary: failed"
        return json.dumps({"success":False,
                           "message":"Error building library "+data["path"]}),410,{'ContentType':'application/json'}

@app.route('/api/updatelibrary',methods=['GET','POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def updatelibrary():
    clientip = request.environ['REMOTE_ADDR']
    print clientip
    print "appd.api.updatelibrary ip =",clientip

    print "We are in the parent process with PID= %d"%os.getpid()
    pid=os.fork()
    if pid==0:
        # init db if it doesn't exist
        if not os.path.exists(webconfig.arkdbfile):
            db.create_all()
            db.session.commit()

        url = "http://"+clientip+":%d"%webconfig.SERVER_PORT
        adb = arkdb.ArkDb(url=url,verbose=True)
    
        data = request.get_json() or request.form
    
        success = adb.updatelibrary(data["name"])    
        if success:
            print "updatelibrary: success"
        else:
            print "updatelibrary: failed"
        return json.dumps({"success":True},
                          200, 
                          {'ContentType':'application/json'})
            
    else:
        return json.dumps({"success":True},
                          200, 
                          {'ContentType':'application/json'})


@app.route('/',methods=['GET','POST'])
def index():
    return render_template("index.html")

