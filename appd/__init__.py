import os

# -------   Primary Flask & SqlAlchemy Setup

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.triangle import Triangle
from flask.ext.cors import CORS, cross_origin 

app = Flask(__name__)
cors = CORS(app, resources={r"/foo": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'

Triangle(app)

app.config.from_object('webconfigd')
db = SQLAlchemy(app)

# -------   Tornado & Websockets Setup
from tornado.wsgi import WSGIContainer
from tornado import web
from tornado import websocket

class WSHandler(websocket.WebSocketHandler):
    clients = []
    def open(self):
        WSHandler.clients.append(self)
        print 'connection opened...'

    def on_message(self, message):
        # application logic
        print 'received:', message
        self.write_message("Roger that")

    def on_close(self):
        WSHandler.clients.remove(self)
        print 'connection closed...'

tr = WSGIContainer(app)
application = web.Application([
    (r'/ws', WSHandler),
    (r".*", web.FallbackHandler, dict(fallback=tr)),
])

# ------- Jinja
#from app.momentjs import momentjs
#app.jinja_env.globals['momentjs'] = momentjs
app.jinja_env.globals['len'] = len

from appd import api,views

import utils
